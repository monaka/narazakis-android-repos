package info.narazaki.android.tuboroid.activity;

import java.io.File;

import info.narazaki.android.lib.activity.PickFileActivityBase;
import info.narazaki.android.lib.activity.base.NPreferenceActivity;
import info.narazaki.android.lib.dialog.SimpleDialog;
import info.narazaki.android.lib.system.MigrationSDK4;
import info.narazaki.android.lib.system.MigrationSDK5;
import info.narazaki.android.lib.toast.ManagedToast;
import info.narazaki.android.tuboroid.R;
import info.narazaki.android.tuboroid.TuboroidApplication;
import info.narazaki.android.tuboroid.agent.FavoriteImportExportAgent;
import info.narazaki.android.tuboroid.agent.TuboroidAgent;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.WindowManager;

public class SettingsActivity extends NPreferenceActivity {
    private static final String TAG = "SettingsActivity";
    public static final int INTENT_RESULT_EXT_STORAGE = 1;
    
    public static final int INTENT_RESULT_EXPORT_FOR_TUBOROID = 10;
    public static final int INTENT_RESULT_EXPORT_FOR_EN2CH = 11;
    public static final int INTENT_RESULT_IMPORT_FROM_TUBOROID = 20;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getTuboroidApplication().isFullScreenMode()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }
        
        setRequestedOrientation(getTuboroidApplication().getCurrentScreenOrientation());
        
        getTuboroidApplication().applyTheme(this);
        
        super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(R.xml.setting);
        
        Configuration config = getResources().getConfiguration();
        
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        // SDカードの有無
        CheckBoxPreference use_external = (CheckBoxPreference) findPreference("pref_use_external_storage");
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            use_external.setEnabled(true);
        }
        else {
            use_external.setEnabled(false);
        }
        
        // 外部ストレージのパス設定
        Preference pref_external_storage_path = findPreference("pref_external_storage_path");
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            pref_external_storage_path.setEnabled(true);
            pref_external_storage_path.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    onPrefExternalStoragePathClicked();
                    return true;
                }
            });
            pref_external_storage_path.setSummary(pref.getString("pref_external_storage_path", ""));
        }
        else {
            pref_external_storage_path.setEnabled(false);
        }
        
        // NGのクリア
        Preference manage_clear_ignores = findPreference("manage_clear_ignores");
        manage_clear_ignores.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                onClearIgnoresPrefClicked();
                return true;
            }
        });
        
        // クッキーのクリア
        Preference manage_clear_cookies= findPreference("manage_clear_cookies");
        manage_clear_cookies.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                onClearCookiesPrefClicked();
                return true;
            }
        });
        
        // 外部AAフォントの設定
        Preference pref_external_aa_font = findPreference("pref_external_aa_font");
        if (MigrationSDK4.supported()) {
            pref_external_aa_font.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    onExternalAAFontPrefClicked();
                    return true;
                }
            });
        }
        else {
            pref_external_aa_font.setEnabled(false);
            pref_external_aa_font.setSummary(R.string.pref_summary_use_external_storage_not_available);
        }
        
        // 操作系
        PreferenceCategory pref_cat_scrolling_setting = (PreferenceCategory) findPreference("pref_cat_scrolling_setting");
        // トラックボール(カーソル)スクロール
        Preference pref_use_page_up_down_key = findPreference("pref_use_page_up_down_key");
        if (config.navigation == Configuration.NAVIGATION_NONAV) {
            pref_use_page_up_down_key.setEnabled(false);
            pref_cat_scrolling_setting.removePreference(pref_use_page_up_down_key);
        }
        
        // ボリュームボタンスクロール
        boolean has_volume_key = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_VOLUME_UP)
                && KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        Preference pref_use_volume_button_scrolling = findPreference("pref_use_volume_button_scrolling");
        if (!has_volume_key) {
            pref_use_volume_button_scrolling.setEnabled(false);
            pref_cat_scrolling_setting.removePreference(pref_use_volume_button_scrolling);
        }
        
        // カメラボタンスクロール
        boolean has_camera_key = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_CAMERA);
        Preference pref_use_camera_button_scrolling = findPreference("pref_use_camera_button_scrolling");
        if (!has_camera_key) {
            pref_use_camera_button_scrolling.setEnabled(false);
            pref_cat_scrolling_setting.removePreference(pref_use_camera_button_scrolling);
        }
        
        setListPreferenceSummary("pref_theme_setting", R.string.pref_summary_theme_setting);
        setListPreferenceSummary("pref_full_screen_mode", R.string.pref_summary_full_screen_mode);
        setListPreferenceSummary("pref_screen_orientation", R.string.pref_summary_screen_orientation);
        
        setListPreferenceSummary("pref_font_size_board_list", R.string.pref_summary_font_size_board_list);
        setListPreferenceSummary("pref_font_size_thread_list_base", R.string.pref_summary_font_size_thread_list_base);
        setListPreferenceSummary("pref_font_size_entry_header", R.string.pref_summary_font_size_entry_header);
        setListPreferenceSummary("pref_font_size_entry_body", R.string.pref_summary_font_size_entry_body);
        setListPreferenceSummary("pref_font_rate_entry_aa_body", R.string.pref_summary_font_rate_entry_aa_body);
        setListPreferenceSummary("pref_thumbnail_size", R.string.pref_summary_thumbnail_size);
        setListPreferenceSummary("pref_scrolling_amount", R.string.pref_summary_scrolling_amount);
        
        setEditTextPreferenceSummary("pref_maru_user_id");
        setEditPasswordPreferenceSummary("pref_maru_password");
        
        setEditTextPreferenceSummary("pref_p2_user_id");
        setEditPasswordPreferenceSummary("pref_p2_password");
        
        setListPreferenceSummary("favorites_update_interval", R.string.pref_summary_favorites_update_interval);
        setListPreferenceSummary("pref_bbsmenu_url_list", R.string.pref_summary_bbsmenu_url);
        
        // インポート・エクスポート
        Preference pref_favorite_export = findPreference("pref_favorite_export");
        Preference pref_favorite_import = findPreference("pref_favorite_import");
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            pref_favorite_export.setEnabled(true);
            pref_favorite_export.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    onImportPrefClicked();
                    return true;
                }
            });
            pref_favorite_import.setEnabled(true);
            pref_favorite_import.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    onImportTuboroidPrefClicked();
                    return true;
                }
            });
        }
        else {
            pref_favorite_export.setEnabled(false);
            pref_favorite_import.setEnabled(false);
        }
        
    }
    
    @Override
    protected void onPause() {
        getTuboroidApplication().reloadPreferences(true);
        super.onPause();
    }
    
    protected TuboroidApplication getTuboroidApplication() {
        return ((TuboroidApplication) getApplication());
    }
    
    public TuboroidAgent getAgent() {
        return getTuboroidApplication().getAgent();
    }
    
    private void onClearIgnoresPrefClicked() {
        SimpleDialog.showYesNo(SettingsActivity.this, R.string.dialog_clear_ignores_title,
                R.string.dialog_clear_ignores, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getAgent().clearNG();
                        ManagedToast.raiseToast(SettingsActivity.this, R.string.toast_clear_ignores);
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {}
                });
    }
    
    private void onClearCookiesPrefClicked() {
        SimpleDialog.showYesNo(SettingsActivity.this, R.string.dialog_clear_cookies_title,
                R.string.dialog_clear_cookies, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getAgent().clearCookie();
                        ManagedToast.raiseToast(SettingsActivity.this, R.string.toast_clear_cookies);
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {}
                });
    }
    
    private void onExternalAAFontPrefClicked() {
        Intent intent = new Intent(this, SettingAAFontActivity.class);
        MigrationSDK5.Intent_addFlagNoAnimation(intent);
        startActivity(intent);
    }
    
    private void onPrefExternalStoragePathClicked() {
        File ext_storage_base = Environment.getExternalStorageDirectory();
        File ext_storage = new File(ext_storage_base, getTuboroidApplication().getExternalStoragePathName());
        ext_storage.mkdirs();
        String path = ext_storage.getAbsolutePath();
        
        Intent intent = new Intent(this, PickFileActivity.class);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_ALLOW_NEW_DIR, true);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_CHECK_WRITABLE, true);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_ROOT, ext_storage_base.getAbsolutePath());
        intent.putExtra(PickFileActivityBase.INTENT_KEY_CURRENT, path);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_FONT_SIZE,
                getTuboroidApplication().view_config_.entry_body_ * 3 / 2);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_TITLE, getString(R.string.pref_title_external_storage_path));
        intent.putExtra(PickFileActivityBase.INTENT_KEY_PICK_DIRECTORY, true);
        
        MigrationSDK5.Intent_addFlagNoAnimation(intent);
        startActivityForResult(intent, INTENT_RESULT_EXT_STORAGE);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == INTENT_RESULT_EXT_STORAGE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                if (uri == null) return;
                String target_path = uri.getPath();
                if (target_path == null) return;
                File ext_storage = Environment.getExternalStorageDirectory();
                String local_path = target_path.substring(ext_storage.getAbsolutePath().length());
                
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
                Editor editor = pref.edit();
                editor.putString("pref_external_storage_path", local_path);
                editor.commit();
                Preference pref_external_storage_path = findPreference("pref_external_storage_path");
                pref_external_storage_path.setSummary(local_path);
            }
            return;
        }
        else if (requestCode == INTENT_RESULT_EXPORT_FOR_TUBOROID) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                if (uri == null) return;
                String target_path = uri.getPath();
                if (target_path == null) return;
                exportForTuboroid(new File(target_path));
            }
            return;
        }
        else if (requestCode == INTENT_RESULT_EXPORT_FOR_EN2CH) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                if (uri == null) return;
                String target_path = uri.getPath();
                if (target_path == null) return;
                exportForEn2ch(target_path);
            }
            return;
        }
        else if (requestCode == INTENT_RESULT_IMPORT_FROM_TUBOROID) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                if (uri == null) return;
                String target_path = uri.getPath();
                if (target_path == null) return;
                importFromTuboroid(new File(target_path));
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    // /////////////////////////////////////////////////////////////////////
    // インポート・エクスポート
    // /////////////////////////////////////////////////////////////////////
    private void onImportPrefClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.pref_title_favorite_export);
        builder.setItems(R.array.export_titles, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                case 0:
                    // 0 : Export for Tuboroid
                    onExportTuboroidPrefClicked();
                    break;
                case 1:
                    // 1 : Export for En2ch
                    onExportEn2chPrefClicked();
                    break;
                }
            }
        });
        builder.create().show();
    }
    
    private void onExportTuboroidPrefClicked() {
        File ext_storage_base = Environment.getExternalStorageDirectory();
        String ext_storage_name = getTuboroidApplication().getExternalStoragePathName();
        if (ext_storage_name == null) ext_storage_name = "";
        File ext_storage = new File(ext_storage_base, ext_storage_name);
        
        Intent intent = new Intent(SettingsActivity.this, PickFileActivity.class);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_ALLOW_NEW_DIR, true);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_ALLOW_NEW_FILE, true);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_CHECK_WRITABLE, true);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_ROOT, ext_storage_base.getAbsolutePath());
        intent.putExtra(PickFileActivityBase.INTENT_KEY_CURRENT, ext_storage.getAbsolutePath());
        intent.putExtra(PickFileActivityBase.INTENT_KEY_DEFAULT_NEW_FILENAME, "bookmarks.xml");
        intent.putExtra(PickFileActivityBase.INTENT_KEY_FONT_SIZE,
                getTuboroidApplication().view_config_.entry_body_ * 3 / 2);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_TITLE, getString(R.string.pref_title_export_tuboroid_file));
        intent.putExtra(PickFileActivityBase.INTENT_KEY_PICK_DIRECTORY, false);
        
        MigrationSDK5.Intent_addFlagNoAnimation(intent);
        startActivityForResult(intent, INTENT_RESULT_EXPORT_FOR_TUBOROID);
    }
    
    private void onExportEn2chPrefClicked() {
        File ext_storage_base = Environment.getExternalStorageDirectory();
        File default_path = new File(ext_storage_base, "/en2ch/logs");
        if (!default_path.canWrite()) {
            default_path = ext_storage_base;
        }
        
        Intent intent = new Intent(SettingsActivity.this, PickFileActivity.class);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_ALLOW_NEW_DIR, false);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_CHECK_WRITABLE, false);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_ROOT, ext_storage_base.getAbsolutePath());
        intent.putExtra(PickFileActivityBase.INTENT_KEY_CURRENT, default_path.getAbsolutePath());
        intent.putExtra(PickFileActivityBase.INTENT_KEY_FONT_SIZE,
                getTuboroidApplication().view_config_.entry_body_ * 3 / 2);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_TITLE, getString(R.string.pref_title_export_en2ch_path));
        intent.putExtra(PickFileActivityBase.INTENT_KEY_PICK_DIRECTORY, true);
        
        MigrationSDK5.Intent_addFlagNoAnimation(intent);
        startActivityForResult(intent, INTENT_RESULT_EXPORT_FOR_EN2CH);
    }
    
    private void onImportTuboroidPrefClicked() {
        File ext_storage_base = Environment.getExternalStorageDirectory();
        String ext_storage_name = getTuboroidApplication().getExternalStoragePathName();
        if (ext_storage_name == null) ext_storage_name = "";
        File ext_storage = new File(ext_storage_base, ext_storage_name);
        
        Intent intent = new Intent(SettingsActivity.this, PickFileActivity.class);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_ALLOW_NEW_DIR, false);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_ALLOW_NEW_FILE, false);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_CHECK_WRITABLE, false);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_ROOT, ext_storage_base.getAbsolutePath());
        intent.putExtra(PickFileActivityBase.INTENT_KEY_CURRENT, ext_storage.getAbsolutePath());
        intent.putExtra(PickFileActivityBase.INTENT_KEY_DEFAULT_NEW_FILENAME, "bookmarks.xml");
        intent.putExtra(PickFileActivityBase.INTENT_KEY_FONT_SIZE,
                getTuboroidApplication().view_config_.entry_body_ * 3 / 2);
        intent.putExtra(PickFileActivityBase.INTENT_KEY_TITLE, getString(R.string.pref_title_import_tuboroid_file));
        intent.putExtra(PickFileActivityBase.INTENT_KEY_PICK_DIRECTORY, false);
        
        MigrationSDK5.Intent_addFlagNoAnimation(intent);
        startActivityForResult(intent, INTENT_RESULT_IMPORT_FROM_TUBOROID);
    }
    
    private void alertImportExportError(final String message) {
        SimpleDialog.showNotice(this, R.string.dialog_error, message, null);
    }
    
    private void exportForTuboroid(final File target_file) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle(R.string.dialog_exporting_is_in_progress);
        dialog.setIndeterminate(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        
        final FavoriteImportExportAgent.Terminator terminator = getAgent().exportFavoritesForTuboroid(target_file,
                new FavoriteImportExportAgent.ExportForTuboroidCallback() {
                    private int current_num_ = 0;
                    
                    @Override
                    public void onStart(final int threads_num) {
                        current_num_ = 0;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                dialog.setMax(threads_num);
                            }
                        });
                    }
                    
                    @Override
                    public void onProgress() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                current_num_++;
                                dialog.setProgress(current_num_);
                            }
                        });
                    }
                    
                    @Override
                    public void onFinished() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                if (dialog.isShowing()) dialog.cancel();
                            }
                        });
                    }
                    
                    @Override
                    public void onFailed() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                if (dialog.isShowing()) dialog.cancel();
                                alertImportExportError(getString(R.string.text_export_failed));
                            }
                        });
                    }
                });
        
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                terminator.terminate();
            }
        });
        dialog.show();
    }
    
    private void importFromTuboroid(final File target_file) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle(R.string.dialog_importing_is_in_progress);
        dialog.setMessage("");
        dialog.setIndeterminate(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        
        final FavoriteImportExportAgent.Terminator terminator = getAgent().importFavoritesFromTuboroid(target_file,
                new FavoriteImportExportAgent.ImportFromTuboroidCallback() {
                    private int current_num_ = 0;
                    
                    @Override
                    public void onStart(final int threads_num) {
                        current_num_ = 0;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                dialog.setMax(threads_num);
                            }
                        });
                    }
                    
                    @Override
                    public void onProgress(final String name) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                current_num_++;
                                dialog.setProgress(current_num_);
                                dialog.setMessage(name);
                            }
                        });
                    }
                    
                    @Override
                    public void onFinished() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                if (dialog.isShowing()) dialog.cancel();
                            }
                        });
                    }
                    
                    @Override
                    public void onFailed(final String message) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                if (dialog.isShowing()) dialog.cancel();
                                alertImportExportError(message);
                            }
                        });
                    }
                });
        
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                terminator.terminate();
            }
        });
        dialog.show();
    }
    
    private void exportForEn2ch(final String target_path) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle(R.string.dialog_exporting_is_in_progress);
        dialog.setMessage("");
        dialog.setIndeterminate(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        
        final FavoriteImportExportAgent.Terminator terminator = getAgent().exportFavoritesForEn2ch(target_path,
                new FavoriteImportExportAgent.ExportForEn2chCallback() {
                    private int current_num_ = 0;
                    
                    @Override
                    public void onStart(final int threads_num) {
                        current_num_ = 0;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                dialog.setMax(threads_num);
                            }
                        });
                    }
                    
                    @Override
                    public void onProgress(final String thread_name) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                current_num_++;
                                dialog.setProgress(current_num_);
                                dialog.setMessage(thread_name);
                            }
                        });
                    }
                    
                    @Override
                    public void onFinished() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                if (dialog.isShowing()) dialog.cancel();
                            }
                        });
                    }
                    
                    @Override
                    public void onFailed() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isFinishing()) return;
                                if (dialog.isShowing()) dialog.cancel();
                                alertImportExportError(getString(R.string.text_export_failed));
                            }
                        });
                    }
                });
        
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                terminator.terminate();
            }
        });
        dialog.show();
    }
}
