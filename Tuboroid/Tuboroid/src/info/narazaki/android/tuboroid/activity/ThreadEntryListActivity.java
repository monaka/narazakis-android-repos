package info.narazaki.android.tuboroid.activity;

import info.narazaki.android.lib.activity.base.NListActivity.PositionData;
import info.narazaki.android.lib.adapter.SimpleListAdapterBase;
import info.narazaki.android.lib.dialog.SimpleDialog;
import info.narazaki.android.lib.system.MigrationSDK5;
import info.narazaki.android.lib.system.NCallback;
import info.narazaki.android.lib.toast.ManagedToast;
import info.narazaki.android.lib.view.NLabelView;
import info.narazaki.android.tuboroid.R;
import info.narazaki.android.tuboroid.TuboroidApplication;
import info.narazaki.android.tuboroid.activity.base.SearchableListActivity;
import info.narazaki.android.tuboroid.adapter.ThreadEntryListAdapter;
import info.narazaki.android.tuboroid.agent.FavoriteCacheListAgent.NextFavoriteThreadFetchedCallback;
import info.narazaki.android.tuboroid.agent.ThreadEntryListAgent;
import info.narazaki.android.tuboroid.agent.thread.SQLiteAgent;
import info.narazaki.android.tuboroid.data.IgnoreData;
import info.narazaki.android.tuboroid.data.ThreadData;
import info.narazaki.android.tuboroid.data.ThreadEntryData;
import info.narazaki.android.tuboroid.service.ITuboroidService;
import info.narazaki.android.tuboroid.service.TuboroidService;
import info.narazaki.android.tuboroid.service.TuboroidServiceTask;
import info.narazaki.android.tuboroid.service.TuboroidServiceTask.ServiceSender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.text.ClipboardManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class ThreadEntryListActivity extends SearchableListActivity {
    public static final String TAG = "ThreadEntryListActivity";
    
    public static final String INTENT_KEY_URL = "KEY_URL";
    public static final String INTENT_KEY_MAYBE_ONLINE_COUNT = "KEY_MAYBE_ONLINE_COUNT";
    public static final String INTENT_KEY_MAYBE_THREAD_NAME = "KEY_MAYBE_THREAD_NAME";
    public static final String INTENT_KEY_FILTER_PARCELABLE = "KEY_FILTER_PARCELABLE";
    
    public static final String INTENT_KEY_RESTORE_ENTRY_ID = "RESTORE_ENTRY_ID";
    public static final String INTENT_KEY_RESTORE_Y = "KEY_RESTORE_Y";
    public static final String INTENT_KEY_ANCHOR_JUMP_STACK = "KEY_ANCHOR_JUMP_STACK";
    
    public static final int INTENT_ID_SHOW_ENTRY_EDITOR = 1;
    
    // コンテキストメニュー
    private final static int CTX_MENU_REPLY_TO_ENTRY = 1;
    private final static int CTX_MENU_FIND_BY_ENTRY_ID = 2;
    private final static int CTX_MENU_FIND_RELATED_ENTRIES = 3;
    private final static int CTX_MENU_ADD_IGNORE = 4;
    private final static int CTX_MENU_DELETE_IGNORE = 5;
    private final static int CTX_MENU_COPY_TO_CLIPBOARD = 6;
    private final static int CTX_MENU_DELETE_IMAGES = 10;
    
    // メニュー
    // ツールバーの出し入れ
    public static final int MENU_KEY_TOOLBAR_1 = 10;
    public static final int MENU_KEY_TOOLBAR_2 = 11;
    
    // サーチバーの出し入れ
    public static final int MENU_KEY_SEARCH_BAR_1 = 15;
    public static final int MENU_KEY_SEARCH_BAR_2 = 16;
    
    //
    public static final int MENU_KEY_COMPOSE = 20;
    public static final int MENU_KEY_SIMILAR = 30;
    
    public static final int MENU_KEY_COPY_INFO = 40;
    
    // スレ情報
    private Uri thread_uri_;
    private ThreadData thread_data_;
    private int maybe_online_count_;
    
    // onResumeで再読み込みする(エディットから戻った時等)
    private boolean reload_on_resume_ = false;
    private boolean jump_on_resume_after_post_ = false;
    private Runnable on_reloaded_callback_ = null;
    
    // キャッシュの読み込みが終了した時点でレジュームしたかどうか
    public static final int ON_CACHE_RELOADED_RESTORE_STATE_NONE = -1;
    public static final int ON_CACHE_RELOADED_RESTORE_STATE_COMPLETE = -2;
    
    private int restore_position_on_cache_reloaded_ = 0;
    
    // 検索・抽出から脱出した時の戻り先(0なら無効)
    private PositionData stored_before_filter_pos_ = null;
    
    // アンカージャンプ中管理スタック
    private ArrayList<Integer> anchor_jump_stack_ = null;
    private PositionData stored_before_anchor_jump_pos_ = null;
    
    // フィルタ情報
    private ParcelableFilterData filter_ = null;
    private boolean quick_relation_mode_ = false;
    
    // プログレスバー
    private final static int DEFAULT_MAX_PROGRESS = 1000;
    private int reload_progress_max_ = DEFAULT_MAX_PROGRESS;
    private int reload_progress_cur_ = 0;
    
    // スクロールキー
    private boolean use_scroll_key_ = false;
    
    // サービスクライアント
    private BroadcastReceiver service_intent_receiver_;
    private TuboroidServiceTask service_task_ = null;
    
    // フッタ
    private View footer_view_;
    private ThreadData next_thread_data_;
    private boolean favorite_check_update_progress_;
    private int unread_thread_count_;
    
    // ////////////////////////////////////////////////////////////
    // ステート管理系
    // ////////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_list);
        
        registerForContextMenu(getListView());
        
        if (savedInstanceState == null) {
            reload_on_resume_ = true;
        }
        else {
            reload_on_resume_ = false;
        }
        service_intent_receiver_ = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, final Intent intent) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onCheckUpdateFinished(intent);
                    }
                });
            }
        };
        
        // スレッド情報の取得(URLから作れる範囲の暫定のもの)
        thread_uri_ = getIntent().getData();
        if (thread_uri_ == null) {
            if (savedInstanceState != null && savedInstanceState.containsKey(INTENT_KEY_URL)) {
                thread_uri_ = Uri.parse(savedInstanceState.getString(INTENT_KEY_URL));
            }
        }
        if (thread_uri_ == null) return;
        thread_data_ = ThreadData.factory(thread_uri_);
        if (thread_data_ == null) return;
        
        // 板情報がない時のために作成処理
        getAgent().getBoardData(Uri.parse(thread_data_.getBoardIndexURI()), false, null);
        
        // 暫定スレ情報
        maybe_online_count_ = DEFAULT_MAX_PROGRESS;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(INTENT_KEY_MAYBE_ONLINE_COUNT)) {
                maybe_online_count_ = extras.getInt(INTENT_KEY_MAYBE_ONLINE_COUNT);
            }
            else if (savedInstanceState != null && savedInstanceState.containsKey(INTENT_KEY_MAYBE_ONLINE_COUNT)) {
                maybe_online_count_ = savedInstanceState.getInt(INTENT_KEY_MAYBE_ONLINE_COUNT);
            }
            if (extras.containsKey(INTENT_KEY_MAYBE_THREAD_NAME)) {
                thread_data_.thread_name_ = extras.getString(INTENT_KEY_MAYBE_THREAD_NAME);
            }
            else if (savedInstanceState != null && savedInstanceState.containsKey(INTENT_KEY_MAYBE_THREAD_NAME)) {
                thread_data_.thread_name_ = savedInstanceState.getString(INTENT_KEY_MAYBE_THREAD_NAME);
            }
        }
        
        // フィルタ
        filter_ = new ParcelableFilterData();
        quick_relation_mode_ = false;
        
        stored_before_filter_pos_ = null;
        
        anchor_jump_stack_ = new ArrayList<Integer>();
        
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(INTENT_KEY_RESTORE_ENTRY_ID)) {
                int restore_entry_id = savedInstanceState.getInt(INTENT_KEY_RESTORE_ENTRY_ID);
                int restore_entry_y = 0;
                if (savedInstanceState.containsKey(INTENT_KEY_RESTORE_Y)) {
                    restore_entry_y = savedInstanceState.getInt(INTENT_KEY_RESTORE_Y);
                }
                stored_before_filter_pos_ = new PositionData(restore_entry_id, restore_entry_y);
            }
            if (savedInstanceState.containsKey(INTENT_KEY_ANCHOR_JUMP_STACK)) {
                anchor_jump_stack_ = new ArrayList<Integer>(
                        savedInstanceState.getIntegerArrayList(INTENT_KEY_ANCHOR_JUMP_STACK));
            }
            if (savedInstanceState.containsKey(INTENT_KEY_FILTER_PARCELABLE)) {
                filter_ = savedInstanceState.getParcelable(INTENT_KEY_FILTER_PARCELABLE);
            }
        }
        updateFilterBar();
        
        // スレ情報読み込み
        getAgent().initNewThreadData(thread_data_, null);
        
        // リロード時ジャンプ
        int jump_on_reloaded_num = thread_data_.getJumpEntryNum(thread_uri_);
        if (jump_on_reloaded_num > 0) {
            setRestorePosition(jump_on_reloaded_num - 1, 0);
        }
        
        // アンカーバー初期化
        NLabelView text_view = (NLabelView) findViewById(R.id.entry_anchor_stack);
        text_view.setTouchMargin(getTuboroidApplication().view_config_.touch_margin_ > 0);
        int on_clicked_bgcolor = obtainStyledAttributes(R.styleable.Theme).getColor(
                R.styleable.Theme_entryLinkClickedBgColor, 0);
        text_view.setTouchBackgroundColorSpan(new BackgroundColorSpan(on_clicked_bgcolor));
        text_view.setTextSize(getTuboroidApplication().view_config_.entry_header_ * 3 / 2);
        updateAnchorBar();
        
        // Related欄初期化
        
        // フッタ
        next_thread_data_ = null;
        favorite_check_update_progress_ = false;
        unread_thread_count_ = 0;
        View footer_row = LayoutInflater.from(this).inflate(R.layout.entry_list_footer_row, null);
        footer_view_ = footer_row.findViewById(R.id.entry_footer_box);
        footer_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFooterClicked();
            }
        });
        footer_view_.setVisibility(View.GONE);
        getListView().addFooterView(footer_row);
        
        // スクロールキー
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        use_scroll_key_ = pref.getBoolean("pref_use_page_up_down_key", true);
        
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (thread_data_ != null && thread_uri_ != null) {
            outState.putString(INTENT_KEY_URL, thread_uri_.toString());
            outState.putInt(INTENT_KEY_MAYBE_ONLINE_COUNT,
                    thread_data_.online_count_ > maybe_online_count_ ? thread_data_.online_count_ : maybe_online_count_);
            outState.putString(INTENT_KEY_MAYBE_THREAD_NAME, thread_data_.thread_name_);
        }
        
        if (filter_ != null) {
            outState.putParcelable(INTENT_KEY_FILTER_PARCELABLE, filter_);
        }
        if (stored_before_filter_pos_ != null) {
            outState.putInt(INTENT_KEY_RESTORE_ENTRY_ID, stored_before_filter_pos_.position_);
            outState.putInt(INTENT_KEY_RESTORE_Y, stored_before_filter_pos_.y_);
        }
        outState.putIntegerArrayList(INTENT_KEY_ANCHOR_JUMP_STACK, new ArrayList<Integer>(anchor_jump_stack_));
        
        super.onSaveInstanceState(outState);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        ((ThreadEntryListAdapter) list_adapter_).setFontSize(getTuboroidApplication().view_config_);
        
        registerReceiver(service_intent_receiver_, new IntentFilter(TuboroidService.CHECK_UPDATE.ACTION_FINISHED));
        service_task_ = new TuboroidServiceTask(getApplicationContext());
        service_task_.bind();
        if (thread_data_ == null) {
            finish();
            return;
        }
        
        // スレッド情報の読み込み
        getAgent().getThreadData(thread_data_, new SQLiteAgent.GetThreadDataResult() {
            @Override
            public void onQuery(final ThreadData thread_data) {
                postListViewAndUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!is_active_) return;
                        thread_data_ = thread_data;
                        ((ThreadEntryListAdapter) list_adapter_).setReadCount(thread_data_.read_count_);
                        ((ThreadEntryListAdapter) list_adapter_).setThreadData(thread_data_);
                        int pos = thread_data_.recent_pos_;
                        int pos_y = thread_data_.recent_pos_y_;
                        onFavoriteUpdated();
                        setTitle(thread_data_.thread_name_);
                        updateFooterRow(false, null);
                        
                        if (!isReloadInProgress()) {
                            restorePosition(pos, pos_y, new NCallback<Boolean>() {
                                @Override
                                public void run(Boolean arg) {
                                    if (arg) {
                                        clearRestorePosition();
                                        setListViewVisibility(true);
                                    }
                                }
                            });
                        }
                        else {
                            setRestorePosition(pos, pos_y);
                        }
                    }
                });
            }
        });
        
        updateAnchorBar();
    }
    
    @Override
    protected void onPause() {
        reload_progress_max_ = DEFAULT_MAX_PROGRESS;
        reload_progress_cur_ = 0;
        showProgressBar(false);
        
        unregisterReceiver(service_intent_receiver_);
        service_task_ = null;
        
        favorite_check_update_progress_ = false;
        
        if (footer_view_ != null) footer_view_.setVisibility(View.GONE);
        super.onPause();
    }
    
    @Override
    protected void onPauseRestorePosition(PositionData pos_data) {
        if (thread_data_ != null && pos_data != null && hasInitialData()) {
            thread_data_.recent_pos_ = pos_data.position_;
            thread_data_.recent_pos_y_ = pos_data.y_;
            getAgent().updateThreadRecentPos(thread_data_, null);
        }
    }
    
    @Override
    protected void onActivityResult(int request_code, int result_code, Intent data) {
        switch (request_code) {
        case INTENT_ID_SHOW_ENTRY_EDITOR:
            if (result_code == RESULT_OK) {
                reload_on_resume_ = true;
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
                if (pref.getBoolean("pref_jump_bottom_on_posted", true)) {
                    jump_on_resume_after_post_ = true;
                }
            }
            break;
        default:
            super.onActivityResult(request_code, result_code, data);
            break;
        }
    }
    
    @Override
    protected SimpleListAdapterBase<?> createListAdapter() {
        ThreadEntryListAdapter list_adapter = new ThreadEntryListAdapter(this, getAgent(), getListFontPref(),
                new ThreadEntryData.ImageViewerLauncher() {
                    @Override
                    public void onRequired(ThreadData threadData, String imageLocalFilename, String imageUri) {
                        Intent intent = new Intent(ThreadEntryListActivity.this, ImageViewerActivity.class);
                        intent.setData(Uri.parse(threadData.getThreadURI()));
                        intent.putExtra(ImageViewerActivity.INTENT_KEY_IMAGE_FILENAME, imageLocalFilename);
                        intent.putExtra(ImageViewerActivity.INTENT_KEY_IMAGE_URI, imageUri);
                        MigrationSDK5.Intent_addFlagNoAnimation(intent);
                        startActivity(intent);
                    }
                }, new ThreadEntryData.OnAnchorClickedCallback() {
                    
                    @Override
                    public void onNumberAnchorClicked(int jumpFrom, int jumpTo) {
                        if (jumpTo > 0) {
                            jumpToAnchor(jumpFrom, jumpTo);
                        }
                    }
                    
                    @Override
                    public void onThreadLinkClicked(Uri uri) {
                        Intent intent = new Intent(ThreadEntryListActivity.this, ThreadEntryListActivity.class);
                        intent.setData(uri);
                        MigrationSDK5.Intent_addFlagNoAnimation(intent);
                        startActivity(intent);
                    }
                    
                    @Override
                    public void onBoardLinkClicked(Uri uri) {
                        Intent intent = new Intent(ThreadEntryListActivity.this, ThreadListActivity.class);
                        intent.setData(uri);
                        MigrationSDK5.Intent_addFlagNoAnimation(intent);
                        startActivity(intent);
                    }
                });
        list_adapter.setThreadData(thread_data_);
        return list_adapter;
    }
    
    @Override
    protected void onFirstDataRequired() {
        if (thread_data_ == null) return;
        updateParcelableFilter(filter_);
        onResumeDataRequired();
    }
    
    @Override
    protected void onResumeDataRequired() {
        if (thread_data_ == null) return;
        if (reload_on_resume_) {
            reloadList(true);
        }
        else {
            reloadList(false);
        }
        reload_on_resume_ = false;
    }
    
    // ////////////////////////////////////////////////////////////
    // 内部インデックス(フィルタ前)からフィルタ後のインデックスを得る
    // ////////////////////////////////////////////////////////////
    @Override
    protected void restorePosition(int position, int y, NCallback<Boolean> callback) {
        PositionData data = getMappedPosition(position, y);
        if (data != null) {
            setListPositionFromTop(data.position_, data.y_, callback);
        }
        else {
            callback.run(false);
        }
    }
    
    @Override
    public PositionData getCurrentPosition() {
        if (thread_data_ != null && list_adapter_ != null && list_adapter_.getCount() > 0 && hasInitialData()) {
            int bottom_pos = getListView().getLastVisiblePosition();
            if (bottom_pos == list_adapter_.getCount()) {
                ThreadEntryData bottom_entry_data = ((ThreadEntryListAdapter) list_adapter_).getData(bottom_pos - 1);
                if (bottom_entry_data != null) {
                    int bottom_id = (int) (bottom_entry_data.entry_id_ - 1);
                    return new PositionData(bottom_id, 0);
                }
            }
            else {
                PositionData first_position = getFirstVisiblePositionData();
                if (first_position != null) return new PositionData(first_position.position_, first_position.y_);
            }
        }
        
        return null;
    }
    
    public PositionData getMappedPosition(int position, int y) {
        PositionData data = new PositionData(position, y);
        if (list_adapter_ == null) return null;
        int pos = ((ThreadEntryListAdapter) list_adapter_).getMappedPosition(position);
        if (pos == -1) return null;
        data.position_ = pos;
        return data;
    }
    
    public PositionData getMappedPosition(PositionData orig) {
        return getMappedPosition(orig.position_, orig.y_);
    }
    
    public void setMappedListPosition(final int position, final NCallback<Boolean> callback) {
        setMappedListPositionFromTop(position, 0, callback);
    }
    
    public void setMappedListPositionFromTop(final int position, final int y, final NCallback<Boolean> callback) {
        if (list_adapter_ == null) return;
        final int pos = ((ThreadEntryListAdapter) list_adapter_).getMappedPosition(position);
        if (pos == -1) return;
        setListPositionFromTop(pos, y, callback);
    }
    
    private PositionData getFirstVisiblePositionData() {
        ListView list_view = getListView();
        int mapped_pos = list_view.getFirstVisiblePosition();
        ThreadEntryData first_entry_data = ((ThreadEntryListAdapter) list_adapter_).getData(mapped_pos);
        if (first_entry_data != null && list_view.getCount() > 0) {
            long pos = first_entry_data.entry_id_ - 1;
            int y = list_view.getChildAt(0).getTop();
            return new PositionData((int) pos, y);
        }
        return null;
    }
    
    // ////////////////////////////////////////////////////////////
    // キー管理系
    // ////////////////////////////////////////////////////////////
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // BACKキーが押されたときの処理
            
            // アンカーの履歴があれば戻す
            if (anchor_jump_stack_.size() > 0) {
                exitAnchorJumpMode();
                if (quick_relation_mode_ && filter_.type_ == ParcelableFilterData.TYPE_RELATION) {
                    quick_relation_mode_ = false;
                    cancelSearchBar();
                }
                return true;
            }
            // フィルタされていれば戻す
            if (filter_.type_ != ParcelableFilterData.TYPE_NONE || hasVisibleSearchBar()) {
                cancelSearchBar();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP && !isToobarForcused()) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (use_scroll_key_) {
                    setListPageUp();
                }
                else {
                    setListRollUp(null);
                }
            }
            return true;
        }
        if (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN && !isToobarForcused()) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (use_scroll_key_) {
                    setListPageDown();
                }
                else {
                    setListRollDown(null);
                }
            }
            return true;
        }
        return super.dispatchKeyEvent(event);
    }
    
    private boolean isToobarForcused() {
        View forcused_view = getCurrentFocus();
        if (forcused_view == null) return false;
        ListView list_view = getListView();
        if (list_view.findFocus() == forcused_view) return false;
        
        return true;
    }
    
    // ////////////////////////////////////////////////////////////
    // アイテムタップ
    // ////////////////////////////////////////////////////////////
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menu_info) {
        if (list_adapter_ == null || menu_info == null) return;
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) menu_info;
        ThreadEntryData entry_data = ((ThreadEntryListAdapter) list_adapter_).getData(info.position);
        if (entry_data == null) return;
        
        menu.clear();
        menu.setHeaderTitle(String.format(getString(R.string.ctx_menu_title_entry), entry_data.entry_id_));
        menu.add(0, CTX_MENU_REPLY_TO_ENTRY, CTX_MENU_REPLY_TO_ENTRY, R.string.ctx_menu_reply_to);
        menu.add(0, CTX_MENU_COPY_TO_CLIPBOARD, CTX_MENU_COPY_TO_CLIPBOARD, R.string.ctx_menu_copy_to_clipboard);
        
        if (entry_data.canAddNGID()) {
            // ?が入ったIDはNG不可
            menu.add(0, CTX_MENU_FIND_BY_ENTRY_ID, CTX_MENU_FIND_BY_ENTRY_ID,
                    String.format(getString(R.string.ctx_menu_find_by_entry_id), entry_data.author_id_));
        }
        if (entry_data.isNG()) {
            menu.add(0, CTX_MENU_DELETE_IGNORE, CTX_MENU_DELETE_IGNORE, R.string.ctx_menu_delete_ignore);
        }
        else {
            menu.add(0, CTX_MENU_ADD_IGNORE, CTX_MENU_ADD_IGNORE, R.string.ctx_menu_add_ignore);
        }
        if (entry_data.hasShownThumbnails()) {
            menu.add(0, CTX_MENU_DELETE_IMAGES, CTX_MENU_DELETE_IMAGES, R.string.ctx_menu_delete_thumbnail_images);
        }
        
        menu.add(0, CTX_MENU_FIND_RELATED_ENTRIES, CTX_MENU_FIND_RELATED_ENTRIES,
                R.string.ctx_menu_find_related_entries);
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        ThreadEntryData entry_data = ((ThreadEntryListAdapter) list_adapter_).getData(info.position);
        if (entry_data == null) return false;
        
        switch (item.getItemId()) {
        case CTX_MENU_REPLY_TO_ENTRY:
            showDialogReplyTo(entry_data);
            break;
        case CTX_MENU_FIND_BY_ENTRY_ID:
            updateFilterByAuthorID(entry_data);
            break;
        case CTX_MENU_FIND_RELATED_ENTRIES:
            updateFilterByRelation(entry_data.entry_id_, entry_data.entry_id_);
            break;
        case CTX_MENU_ADD_IGNORE:
            showDialogAddIgnore(entry_data);
            break;
        case CTX_MENU_DELETE_IGNORE:
            getAgent().deleteNG(entry_data);
            reloadList(false, true);
            break;
        case CTX_MENU_COPY_TO_CLIPBOARD:
            showDialogCopyToClipboard(entry_data);
            break;
        case CTX_MENU_DELETE_IMAGES:
            entry_data.deleteThumbnails(getTuboroidApplication(), getAgent(), thread_data_);
            ((ThreadEntryListAdapter) list_adapter_).notifyDataSetChanged();
            break;
        default:
            break;
        }
        return true;
    }
    
    private void showDialogReplyTo(final ThreadEntryData entry_data) {
        String[] menu_strings = new String[] { getString(R.string.ctx_submenu_reply_to_this_entry),
                getString(R.string.ctx_submenu_quote_and_reply_to_this_entry) };
        
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.ctx_submenu_reply_to_title);
        builder.setItems(menu_strings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                
                switch (which) {
                case 0:
                    intent = new Intent(ThreadEntryListActivity.this, ThreadEntryEditActivity.class);
                    intent.setData(Uri.parse(thread_data_.getThreadURI()));
                    intent.putExtra(Intent.EXTRA_TEXT, ">>" + entry_data.entry_id_ + "\n");
                    startActivityForResult(intent, INTENT_ID_SHOW_ENTRY_EDITOR);
                    break;
                case 1:
                    intent = new Intent(ThreadEntryListActivity.this, ThreadEntryEditActivity.class);
                    intent.setData(Uri.parse(thread_data_.getThreadURI()));
                    String quoted_entry = getQuotedEntry(entry_data.entry_id_, entry_data.entry_body_);
                    intent.putExtra(Intent.EXTRA_TEXT, quoted_entry);
                    startActivityForResult(intent, INTENT_ID_SHOW_ENTRY_EDITOR);
                    break;
                }
            }
        });
        builder.create().show();
    }
    
    private void showDialogAddIgnore(final ThreadEntryData entry_data) {
        final String author_id = entry_data.author_id_;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.ctx_menu_add_ignore);
        
        if (entry_data.canAddNGID()) {
            String[] menu_strings = new String[] {
                    String.format(getString(R.string.ctx_menu_add_ignore_id_normal), entry_data.author_id_),
                    String.format(getString(R.string.ctx_menu_add_ignore_id_gone), entry_data.author_id_),
                    getString(R.string.ctx_menu_add_ignore_word_normal),
                    getString(R.string.ctx_menu_add_ignore_word_gone) };
            builder.setItems(menu_strings, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                    case 0:
                        getAgent().addNGID(author_id, IgnoreData.TYPE.NGID);
                        reloadList(false, true);
                        break;
                    case 1:
                        getAgent().addNGID(author_id, IgnoreData.TYPE.NGID_GONE);
                        reloadList(false, true);
                        break;
                    case 2:
                        showDialogAddNGWord(entry_data, false);
                        break;
                    case 3:
                        showDialogAddNGWord(entry_data, true);
                        break;
                    }
                }
            });
        }
        else {
            String[] menu_strings = new String[] { getString(R.string.ctx_menu_add_ignore_word_normal),
                    getString(R.string.ctx_menu_add_ignore_word_gone) };
            builder.setItems(menu_strings, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                    case 0:
                        showDialogAddNGWord(entry_data, false);
                        break;
                    case 1:
                        showDialogAddNGWord(entry_data, true);
                        break;
                    }
                }
            });
        }
        
        builder.create().show();
    }
    
    private void showDialogAddNGWord(final ThreadEntryData entry_data, final boolean gone) {
        // ビュー作成
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (!gone) {
            builder.setTitle(R.string.ctx_menu_add_ignore_word_normal);
        }
        else {
            builder.setTitle(R.string.ctx_menu_add_ignore_word_gone);
        }
        
        LayoutInflater layout_inflater = LayoutInflater.from(this);
        LinearLayout layout_view = (LinearLayout) layout_inflater.inflate(R.layout.add_ngword_dialog, null);
        builder.setView(layout_view);
        
        final int type = gone ? IgnoreData.TYPE.NGWORD_GONE : IgnoreData.TYPE.NGWORD;
        
        final EditText ngword_token = (EditText) layout_view.findViewById(R.id.add_ngword_token);
        final EditText ngword_orig = (EditText) layout_view.findViewById(R.id.add_ngword_orig);
        StringBuilder orig_text = new StringBuilder();
        orig_text.append(entry_data.author_name_);
        orig_text.append("\n");
        orig_text.append(entry_data.entry_body_);
        ngword_orig.setText(orig_text);
        ngword_orig.setSingleLine(false);
        
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                getAgent().addNGWord(ngword_token.getText().toString(), type);
                reloadList(false, true);
            }
        });
        builder.setCancelable(true);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {}
        });
        builder.create().show();
    }
    
    private void showDialogCopyToClipboard(final ThreadEntryData entry_data) {
        String[] menu_strings = new String[] { getString(R.string.ctx_submenu_copy_to_clipboard_id),
                getString(R.string.ctx_submenu_copy_to_clipboard_name),
                getString(R.string.ctx_submenu_copy_to_clipboard_body) };
        
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.ctx_menu_copy_to_clipboard);
        builder.setItems(menu_strings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                switch (which) {
                case 0:
                    cm.setText(entry_data.author_id_);
                    break;
                case 1:
                    cm.setText(entry_data.author_name_);
                    break;
                case 2:
                    showDialogCopyEntryBody(entry_data);
                    return;
                default:
                    return;
                }
                ManagedToast.raiseToast(getApplicationContext(), R.string.toast_copied);
            }
        });
        builder.create().show();
    }
    
    private void showDialogCopyThreadInfoToClipboard() {
        String[] menu_strings = new String[] { getString(R.string.label_submenu_copy_thread_info_title),
                getString(R.string.label_submenu_copy_thread_info_url),
                getString(R.string.label_submenu_copy_thread_info_title_url) };
        
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.label_menu_copy_thread_info);
        builder.setItems(menu_strings, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                switch (which) {
                case 0:
                    cm.setText(thread_data_.thread_name_);
                    break;
                case 1:
                    cm.setText(thread_data_.getThreadURI());
                    break;
                case 2:
                    cm.setText(thread_data_.thread_name_ + "\n" + thread_data_.getThreadURI());
                    break;
                default:
                    return;
                }
                ManagedToast.raiseToast(getApplicationContext(), R.string.toast_copied);
            }
        });
        builder.create().show();
    }
    
    // レス部分コピー
    private void showDialogCopyEntryBody(final ThreadEntryData entry_data) {
        // ビュー作成
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layout_inflater = LayoutInflater.from(this);
        LinearLayout layout_view = (LinearLayout) layout_inflater.inflate(R.layout.copy_entry_body_dialog, null);
        builder.setView(layout_view);
        
        final EditText copy_orig = (EditText) layout_view.findViewById(R.id.copy_orig);
        copy_orig.setText(entry_data.entry_body_);
        copy_orig.setSingleLine(false);
        
        builder.setCancelable(true);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {}
        });
        
        builder.create().show();
    }
    
    // ////////////////////////////////////////////////////////////
    // オプションメニュー
    // ////////////////////////////////////////////////////////////
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        
        // ツールバー
        createToolBarOptionMenu(menu, MENU_KEY_TOOLBAR_1, MENU_KEY_TOOLBAR_2, MENU_KEY_SEARCH_BAR_1,
                MENU_KEY_SEARCH_BAR_2);
        
        // 書き込み
        MenuItem compose_item = menu.add(0, MENU_KEY_COMPOSE, MENU_KEY_COMPOSE, getString(R.string.label_menu_compose));
        compose_item.setIcon(R.drawable.ic_menu_compose);
        compose_item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(ThreadEntryListActivity.this, ThreadEntryEditActivity.class);
                intent.setData(Uri.parse(thread_data_.getThreadURI()));
                startActivityForResult(intent, INTENT_ID_SHOW_ENTRY_EDITOR);
                return false;
            }
        });
        
        // 類似検索
        MenuItem similar_item = menu.add(0, MENU_KEY_SIMILAR, MENU_KEY_SIMILAR,
                getString(R.string.label_menu_find_similar_thread));
        similar_item.setIcon(android.R.drawable.ic_menu_gallery);
        similar_item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(ThreadEntryListActivity.this, SimilarThreadListActivity.class);
                intent.setData(Uri.parse(thread_data_.getBoardSubjectsURI()));
                intent.putExtra(SimilarThreadListActivity.KEY_SEARCH_KEY_NAME, thread_data_.thread_name_);
                intent.putExtra(SimilarThreadListActivity.KEY_SEARCH_THREAD_ID, thread_data_.thread_id_);
                MigrationSDK5.Intent_addFlagNoAnimation(intent);
                startActivity(intent);
                return false;
            }
        });
        
        // スレ情報コピー
        MenuItem copy_info_item = menu.add(0, MENU_KEY_COPY_INFO, MENU_KEY_COPY_INFO,
                getString(R.string.label_menu_copy_thread_info));
        copy_info_item.setIcon(android.R.drawable.ic_menu_agenda);
        copy_info_item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                showDialogCopyThreadInfoToClipboard();
                return false;
            }
        });
        
        return true;
    }
    
    // ////////////////////////////////////////////////////////////
    // ツールバー
    // ////////////////////////////////////////////////////////////
    
    @Override
    protected void createToolbarButtons() {
        super.createToolbarButtons();
        
        ImageButton button_thread_list = (ImageButton) findViewById(R.id.button_toolbar_thread_list);
        button_thread_list.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ThreadEntryListActivity.this, ThreadListActivity.class);
                intent.setData(Uri.parse(thread_data_.getBoardSubjectsURI()));
                startActivityForResult(intent, INTENT_ID_SHOW_ENTRY_EDITOR);
            }
        });
    }
    
    // ////////////////////////////////////////////////////////////
    // フッタ
    // ////////////////////////////////////////////////////////////
    
    private void updateFooterRow(final boolean maybe_has_new_unread, final Runnable callback) {
        if (!is_active_ || thread_data_ == null) {
            if (callback != null) callback.run();
            return;
        }
        
        getAgent().fetchNextFavoriteThread(thread_data_, new NextFavoriteThreadFetchedCallback() {
            @Override
            public void onNextFavoriteThreadFetched(final int unread_thread_count, final ThreadData next_thread_data,
                    final boolean current_has_unread) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!is_active_ || thread_data_ == null) {
                            if (callback != null) callback.run();
                            return;
                        }
                        if (maybe_has_new_unread) {
                            if (unread_thread_count > 0) {
                                String message = String.valueOf(unread_thread_count) + " "
                                        + getString(R.string.toast_new_entry_found_at_favorite_threads);
                                ManagedToast.raiseToast(ThreadEntryListActivity.this, message);
                            }
                            if (current_has_unread) {
                                reloadList(true);
                                if (callback != null) callback.run();
                                return;
                            }
                        }
                        if (unread_thread_count == 0) {
                            NotificationManager notif_manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            notif_manager.cancel(TuboroidApplication.NOTIF_ID_BACKGROUND_UPDATED);
                        }
                        
                        unread_thread_count_ = unread_thread_count;
                        next_thread_data_ = next_thread_data;
                        setFooterView();
                        
                        if (callback != null) callback.run();
                    }
                });
            }
        });
    }
    
    private void setFooterView() {
        if (thread_data_ == null || list_adapter_ == null || list_adapter_.getCount() <= 0) return;
        
        if (footer_view_.getVisibility() == View.GONE) footer_view_.setVisibility(View.VISIBLE);
        ImageView image_view = (ImageView) footer_view_.findViewById(R.id.entry_footer_image_view);
        NLabelView entry_footer_header = (NLabelView) footer_view_.findViewById(R.id.entry_footer_header);
        NLabelView entry_footer_body = (NLabelView) footer_view_.findViewById(R.id.entry_footer_body);
        if (favorite_check_update_progress_) {
            image_view.setImageResource(R.drawable.toolbar_btn_reload);
            entry_footer_header.setText(R.string.text_check_update_unread_favorite_threads);
            entry_footer_body.setText("");
        }
        else if (unread_thread_count_ > 0 && next_thread_data_ != null) {
            image_view.setImageResource(R.drawable.toolbar_btn_jump_right);
            String message = String.valueOf(unread_thread_count_) + " "
                    + getString(R.string.text_new_entry_found_at_favorite_threads);
            entry_footer_header.setText(message);
            entry_footer_body.setText(next_thread_data_.thread_name_);
        }
        else {
            image_view.setImageResource(R.drawable.toolbar_btn_reload);
            entry_footer_header.setText(R.string.text_no_new_entry_found_at_favorite_threads);
            entry_footer_body.setText(R.string.text_no_new_entry_found_at_favorite_threads_func);
        }
        footer_view_.invalidate();
    }
    
    private void showFooterView() {
        if (footer_view_.getVisibility() == View.GONE) setFooterView();
    }
    
    public void onFooterClicked() {
        if (!is_active_) return;
        if (service_task_ == null) return;
        if (list_adapter_ == null) return;
        if (next_thread_data_ == null) {
            if (favorite_check_update_progress_) return;
            favorite_check_update_progress_ = true;
            setFooterView();
            
            ManagedToast.raiseToast(ThreadEntryListActivity.this, R.string.toast_check_unread_favorite_threads,
                    Toast.LENGTH_SHORT);
            service_task_.send(new ServiceSender() {
                @Override
                public void send(ITuboroidService service) throws RemoteException {
                    service.checkUpdateFavorites(false);
                }
            });
        }
        else {
            Intent intent = new Intent(ThreadEntryListActivity.this, ThreadEntryListActivity.class);
            String uri = next_thread_data_.getThreadURI();
            intent.setData(Uri.parse(uri));
            MigrationSDK5.Intent_addFlagNoAnimation(intent);
            startActivity(intent);
        }
    }
    
    private void onCheckUpdateFinished(final Intent intent) {
        if (list_adapter_ == null) return;
        favorite_check_update_progress_ = false;
        int unread_threads = intent.getIntExtra(TuboroidService.CHECK_UPDATE.NUM_UNREAD_THREADS, 0);
        String message = null;
        Runnable callback = new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (list_adapter_ == null) return;
                        ((ThreadEntryListAdapter) list_adapter_).notifyDataSetChanged();
                    }
                });
            }
        };
        if (unread_threads == 0) {
            message = getString(R.string.toast_no_new_entry_found_at_favorite_threads);
            ManagedToast.raiseToast(this, message);
            updateFooterRow(false, callback);
        }
        else {
            updateFooterRow(true, callback);
        }
    }
    
    // ////////////////////////////////////////////////////////////
    // その他
    // ////////////////////////////////////////////////////////////
    private String getQuotedEntry(long entry_id, String body) {
        StringBuilder buf = new StringBuilder();
        buf.append(">>");
        buf.append(entry_id);
        buf.append("\n");
        for (String data : body.split("(\\r\\n|\\r|\\n)")) {
            buf.append("> ");
            buf.append(data);
            buf.append("\n");
        }
        return buf.toString();
    }
    
    @Override
    protected boolean isFavorite() {
        if (thread_data_ == null) return false;
        return thread_data_.is_favorite_;
    }
    
    @Override
    protected void addFavorite() {
        if (thread_data_ == null) return;
        getAgent().addFavorite(thread_data_, new Runnable() {
            @Override
            public void run() {
                thread_data_.is_favorite_ = true;
                onFavoriteUpdated();
            }
        });
    }
    
    @Override
    protected void deleteFavorite() {
        if (thread_data_ == null) return;
        getAgent().delFavorite(thread_data_, new Runnable() {
            @Override
            public void run() {
                thread_data_.is_favorite_ = false;
                onFavoriteUpdated();
            }
        });
    }
    
    // ////////////////////////////////////////////////////////////
    // フィルタモード
    // ////////////////////////////////////////////////////////////
    private void exitFilterMode() {
        if (stored_before_filter_pos_ != null) {
            if (hasRestorePosition()) {
                setRestorePosition(stored_before_filter_pos_.position_, stored_before_filter_pos_.y_);
            }
            else {
                setListPositionFromTop(stored_before_filter_pos_.position_, stored_before_filter_pos_.y_, null);
            }
            stored_before_filter_pos_ = null;
        }
    }
    
    private void onEntryFilterMode(int seved_entry_id, int saved_y) {
        stored_before_filter_pos_ = getFirstVisiblePositionData();
        updateAnchorBar();
    }
    
    private void updateFilterBar() {
        NLabelView text_view = (NLabelView) findViewById(R.id.entry_related_by);
        if (filter_.type_ == ParcelableFilterData.TYPE_NONE) {
            text_view.setVisibility(View.GONE);
            return;
        }
        text_view.setVisibility(View.VISIBLE);
        
        if (filter_.type_ == ParcelableFilterData.TYPE_AUTHOR_ID) {
            text_view.setText("ID: " + filter_.author_id_);
        }
        else if (filter_.type_ == ParcelableFilterData.TYPE_RELATION) {
            text_view.setText("By: >>" + filter_.target_entry_id_);
        }
        else if (filter_.type_ == ParcelableFilterData.TYPE_STRING) {
            text_view.setText("[" + filter_.string_filter_word_ + "]");
        }
        else {
            text_view.setText("");
        }
    }
    
    // ////////////////////////////////////////////////////////////
    // アンカー管理
    // ////////////////////////////////////////////////////////////
    private void jumpToAnchor(int current_num, int num) {
        if (anchor_jump_stack_.size() == 0) {
            anchor_jump_stack_.add(current_num);
            stored_before_anchor_jump_pos_ = getFirstVisiblePositionData();
            if (filter_.type_ == ParcelableFilterData.TYPE_NONE) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
                if (pref.getBoolean("pref_enter_related_mode_on_anchor_jump", false)) {
                    jumpToAnchorWithRelationMode(current_num, num);
                    return;
                }
            }
        }
        if (anchor_jump_stack_.indexOf(num) == -1) {
            anchor_jump_stack_.add(num);
            updateAnchorBar();
        }
        setMappedListPosition(num - 1, null);
    }
    
    private void jumpToAnchorWithRelationMode(int current_num, int num) {
        updateFilterByRelation(current_num, num);
        quick_relation_mode_ = true;
    }
    
    private void exitAnchorJumpMode() {
        if (anchor_jump_stack_.size() == 0) return;
        disableAnchorBar();
        updateAnchorBar();
        if (stored_before_anchor_jump_pos_ != null) {
            setMappedListPositionFromTop(stored_before_anchor_jump_pos_.position_, stored_before_anchor_jump_pos_.y_,
                    null);
        }
    }
    
    private int disableAnchorBar() {
        int entry_id = 0;
        if (anchor_jump_stack_.size() > 0) {
            entry_id = anchor_jump_stack_.get(0);
            anchor_jump_stack_.clear();
        }
        return entry_id;
    }
    
    private void updateAnchorBar() {
        class JumpAnchorSpan extends ClickableSpan {
            private int num_;
            
            public JumpAnchorSpan(int num) {
                num_ = num;
            }
            
            @Override
            public void onClick(View widget) {
                if (num_ > 0) ThreadEntryListActivity.this.onAnchorBarClicked(num_);
            }
        }
        
        NLabelView text_view = (NLabelView) findViewById(R.id.entry_anchor_stack);
        HorizontalScrollView box = (HorizontalScrollView) findViewById(R.id.entry_anchor_stack_box);
        
        if (anchor_jump_stack_.size() == 0) {
            disableAnchorBar();
            text_view.setText("");
            box.setVisibility(View.GONE);
            return;
        }
        box.setVisibility(View.VISIBLE);
        
        SpannableStringBuilder text = new SpannableStringBuilder();
        
        for (int num : anchor_jump_stack_) {
            text.append("[ ");
            String num_str = String.valueOf(num);
            int start_index = text.length();
            text.append(String.valueOf(num));
            text.setSpan(new JumpAnchorSpan(num), start_index, start_index + num_str.length(),
                    Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            text.append(" ]");
        }
        
        text_view.setText(text);
    }
    
    private void onAnchorBarClicked(int num) {
        if (anchor_jump_stack_.size() == 0) return;
        int index = anchor_jump_stack_.indexOf(num);
        if (index >= 0) {
            while (anchor_jump_stack_.size() - 1 > index) {
                anchor_jump_stack_.remove(anchor_jump_stack_.size() - 1);
            }
            if (anchor_jump_stack_.size() <= 1) {
                anchor_jump_stack_.clear();
            }
            
            updateAnchorBar();
        }
        setMappedListPosition(num - 1, null);
    }
    
    // ////////////////////////////////////////////////////////////
    // リロード
    // ////////////////////////////////////////////////////////////
    
    @Override
    protected void reloadList(final boolean force_reload) {
        reloadList(force_reload, false);
    }
    
    protected void reloadList(final boolean force_reload, final boolean no_cache) {
        if (!is_active_) return;
        if (!onBeginReload()) return;
        on_reloaded_callback_ = null;
        restore_position_on_cache_reloaded_ = ON_CACHE_RELOADED_RESTORE_STATE_NONE;
        reload_progress_max_ = maybe_online_count_ > DEFAULT_MAX_PROGRESS ? maybe_online_count_ : DEFAULT_MAX_PROGRESS;
        reload_progress_cur_ = 0;
        setProgress(0);
        setSecondaryProgress(0);
        ((ThreadEntryListAdapter) list_adapter_).setQuickShow(true);
        
        if (force_reload) showProgressBar(true);
        getAgent().reloadThreadEntryList(thread_data_.clone(), force_reload, no_cache, getFetchTask(force_reload));
    }
    
    private void reloadListSpecial() {
        if (!is_active_) return;
        if (!onBeginReload()) return;
        on_reloaded_callback_ = null;
        
        reload_progress_max_ = maybe_online_count_ > DEFAULT_MAX_PROGRESS ? maybe_online_count_ : DEFAULT_MAX_PROGRESS;
        reload_progress_cur_ = 0;
        setProgress(0);
        setSecondaryProgress(0);
        ((ThreadEntryListAdapter) list_adapter_).setQuickShow(true);
        
        showProgressBar(true);
        getAgent().reloadSpecialThreadEntryList(thread_data_.clone(), getTuboroidApplication().getAccountPref(),
                getFetchTask(true));
    }
    
    private ThreadEntryListAgent.ThreadEntryListAgentCallback getFetchTask(final boolean force_reload) {
        final ReloadTerminator reload_terminator = getNewReloadTerminator();
        return new ThreadEntryListAgent.ThreadEntryListAgentCallback() {
            
            private final ArrayList<ThreadEntryData> data_list_all_ = new ArrayList<ThreadEntryData>();
            
            @Override
            public void onThreadEntryListFetchedCompleted(final ThreadData thread_data, final boolean is_analyzed) {
                // 読み込みとの待ち合わせ
                postListViewAndUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (reload_terminator.is_terminated_) return;
                        storeOnMemoryCache(thread_data);
                        maybe_online_count_ = DEFAULT_MAX_PROGRESS;
                        setSecondaryProgress(10000);
                        ThreadEntryListActivity.this.onReloadCompleted(thread_data, force_reload, is_analyzed);
                    }
                });
            }
            
            private void storeOnMemoryCache(final ThreadData thread_data) {
                if (thread_data_ != null) getAgent().storeThreadEntryListAnalyzedCache(thread_data_, data_list_all_);
            }
            
            @Override
            public void onThreadEntryListFetchedByCache(final List<ThreadEntryData> data_list) {
                postListViewAndUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (reload_terminator.is_terminated_) return;
                        data_list_all_.addAll(data_list);
                        final int data_size = data_list.size();
                        ((ThreadEntryListAdapter) list_adapter_).setDataList(data_list, new Runnable() {
                            @Override
                            public void run() {
                                reload_progress_cur_ += data_size;
                                setSecondaryProgressBar(reload_progress_cur_, reload_progress_max_);
                                onReloadCacheCompleted(data_list);
                            }
                        });
                    }
                });
            }
            
            @Override
            public void onThreadEntryListFetchStarted(final ThreadData thread_data) {
                postListViewAndUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (reload_terminator.is_terminated_) return;
                        setSecondaryProgressBar(reload_progress_cur_, reload_progress_max_);
                        thread_data_ = thread_data;
                        onThreadFetchStarted();
                    }
                });
            }
            
            @Override
            public void onThreadEntryListFetched(final List<ThreadEntryData> data_list) {
                postListViewAndUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (reload_terminator.is_terminated_) return;
                        data_list_all_.addAll(data_list);
                        final int data_size = data_list.size();
                        ((ThreadEntryListAdapter) list_adapter_).addDataList(data_list, new Runnable() {
                            @Override
                            public void run() {
                                reload_progress_cur_ += data_size;
                                setSecondaryProgressBar(reload_progress_cur_, reload_progress_max_);
                            }
                        });
                        restorePositionInProgress();
                    }
                });
            }
            
            @Override
            public void onThreadEntryListClear() {
                postListViewAndUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (reload_terminator.is_terminated_) return;
                        data_list_all_.clear();
                        reload_progress_cur_ = 0;
                        setSecondaryProgressBar(reload_progress_cur_, reload_progress_max_);
                        ((ThreadEntryListAdapter) list_adapter_).clearData();
                    }
                });
            }
            
            @Override
            public void onInterrupted() {
                postListViewAndUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (reload_terminator.is_terminated_) return;
                        storeOnMemoryCache(thread_data_);
                        ((ThreadEntryListAdapter) list_adapter_).clearData();
                        onEndReload();
                    }
                });
            }
            
            @Override
            public void onDatDropped(final boolean is_permanently) {
                postListViewAndUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (reload_terminator.is_terminated_) return;
                        storeOnMemoryCache(thread_data_);
                        setProgress(10000);
                        setSecondaryProgress(10000);
                        showProgressBar(false);
                        on_reloaded_callback_ = new Runnable() {
                            @Override
                            public void run() {
                                ThreadEntryListActivity.this.onDatDropped(is_permanently);
                            }
                        };
                        ThreadEntryListActivity.this.onReloadCompleted(null, force_reload, false);
                    }
                });
            }
            
            @Override
            public void onConnectionFailed(final boolean connectionFailed) {
                postListViewAndUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (reload_terminator.is_terminated_) return;
                        storeOnMemoryCache(thread_data_);
                        setProgress(10000);
                        setSecondaryProgress(10000);
                        showProgressBar(false);
                        on_reloaded_callback_ = new Runnable() {
                            @Override
                            public void run() {
                                ThreadEntryListActivity.this.onConnectionFailed(connectionFailed);
                            }
                        };
                        ThreadEntryListActivity.this.onReloadCompleted(null, force_reload, false);
                    }
                });
            }
            
            @Override
            public void onConnectionOffline(ThreadData threadData) {
                onThreadEntryListFetchedCompleted(threadData, false);
            }
            
        };
    }
    
    private void onThreadFetchStarted() {}
    
    private void onReloadCacheCompleted(final List<ThreadEntryData> data_list) {
        showFooterView();
        if (thread_data_ != null) getAgent().updateThreadReadCount(thread_data_, data_list.size());
        hasInitialData(true);
        restorePositionInProgress();
    }
    
    private void restorePositionInProgress() {
        if (restore_position_on_cache_reloaded_ != ON_CACHE_RELOADED_RESTORE_STATE_NONE) return;
        
        final PositionData restore_position = getRestorePosition();
        restorePosition(new NCallback<Boolean>() {
            @Override
            public void run(final Boolean arg) {
                postListViewAndUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (arg) {
                            setListViewVisibility(true);
                            ListView view = getListView();
                            if (view != null) {
                                int first_position = view.getFirstVisiblePosition();
                                if (restore_position != null && restore_position.position_ == first_position) {
                                    restore_position_on_cache_reloaded_ = ON_CACHE_RELOADED_RESTORE_STATE_COMPLETE;
                                }
                                else {
                                    restore_position_on_cache_reloaded_ = first_position;
                                }
                            }
                        }
                    }
                });
            }
        });
    }
    
    private void onReloadCompleted(final ThreadData thread_data, final boolean force_reload, final boolean is_analyzed) {
        if (thread_data != null) thread_data_ = thread_data;
        
        if (thread_data_.online_count_ < thread_data_.cache_count_) {
            thread_data_.online_count_ = thread_data_.cache_count_;
        }
        if (force_reload) {
            ((ThreadEntryListAdapter) list_adapter_).setReadCount(thread_data_.read_count_);
            thread_data_.read_count_ = thread_data_.cache_count_;
        }
        ((ThreadEntryListAdapter) list_adapter_).setThreadData(thread_data_);
        long current_time = System.currentTimeMillis() / 1000;
        thread_data_.recent_time_ = current_time;
        setTitle(thread_data_.thread_name_);
        
        getAgent().updateThreadReadCount(thread_data_, thread_data_.read_count_);
        getAgent().updateThreadData(thread_data_, new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setFooterView();
                        if (list_adapter_ != null && jump_on_resume_after_post_) {
                            clearRestorePosition();
                            setRestorePosition(thread_data_.read_count_ - 1, 0);
                            restore_position_on_cache_reloaded_ = ON_CACHE_RELOADED_RESTORE_STATE_NONE;
                        }
                        
                        if (is_analyzed) {
                            onEndReload();
                        }
                        else {
                            analyzeForEndReload();
                        }
                    }
                });
            }
        });
    }
    
    @Override
    protected void onEndReload() {
        ListView view = getListView();
        final int first_position = view != null ? view.getFirstVisiblePosition() : -1;
        updateFooterRow(false, new Runnable() {
            @Override
            public void run() {
                ((ThreadEntryListAdapter) list_adapter_).setQuickShow(false);
                reapplyFilterOnReloaded(new Runnable() {
                    @Override
                    public void run() {
                        postListViewAndUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (restore_position_on_cache_reloaded_ == ON_CACHE_RELOADED_RESTORE_STATE_COMPLETE) {
                                    clearRestorePosition();
                                }
                                else if (restore_position_on_cache_reloaded_ != ON_CACHE_RELOADED_RESTORE_STATE_NONE
                                        && first_position != restore_position_on_cache_reloaded_) {
                                    // キャッシュ読み込み後のジャンプ地点からユーザが移動していたら
                                    // ユーザの操作を尊重してもうジャンプしない
                                    clearRestorePosition();
                                }
                                restore_position_on_cache_reloaded_ = ON_CACHE_RELOADED_RESTORE_STATE_NONE;
                                
                                ThreadEntryListActivity.super.onEndReload();
                            }
                        });
                    }
                });
            }
        });
    }
    
    protected void analyzeForEndReload() {
        ((ThreadEntryListAdapter) list_adapter_).analyzeThreadEntryList(new Runnable() {
            @Override
            public void run() {
                onEndReload();
            }
        }, new ThreadEntryData.AnalyzeThreadEntryListProgressCallback() {
            @Override
            public void onProgress(final int current, final int max) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setProgress(10000 * current / max);
                        if (current == max) showProgressBar(false);
                    }
                });
            }
        });
    }
    
    @Override
    protected void onEndReloadJumped() {
        postListViewAndUiThread(new Runnable() {
            @Override
            public void run() {
                if (is_active_ && list_adapter_ != null) {
                    ThreadEntryListAdapter list_adapter = (ThreadEntryListAdapter) list_adapter_;
                    list_adapter.notifyDataSetChanged();
                }
                reload_on_resume_ = false;
                jump_on_resume_after_post_ = false;
                if (on_reloaded_callback_ != null) {
                    on_reloaded_callback_.run();
                    on_reloaded_callback_ = null;
                }
                ThreadEntryListActivity.super.onEndReloadJumped();
            }
        });
    }
    
    private void onDatDropped(final boolean is_permanently) {
        if (!is_active_) return;
        
        if (!is_permanently && thread_data_.canRetryWithMaru(getTuboroidApplication().getAccountPref())) {
            onDatDroppedRetryWithMaru();
        }
        else if (!is_permanently && thread_data_.canRetryWithoutMaru()) {
            onDatDroppedRetryWithoutMaru();
        }
        else {
            onDatDroppedNoRetry();
        }
    }
    
    private void cancelDatDropped() {
        updateFilter(null);
        if (((ThreadEntryListAdapter) list_adapter_).getCount() == 0 && thread_data_.cache_count_ == 0
                && thread_data_.read_count_ == 0) {
            getAgent().deleteThreadEntryListCache(thread_data_, null);
            finish();
        }
    }
    
    private void onDatDroppedNoRetry() {
        if (!is_active_) return;
        
        if (thread_data_.thread_name_.length() > 0) {
            SimpleDialog.showYesNo(this, R.string.dialog_dat_dropped_title, R.string.dialog_dat_dropped_summary,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(ThreadEntryListActivity.this, SimilarThreadListActivity.class);
                            intent.setData(Uri.parse(thread_data_.getBoardSubjectsURI()));
                            intent.putExtra(SimilarThreadListActivity.KEY_SEARCH_KEY_NAME, thread_data_.thread_name_);
                            intent.putExtra(SimilarThreadListActivity.KEY_SEARCH_THREAD_ID, thread_data_.thread_id_);
                            intent.putExtra(SimilarThreadListActivity.KEY_SEARCH_NEXT_THREAD, true);
                            MigrationSDK5.Intent_addFlagNoAnimation(intent);
                            startActivity(intent);
                            finish();
                        }
                    }, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            cancelDatDropped();
                        }
                    });
        }
        else {
            SimpleDialog.showNotice(this, R.string.dialog_dat_dropped_title,
                    R.string.dialog_dat_dropped_summary_no_name, new Runnable() {
                        @Override
                        public void run() {
                            cancelDatDropped();
                        }
                    });
        }
    }
    
    private void onDatDroppedRetryWithMaru() {
        onDatDroppedRetryImpl(R.string.dialog_dat_dropped_summary_retry_with_maru,
                R.string.dialog_dat_dropped_summary_retry_with_maru_no_name,
                R.string.dialog_label_dat_dropped_retry_with_maru);
    }
    
    private void onDatDroppedRetryWithoutMaru() {
        onDatDroppedRetryImpl(R.string.dialog_dat_dropped_summary_retry_without_maru,
                R.string.dialog_dat_dropped_summary_retry_without_maru_no_name,
                R.string.dialog_label_dat_dropped_retry_without_maru);
    }
    
    private void onDatDroppedRetryImpl(int summary_with_find_next, int summary_no_name, int label_retry) {
        if (!is_active_) return;
        
        if (thread_data_.cache_count_ == 0) {
            ManagedToast.raiseToast(getApplicationContext(), R.string.toast_fetch_dat_by_storage);
            reloadListSpecial();
            return;
        }
        
        if (thread_data_.thread_name_.length() > 0) {
            SimpleDialog.showYesEtcNo(this, R.string.dialog_dat_dropped_title, summary_with_find_next,
                    R.string.dialog_label_dat_dropped_find_similar, label_retry, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(ThreadEntryListActivity.this, SimilarThreadListActivity.class);
                            intent.setData(Uri.parse(thread_data_.getBoardSubjectsURI()));
                            intent.putExtra(SimilarThreadListActivity.KEY_SEARCH_KEY_NAME, thread_data_.thread_name_);
                            intent.putExtra(SimilarThreadListActivity.KEY_SEARCH_THREAD_ID, thread_data_.thread_id_);
                            intent.putExtra(SimilarThreadListActivity.KEY_SEARCH_NEXT_THREAD, true);
                            MigrationSDK5.Intent_addFlagNoAnimation(intent);
                            startActivity(intent);
                            finish();
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            reloadListSpecial();
                        }
                    }, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            cancelDatDropped();
                        }
                    });
        }
        else {
            SimpleDialog.showYesNo(this, R.string.dialog_dat_dropped_title, summary_no_name,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            reloadListSpecial();
                        }
                    }, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            cancelDatDropped();
                        }
                    });
        }
    }
    
    private void onConnectionFailed(boolean connectionFailed) {
        if (!is_active_) return;
        
        if (connectionFailed) {
            ManagedToast.raiseToast(getApplicationContext(), R.string.toast_reload_entry_list_failed);
        }
    }
    
    protected TuboroidApplication.ViewConfig getListFontPref() {
        return getTuboroidApplication().view_config_;
    }
    
    // ////////////////////////////////////////////////////////////
    // フィルタ
    // ////////////////////////////////////////////////////////////
    public static class ParcelableFilterData implements Parcelable {
        final public int type_;
        final public String string_filter_word_;
        final public String author_id_;
        final public long target_entry_id_;
        
        public static int TYPE_NONE = 0;
        public static int TYPE_STRING = 1;
        public static int TYPE_AUTHOR_ID = 2;
        public static int TYPE_RELATION = 3;
        
        public ParcelableFilterData() {
            type_ = TYPE_NONE;
            string_filter_word_ = null;
            author_id_ = null;
            target_entry_id_ = 0;
        }
        
        public ParcelableFilterData(int type, String stringFilterWord, String author_id, long target_entry_id) {
            super();
            type_ = type;
            string_filter_word_ = stringFilterWord;
            author_id_ = author_id;
            target_entry_id_ = target_entry_id;
        }
        
        @Override
        public int describeContents() {
            return 0;
        }
        
        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(type_);
            dest.writeString(string_filter_word_);
            dest.writeString(author_id_);
            dest.writeLong(target_entry_id_);
        }
        
        public static final Parcelable.Creator<ParcelableFilterData> CREATOR = new Parcelable.Creator<ParcelableFilterData>() {
            @Override
            public ParcelableFilterData createFromParcel(Parcel in) {
                return new ParcelableFilterData(in);
            }
            
            @Override
            public ParcelableFilterData[] newArray(int size) {
                return new ParcelableFilterData[size];
            }
        };
        
        private ParcelableFilterData(Parcel in) {
            type_ = in.readInt();
            string_filter_word_ = in.readString();
            author_id_ = in.readString();
            target_entry_id_ = in.readLong();
        }
        
    }
    
    static class BaseFilter implements ThreadEntryListAdapter.Filter<ThreadEntryData> {
        @Override
        public boolean filter(ThreadEntryData data) {
            if (data.isGone()) return false;
            return true;
        }
    }
    
    @Override
    protected void updateFilter(String filter_string) {
        if (filter_string == null || filter_string.length() == 0) {
            updateFilterNone();
            return;
        }
        updateStringFilter(filter_string);
    }
    
    private void updateParcelableFilter(ParcelableFilterData filter) {
        if (!is_active_) return;
        
        if (filter.type_ == ParcelableFilterData.TYPE_STRING) {
            updateStringFilter(filter.string_filter_word_);
        }
        else if (filter.type_ == ParcelableFilterData.TYPE_AUTHOR_ID) {
            updateFilterByAuthorID(filter_.target_entry_id_, filter_.author_id_);
        }
        else if (filter.type_ == ParcelableFilterData.TYPE_RELATION) {
            updateFilterByRelation(filter_.target_entry_id_, filter_.target_entry_id_);
        }
        else {
            updateFilterNone();
        }
    }
    
    private void updateFilterNone() {
        if (!is_active_) return;
        
        quick_relation_mode_ = false;
        filter_ = new ParcelableFilterData();
        updateFilterBar();
        
        ((ThreadEntryListAdapter) list_adapter_).setFilter(new BaseFilter(), new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        exitFilterMode();
                    }
                });
            }
        });
    }
    
    private void updateStringFilter(String filter_string) {
        if (!is_active_) return;
        
        quick_relation_mode_ = false;
        filter_ = new ParcelableFilterData(ParcelableFilterData.TYPE_STRING, filter_string, null, 0);
        updateFilterBar();
        
        PositionData pos_data = getCurrentPosition();
        onEntryFilterMode(pos_data.position_ + 1, pos_data.y_);
        final String filter_lc = filter_.string_filter_word_.toLowerCase();
        ((ThreadEntryListAdapter) list_adapter_).setFilter(new BaseFilter() {
            @Override
            public boolean filter(ThreadEntryData data) {
                if (data.entry_body_.toLowerCase().indexOf(filter_lc) == -1) return false;
                return super.filter(data);
            }
        }, new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setListPositionTop(null);
                    }
                });
            }
        });
    }
    
    private void updateFilterByAuthorID(final ThreadEntryData entry_data) {
        if (!is_active_) return;
        
        updateFilterByAuthorID(entry_data.entry_id_, entry_data.author_id_);
    }
    
    private void updateFilterByAuthorID(final long target_entry_id, final String target_author_id) {
        if (!is_active_) return;
        
        onEntryFilterMode((int) target_entry_id, 0);
        
        quick_relation_mode_ = false;
        filter_ = new ParcelableFilterData(ParcelableFilterData.TYPE_AUTHOR_ID, null, target_author_id, target_entry_id);
        updateFilterBar();
        
        ((ThreadEntryListAdapter) list_adapter_).setFilter(new BaseFilter() {
            @Override
            public boolean filter(ThreadEntryData data) {
                if (!target_author_id.equals(data.author_id_)) return false;
                return super.filter(data);
            }
        }, new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setMappedListPosition((int) (target_entry_id - 1), null);
                    }
                });
            }
        });
    }
    
    private void updateFilterByRelation(final long target_entry_id, final long jump_entry_id) {
        if (!is_active_) return;
        
        updateFilterByRelation(target_entry_id, new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setMappedListPosition((int) (jump_entry_id - 1), null);
                    }
                });
            }
        });
    }
    
    private void reapplyFilterOnReloaded(final Runnable callback) {
        if (filter_.type_ == ParcelableFilterData.TYPE_RELATION) {
            updateFilterByRelation(filter_.target_entry_id_, callback);
        }
        else {
            if (callback != null) callback.run();
        }
    }
    
    private void updateFilterByRelation(final long target_entry_id, final Runnable callback) {
        if (!is_active_) return;
        
        onEntryFilterMode((int) target_entry_id, 0);
        
        filter_ = new ParcelableFilterData(ParcelableFilterData.TYPE_RELATION, null, null, target_entry_id);
        updateFilterBar();
        
        final HashSet<Long> result_id_map = new HashSet<Long>();
        final HashMap<Long, ThreadEntryData> inner_data_map = new HashMap<Long, ThreadEntryData>();
        
        ((ThreadEntryListAdapter) list_adapter_).setFilter(new ThreadEntryListAdapter.PrepareFilter<ThreadEntryData>() {
            @Override
            public void prepare(ArrayList<ThreadEntryData> inner_data_list) {
                ThreadEntryData target_data = null;
                for (ThreadEntryData data : inner_data_list) {
                    inner_data_map.put(data.entry_id_, data);
                    if (data.entry_id_ == target_entry_id) {
                        target_data = data;
                    }
                }
                if (target_data == null) return;
                check(target_data);
            }
            
            private void check(ThreadEntryData target_data) {
                if (result_id_map.contains(target_data.entry_id_)) return;
                result_id_map.add(target_data.entry_id_);
                for (Long next_id : target_data.back_anchor_list_) {
                    ThreadEntryData data = inner_data_map.get(next_id);
                    if (data != null) check(data);
                }
                
                for (Long next_id : target_data.forward_anchor_list_) {
                    ThreadEntryData data = inner_data_map.get(next_id);
                    if (data != null) check(data);
                }
            }
            
        }, new BaseFilter() {
            @Override
            public boolean filter(ThreadEntryData data) {
                if (!result_id_map.contains(data.entry_id_)) return false;
                return super.filter(data);
            }
        }, callback);
    }
}
