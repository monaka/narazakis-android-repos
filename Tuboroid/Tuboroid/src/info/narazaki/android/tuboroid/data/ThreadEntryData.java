package info.narazaki.android.tuboroid.data;

import info.narazaki.android.lib.adapter.NListAdapterDataInterface;
import info.narazaki.android.lib.list.ListUtils;
import info.narazaki.android.lib.text.HtmlUtils;
import info.narazaki.android.lib.text.span.SpanSpec;
import info.narazaki.android.lib.text.span.SpanifyAdapter;
import info.narazaki.android.lib.text.span.WebURLFilter;
import info.narazaki.android.lib.view.NLabelView;
import info.narazaki.android.tuboroid.R;
import info.narazaki.android.tuboroid.TuboroidApplication;
import info.narazaki.android.tuboroid.TuboroidApplication.ViewConfig;
import info.narazaki.android.tuboroid.agent.ImageFetchAgent;
import info.narazaki.android.tuboroid.agent.TuboroidAgent;
import info.narazaki.android.tuboroid.text.span.EntryAnchorFilter;
import info.narazaki.android.tuboroid.text.span.EntryAnchorSpanSpec;

import java.io.File;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.text.Layout;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.URLSpan;
import android.util.FloatMath;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

public class ThreadEntryData implements NListAdapterDataInterface {
    private static final String TAG = "ThreadEntryData";
    
    private static final int MAX_BACK_LINKS = 10;
    
    public static final String IS_AA = "1";
    
    private static final EntryAnchorFilter entry_anchor_finder_ = new EntryAnchorFilter();
    private static final WebURLFilter web_url_finder_ = new WebURLFilter(new String[] { "http", "ttp" });
    private static String[] image_url_suffix_list = new String[] { ".gif", ".jpg", ".jpeg", ".png" };
    
    public long entry_id_;
    
    public String author_name_;
    public String author_mail_;
    public String entry_body_;
    
    public String author_id_;
    public String author_be_;
    public String entry_time_;
    
    public int author_id_count_;
    
    public boolean entry_is_aa_;
    
    public String forward_anchor_list_str_;
    public long[] forward_anchor_list_;
    
    public ArrayList<Long> back_anchor_list_;
    private boolean is_img_uri_list_parsed_;
    
    private static class ImageData {
        final String url_;
        boolean file_checked_;
        volatile boolean is_enabled_;
        volatile boolean is_failed_;
        
        public ImageData(String url) {
            super();
            url_ = url;
            file_checked_ = false;
            is_enabled_ = false;
            is_failed_ = false;
        }
        
    }
    
    private ArrayList<ImageData> image_list_;
    
    public int ng_flag_;
    
    static class SpannableCache {
        private CharSequence cache_;
        private ViewStyle view_style_;
        private TuboroidApplication.ViewConfig view_config_;
        
        public SpannableCache(CharSequence cache, ViewStyle viewStyle, ViewConfig viewConfig) {
            super();
            cache_ = cache;
            view_style_ = viewStyle;
            view_config_ = viewConfig;
        }
        
        public boolean isValid(ViewStyle viewStyle, ViewConfig viewConfig) {
            if (cache_ == null || view_style_ != viewStyle || view_config_ != viewConfig) return false;
            return true;
        }
        
        public CharSequence get() {
            return cache_;
        }
        
    }
    
    private SpannableCache entry_header_cache_ = null;
    private SoftReference<SpannableCache> entry_body_cache_ref_ = null;
    private SoftReference<SpannableCache> entry_rev_cache_ref_ = null;
    
    public ThreadEntryData(boolean cached_data, long entry_id, String author_name, String author_mail,
            String entry_body, String author_id, String author_be, String entry_time, String entry_is_aa,
            String forward_anchor_list_str) {
        super();
        entry_id_ = entry_id;
        author_name_ = author_name;
        author_mail_ = author_mail;
        entry_body_ = entry_body;
        
        author_id_ = author_id;
        author_be_ = author_be;
        entry_time_ = entry_time;
        
        author_id_count_ = 0;
        entry_is_aa_ = entry_is_aa.equals(IS_AA);
        forward_anchor_list_str_ = forward_anchor_list_str;
        
        if (cached_data) {
            initCachedData();
        }
        else {
            initNewData();
        }
        
        back_anchor_list_ = new ArrayList<Long>();
        
        is_img_uri_list_parsed_ = false;
        image_list_ = new ArrayList<ImageData>();
        
        ng_flag_ = IgnoreData.TYPE.NONE;
    }
    
    private void initCachedData() {
        // アンカーリスト初期化
        forward_anchor_list_ = ListUtils.split(",", forward_anchor_list_str_, 0L);
    }
    
    private String shrinkBody(String entry_body) {
        return HtmlUtils.shrinkHtml(entry_body, true);
    }
    
    private void initNewData() {
        // データのHTML削除
        author_name_ = HtmlUtils.stripAllHtmls(author_name_, false);
        author_mail_ = HtmlUtils.stripAllHtmls(author_mail_, false);
        entry_body_ = shrinkBody(entry_body_);
        
        author_id_ = HtmlUtils.stripAllHtmls(author_id_, false);
        author_be_ = HtmlUtils.stripAllHtmls(author_be_, false);
        entry_time_ = HtmlUtils.stripAllHtmls(entry_time_, false);
        
        entry_is_aa_ = is2chAsciiArt(entry_body_);
        
        // アンカーリスト初期化
        EntryAnchorSpanSpec[] founds = entry_anchor_finder_.gatherNative(entry_body_);
        StringBuilder buf = new StringBuilder();
        
        forward_anchor_list_ = new long[founds.length];
        for (int i = 0; i < founds.length; i++) {
            long id = founds[i].target_id_;
            for (int j = 0; j < i; j++) {
                if (forward_anchor_list_[j] == id) {
                    id = 0;
                    break;
                }
            }
            if (id > 0 && id < entry_id_) {
                forward_anchor_list_[i] = founds[i].target_id_;
                if (buf.length() > 0) buf.append(',');
                buf.append(id);
            }
        }
        forward_anchor_list_str_ = buf.toString();
    }
    
    public boolean hasShownThumbnails() {
        for (ImageData data : image_list_) {
            if (data.is_enabled_) return true;
        }
        return false;
    }
    
    private void parseImageUrl() {
        if (is_img_uri_list_parsed_) return;
        
        final List<ImageData> image_list = image_list_;
        image_list.clear();
        
        SpanSpec[] founds = web_url_finder_.gather(entry_body_, null);
        for (SpanSpec found : founds) {
            String uri = found.text_;
            for (String suffix : image_url_suffix_list) {
                if (uri.endsWith(suffix)) {
                    if (uri.startsWith("ttp")) uri = "h" + uri;
                    image_list.add(new ImageData(uri));
                    break;
                }
            }
        }
        is_img_uri_list_parsed_ = true;
    }
    
    private boolean getImageEnabled(final TuboroidAgent agent, final ThreadData thread_data, final int index) {
        if (image_list_.size() < index) return false;
        ImageData image_data = image_list_.get(index);
        if (image_data.is_enabled_) return true;
        if (image_data.file_checked_) return false;
        
        final boolean enabled = agent.hasImageCacheFile(thread_data, this, index);
        image_data.is_enabled_ = enabled;
        image_data.file_checked_ = true;
        return enabled;
    }
    
    public File getImageLocalFile(TuboroidApplication application, ThreadData thread_data, int image_index) {
        if (image_list_.size() < image_index) return null;
        ImageData image_data = image_list_.get(image_index);
        int dot_index = image_data.url_.lastIndexOf('.');
        if (dot_index == -1) return null;
        
        String dot_ext = image_data.url_.substring(dot_index).toLowerCase();
        
        String filename = "image_" + entry_id_ + "_" + image_index + dot_ext;
        
        return thread_data.getLocalAttachFile(application, filename);
    }
    
    public boolean isNG() {
        return IgnoreData.isNG(ng_flag_);
    }
    
    public boolean isGone() {
        return IgnoreData.isGone(ng_flag_);
    }
    
    public static interface AnalyzeThreadEntryListProgressCallback {
        public void onProgress(int current, int max);
    }
    
    private final static int ANALYZE_PROGRESS_UI_THREAD = 7;
    private final static int ANALYZE_PROGRESS_EXT_THREAD = 5;
    private final static int ANALYZE_PROGRESS_MAX = ANALYZE_PROGRESS_UI_THREAD + ANALYZE_PROGRESS_EXT_THREAD;
    
    public static void analyzeThreadEntryList(final TuboroidAgent agent, final ThreadData thread_data,
            final TuboroidApplication.ViewConfig view_config, final ViewStyle style,
            final List<ThreadEntryData> data_list_orig, final AnalyzeThreadEntryListProgressCallback callback) {
        
        final int data_list_size = data_list_orig.size();
        final ThreadEntryData[] data_list = new ThreadEntryData[data_list_size];
        data_list_orig.toArray(data_list);
        HashMap<String, ThreadEntryData> author_id_map = new HashMap<String, ThreadEntryData>(data_list_size);
        
        int progress = 1;
        callback.onProgress(progress++, ANALYZE_PROGRESS_MAX);
        for (ThreadEntryData data : data_list) {
            synchronized (thread_data) {
                // 状態クリア
                data.back_anchor_list_ = new ArrayList<Long>();
                
                // バックアンカーの抽出
                for (long id : data.forward_anchor_list_) {
                    int iid = (int) id;
                    if (data_list_size >= iid && iid > 0) {
                        data_list[iid - 1].back_anchor_list_.add(data.entry_id_);
                    }
                }
            }
        }
        
        // レス数を数える
        callback.onProgress(progress++, ANALYZE_PROGRESS_MAX);
        for (ThreadEntryData data : data_list) {
            synchronized (thread_data) {
                ThreadEntryData orig = author_id_map.get(data.author_id_);
                if (orig != null) {
                    orig.author_id_count_++;
                }
                else {
                    author_id_map.put(data.author_id_, data);
                    data.author_id_count_ = 1;
                }
            }
        }
        
        // 画像チェック
        callback.onProgress(progress++, ANALYZE_PROGRESS_MAX);
        for (ThreadEntryData data : data_list) {
            synchronized (data) {
                data.parseImageUrl();
            }
        }
        callback.onProgress(progress++, ANALYZE_PROGRESS_MAX);
        for (ThreadEntryData data : data_list) {
            synchronized (data) {
                int data_image_list_size = data.image_list_.size();
                for (int i = 0; i < data_image_list_size; i++) {
                    data.getImageEnabled(agent, thread_data, i);
                }
            }
        }
        
        callback.onProgress(progress++, ANALYZE_PROGRESS_MAX);
        for (ThreadEntryData data : data_list) {
            // 数えたレス数を反映させる
            data.author_id_count_ = author_id_map.get(data.author_id_).author_id_count_;
        }
        
        // 逆アンカー
        callback.onProgress(progress++, ANALYZE_PROGRESS_MAX);
        if (view_config.use_back_anchor_) {
            for (ThreadEntryData data : data_list) {
                synchronized (data) {
                    if (data.back_anchor_list_.size() > 0) {
                        data.createSpannedRevLink(view_config, style);
                    }
                }
            }
        }
        
        // ヘッダキャッシュの消去
        for (ThreadEntryData data : data_list) {
            data.clearSpannedEntryHeader();
        }
        callback.onProgress(ANALYZE_PROGRESS_UI_THREAD, ANALYZE_PROGRESS_MAX);
        
        // ヘッダキャッシュの再構築
        analyzeThreadEntryListExt(view_config, style, data_list, callback);
    }
    
    private static ExecutorService ext_analyze_thread_executor_;
    
    private synchronized static void analyzeThreadEntryListExt(final TuboroidApplication.ViewConfig view_config,
            final ViewStyle style, final ThreadEntryData[] data_list,
            final AnalyzeThreadEntryListProgressCallback callback) {
        if (ext_analyze_thread_executor_ == null) {
            ext_analyze_thread_executor_ = Executors.newSingleThreadExecutor();
        }
        
        ext_analyze_thread_executor_.submit(new Runnable() {
            @Override
            public void run() {
                int progress_interval = data_list.length / ANALYZE_PROGRESS_EXT_THREAD + 1;
                int progress_remain = progress_interval;
                int progress = 1;
                for (ThreadEntryData data : data_list) {
                    // ヘッダを処理
                    data.getSpannedEntryHeader(view_config, style);
                    progress_remain--;
                    if (progress_remain == 0) {
                        callback.onProgress(ANALYZE_PROGRESS_UI_THREAD + progress, ANALYZE_PROGRESS_MAX);
                        progress++;
                        progress_remain = progress_interval;
                    }
                }
                callback.onProgress(ANALYZE_PROGRESS_MAX, ANALYZE_PROGRESS_MAX);
            }
        });
    }
    
    @Override
    public long getId() {
        return entry_id_;
    }
    
    static public interface OnAnchorClickedCallback {
        public void onNumberAnchorClicked(int jump_from, int jump_to);
        
        public void onThreadLinkClicked(Uri uri);
        
        public void onBoardLinkClicked(Uri uri);
    }
    
    static class ViewTag {
        NLabelView header_view;
        NLabelView entry_body_view;
        LinearLayout rev_anchor_box_view;
        NLabelView rev_anchor_view;
        TableLayout thumbnail_box;
        int header_bgcolor = -1;
        boolean is_aa_font = false;
        int font_size = -1;
        
        void setHeaderBackgroundColor(int color) {
            if (header_bgcolor != color) {
                header_view.setBackgroundColor(color);
                header_bgcolor = color;
            }
        }
    }
    
    private static View.OnLongClickListener createLongClickDelegate(final View view) {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (v == null) return false;
                if (!v.isShown()) return false;
                return view.performLongClick();
            }
        };
    }
    
    public static View initView(final View view, TuboroidApplication.ViewConfig view_config, ViewStyle style) {
        View.OnLongClickListener long_click_delegate = createLongClickDelegate(view);
        
        ViewTag tag = new ViewTag();
        view.setTag(tag);
        
        tag.header_view = (NLabelView) view.findViewById(R.id.entry_header);
        tag.header_view.setTextSize(view_config.entry_header_);
        
        tag.entry_body_view = (NLabelView) view.findViewById(R.id.entry_body);
        tag.entry_body_view.setLinkTextColor(style.link_color_);
        tag.entry_body_view.setTextSize(view_config.entry_body_);
        tag.font_size = view_config.entry_body_;
        tag.entry_body_view.getPaint().setSubpixelText(true);
        tag.entry_body_view.setTouchBackgroundColorSpan(style.on_clicked_bgcolor_span_);
        tag.entry_body_view.setTouchMargin(view_config.touch_margin_ > 0);
        
        tag.rev_anchor_view = (NLabelView) view.findViewById(R.id.entry_rev_anchor);
        tag.rev_anchor_box_view = (LinearLayout) view.findViewById(R.id.entry_rev_anchor_box);
        if (view_config.use_back_anchor_) {
            tag.rev_anchor_view.setTextSize(view_config.entry_body_);
            tag.rev_anchor_view.setLinkTextColor(style.link_color_);
            tag.rev_anchor_view.setTouchBackgroundColorSpan(style.on_clicked_bgcolor_span_);
            tag.rev_anchor_view.setTouchMargin(view_config.touch_margin_ > 0);
            tag.rev_anchor_box_view.setVisibility(View.VISIBLE);
        }
        else {
            tag.rev_anchor_box_view.setVisibility(View.GONE);
        }
        
        tag.thumbnail_box = (TableLayout) view.findViewById(R.id.thread_list_thumbnail_box);
        tag.thumbnail_box.setOnLongClickListener(long_click_delegate);
        tag.thumbnail_box.setLongClickable(false);
        
        return view;
    }
    
    public interface ImageViewerLauncher {
        void onRequired(final ThreadData thread_data, final String image_local_filename, final String image_uri);
    }
    
    public synchronized View setView(final TuboroidAgent agent, final ThreadData thread_data, final View view,
            final ViewGroup parent, int read_count, final TuboroidApplication.ViewConfig view_config,
            final ViewStyle style, final boolean is_quick_show) {
        ViewTag tag = (ViewTag) view.getTag();
        int parent_width = parent.getWidth();
        
        // 透明あぼーん判定
        if (isGone()) {
            view.setVisibility(View.GONE);
            return view;
        }
        else {
            view.setVisibility(View.VISIBLE);
        }
        
        // ////////////////////////////////////////////////////////////
        // ヘッダ部分
        if (entry_id_ <= read_count) {
            tag.setHeaderBackgroundColor(style.style_header_color_default_);
        }
        else {
            tag.setHeaderBackgroundColor(style.style_header_color_emphasis_);
        }
        
        CharSequence spanned = getSpannedEntryHeader(view_config, style);
        tag.header_view.setText(spanned);
        
        // ////////////////////////////////////////////////////////////
        // ボディ
        if (isNG()) {
            // あぼーん
            SpannableString ignored_token = new SpannableString(view.getResources().getString(
                    R.string.text_ignored_entry));
            ignored_token.setSpan(style.ignored_span_, 0, ignored_token.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            tag.entry_body_view.setText(ignored_token);
        }
        else {
            tag.entry_body_view.setText(getSpannedEntryBody(view_config, style));
            if (tag.font_size != view_config.entry_body_) {
                tag.entry_body_view.setTextSize(view_config.entry_body_);
                tag.font_size = view_config.entry_body_;
            }
            if (entry_is_aa_) {
                if (!tag.is_aa_font) {
                    Typeface aa_font = view_config.getAAFont();
                    if (aa_font != null) {
                        tag.entry_body_view.setTypeface(aa_font);
                        tag.is_aa_font = true;
                    }
                }
                TextPaint paint = tag.entry_body_view.getPaint();
                int desired_width = (int) FloatMath.ceil(Layout.getDesiredWidth(entry_body_, paint));
                int aa_font_rate = parent_width * 95 / desired_width;
                int real_aa_font_rate = (view_config.entry_aa_size_rate_ > aa_font_rate) ? view_config.entry_aa_size_rate_
                        : aa_font_rate;
                if (real_aa_font_rate < 100) {
                    int new_size = view_config.entry_body_ * real_aa_font_rate / 100;
                    tag.entry_body_view.setTextSize(view_config.entry_body_ * real_aa_font_rate / 100);
                    tag.font_size = new_size;
                }
            }
            else {
                if (tag.is_aa_font) {
                    tag.entry_body_view.setTypeface(Typeface.DEFAULT);
                    tag.is_aa_font = false;
                }
            }
        }
        
        // ////////////////////////////////////////////////////////////
        // 逆リンク
        if (view_config.use_back_anchor_) {
            if (back_anchor_list_.size() > 0) {
                tag.rev_anchor_view.setText(getSpannedRevLink(view_config, style));
                tag.rev_anchor_box_view.setVisibility(View.VISIBLE);
            }
            else {
                tag.rev_anchor_box_view.setVisibility(View.GONE);
            }
        }
        
        // ////////////////////////////////////////////////////////////
        // サムネ
        if (is_quick_show || image_list_.size() == 0) {
            // サムネ無し
            if (tag.entry_body_view.isLongClickable()) tag.entry_body_view.setLongClickable(false);
            
            if (tag.thumbnail_box.getChildCount() > 0) tag.thumbnail_box.removeAllViews();
            tag.thumbnail_box.setVisibility(View.GONE);
        }
        else {
            // サムネ有り
            int thumbnail_size = view_config.real_thumbnail_size_;
            final int thumbnail_cols = (thumbnail_size > 0 && parent_width > thumbnail_size) ? parent_width
                    / thumbnail_size : 1;
            
            if (!tag.entry_body_view.isLongClickable()) tag.entry_body_view.setLongClickable(true);
            
            final int thumbnail_rows = (image_list_.size() - 1) / thumbnail_cols + 1;
            
            initThumbnailsBox(tag.thumbnail_box, thumbnail_cols, thumbnail_rows, view_config);
            rebuildThumbnails(tag.thumbnail_box, thumbnail_cols, thumbnail_rows, agent, thread_data, view_config, style);
            tag.thumbnail_box.setVisibility(View.VISIBLE);
        }
        
        return view;
    }
    
    private void initThumbnailsBox(final TableLayout thumbnail_box, final int thumbnail_cols, final int thumbnail_rows,
            final TuboroidApplication.ViewConfig view_config) {
        if (thumbnail_box.getChildCount() > 0) thumbnail_box.removeAllViews();
        
        ListIterator<ImageData> it = image_list_.listIterator();
        for (int i = 0; i < thumbnail_rows; i++) {
            TableRow table_row = new TableRow(thumbnail_box.getContext());
            for (int j = 0; j < thumbnail_cols; j++) {
                if (it.hasNext()) {
                    final ImageButton image_button = new ImageButton(thumbnail_box.getContext());
                    
                    image_button.setBackgroundResource(android.R.color.transparent);
                    image_button.setPadding(0, 5, 0, 5);
                    image_button.setMinimumWidth(view_config.thumbnail_size_);
                    image_button.setMaxWidth(view_config.thumbnail_size_);
                    image_button.setScaleType(ScaleType.CENTER);
                    image_button.setOnClickListener(null);
                    image_button.setLongClickable(false);
                    table_row.addView(image_button);
                    it.next();
                }
            }
            thumbnail_box.addView(table_row);
        }
    }
    
    public void deleteThumbnails(final TuboroidApplication application, final TuboroidAgent agent,
            final ThreadData thread_data) {
        for (int i = 0; i < image_list_.size(); i++) {
            ImageData image_data = image_list_.get(i);
            if (image_data.is_enabled_) {
                image_data.is_enabled_ = false;
                final File local_image_file = getImageLocalFile(application, thread_data, i);
                try {
                    local_image_file.delete();
                }
                catch (SecurityException e) {
                }
                agent.deleteImage(local_image_file);
            }
        }
    }
    
    private void rebuildThumbnails(final TableLayout thumbnail_box, final int thumbnail_cols, final int thumbnail_rows,
            final TuboroidAgent agent, final ThreadData thread_data, final TuboroidApplication.ViewConfig view_config,
            final ViewStyle style) {
        int image_index = 0;
        for (int i = 0; i < thumbnail_rows; i++) {
            final TableRow table_row = (TableRow) thumbnail_box.getChildAt(i);
            for (int j = 0; j < thumbnail_cols; j++) {
                if (image_index < image_list_.size()) {
                    final ImageButton image_button = (ImageButton) table_row.getChildAt(j);
                    final ImageData image_data = image_list_.get(image_index);
                    
                    if (image_data.is_failed_) {
                        image_button.setImageResource(R.drawable.ic_btn_load_image_failed);
                        setShowThumbnailButton(image_button, agent, thread_data, image_index, view_config, style);
                    }
                    else if (image_data.is_enabled_) {
                        showThumbnail(image_button, agent, thread_data, image_index, view_config, style);
                    }
                    else {
                        image_button.setImageResource(R.drawable.ic_btn_show_thumbnail);
                        setShowThumbnailButton(image_button, agent, thread_data, image_index, view_config, style);
                    }
                    image_index++;
                }
            }
        }
    }
    
    private void showThumbnail(final ImageButton image_button, final TuboroidAgent agent, final ThreadData thread_data,
            final int image_index, final TuboroidApplication.ViewConfig view_config, final ViewStyle style) {
        if (thread_data == null) return;
        
        final ImageData image_data = image_list_.get(image_index);
        image_data.is_enabled_ = true;
        
        final Context context = image_button.getContext();
        final String image_uri = image_data.url_;
        final File local_image_file = getImageLocalFile(agent.getApplication(), thread_data, image_index);
        
        final WeakReference<ImageButton> image_button_ref = new WeakReference<ImageButton>(image_button);
        
        final ImageFetchAgent.BitmapFetchedCallback callback = new ImageFetchAgent.BitmapFetchedCallback() {
            ClipDrawable progress_bar_ = null;
            
            @Override
            public void onBegeinNoCache() {
                final ImageButton image_button_tmp = image_button_ref.get();
                if (image_button_tmp == null) return;
                AnimationDrawable progress_anim = (AnimationDrawable) context.getResources().getDrawable(
                        R.anim.ic_btn_load_image_amination);
                image_button_tmp.setImageDrawable(progress_anim);
                progress_anim.start();
            }
            
            @Override
            public void onCacheFetched(Bitmap bitmap) {
                final ImageButton image_button_tmp = image_button_ref.get();
                if (image_button_tmp == null) return;
                image_button_tmp.setImageBitmap(bitmap);
                image_button_tmp.setOnClickListener(createThumbnailOnClickListener(thread_data, style, image_uri,
                        local_image_file));
                image_data.is_failed_ = false;
            }
            
            @Override
            public void onFetched(final Bitmap bitmap) {
                final ImageButton image_button_tmp = image_button_ref.get();
                if (image_button_tmp == null) return;
                image_button_tmp.post(new Runnable() {
                    @Override
                    public void run() {
                        image_button_tmp.setImageBitmap(bitmap);
                        image_button_tmp.setOnClickListener(createThumbnailOnClickListener(thread_data, style,
                                image_uri, local_image_file));
                        image_data.is_failed_ = false;
                    }
                });
            }
            
            @Override
            public void onFailed() {
                final ImageButton image_button_tmp = image_button_ref.get();
                if (image_button_tmp == null) return;
                image_button_tmp.post(new Runnable() {
                    @Override
                    public void run() {
                        image_button_tmp.setImageResource(R.drawable.ic_btn_load_image_failed);
                        setShowThumbnailButton(image_button, agent, thread_data, image_index, view_config, style);
                        image_data.is_failed_ = true;
                    }
                });
            }
            
            @Override
            public void onBeginOnlineFetch() {
                
                final ImageButton image_button_tmp = image_button_ref.get();
                if (image_button_tmp == null) return;
                image_button_tmp.post(new Runnable() {
                    @Override
                    public void run() {
                        initProgressBar(image_button_tmp, 0, 0);
                    }
                });
                
            }
            
            private void initProgressBar(final ImageButton image_button, final int current_length,
                    final int content_length) {
                Drawable[] drawables = new Drawable[3];
                AnimationDrawable progress_anim = (AnimationDrawable) context.getResources().getDrawable(
                        R.anim.ic_btn_load_image_amination);
                drawables[0] = progress_anim;
                Drawable progress_bar_base = context.getResources().getDrawable(R.drawable.ic_btn_load_progress_base);
                drawables[1] = progress_bar_base;
                Drawable bar = context.getResources().getDrawable(R.drawable.ic_btn_load_progress);
                progress_bar_ = new ClipDrawable(bar, Gravity.LEFT, ClipDrawable.HORIZONTAL);
                drawables[2] = progress_bar_;
                LayerDrawable layer = new LayerDrawable(drawables);
                int rate = content_length > 0 ? (int) ((long) current_length * 9500 / content_length + 500) : 500;
                progress_bar_.setLevel(rate);
                image_button.setImageDrawable(layer);
                progress_anim.start();
            }
            
            @Override
            public void onProgress(final int current_length, final int content_length) {
                final ImageButton image_button_tmp = image_button_ref.get();
                if (image_button_tmp == null) return;
                image_button_tmp.post(new Runnable() {
                    @Override
                    public void run() {
                        if (progress_bar_ != null) {
                            int rate = content_length > 0 ? (int) ((long) current_length * 9500 / content_length + 500)
                                    : 500;
                            progress_bar_.setLevel(rate);
                        }
                        else {
                            initProgressBar(image_button_tmp, current_length, content_length);
                        }
                    }
                });
            }
        };
        
        agent.fetchImage(callback, local_image_file, image_uri, view_config.real_thumbnail_size_,
                view_config.real_thumbnail_size_ * 2, true);
    }
    
    private void setShowThumbnailButton(final ImageButton image_button, final TuboroidAgent agent,
            final ThreadData thread_data, final int image_index, final TuboroidApplication.ViewConfig view_config,
            final ViewStyle style) {
        image_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.post(new Runnable() {
                    @Override
                    public void run() {
                        image_button.setClickable(false);
                        showThumbnail((ImageButton) v, agent, thread_data, image_index, view_config, style);
                    }
                });
            }
        });
    }
    
    private View.OnClickListener createThumbnailOnClickListener(final ThreadData thread_data, final ViewStyle style,
            final String image_uri, final File local_image_file) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.post(new Runnable() {
                    @Override
                    public void run() {
                        if (style.image_viewer_launcher_ != null) {
                            style.image_viewer_launcher_.onRequired(thread_data, local_image_file.getAbsolutePath(),
                                    image_uri);
                        }
                    }
                });
            }
        };
    }
    
    private synchronized CharSequence getSpannedEntryHeader(final TuboroidApplication.ViewConfig view_config,
            final ViewStyle style) {
        if (entry_header_cache_ == null || !entry_header_cache_.isValid(style, view_config)) {
            entry_header_cache_ = createSpannedEntryHeader(view_config, style);
        }
        return entry_header_cache_.get();
    }
    
    private synchronized void clearSpannedEntryHeader() {
        entry_header_cache_ = null;
    }
    
    private synchronized SpannableCache createSpannedEntryHeader(final TuboroidApplication.ViewConfig view_config,
            final ViewStyle style) {
        StringBuilder buf = new StringBuilder(author_name_.length() + 100);
        
        // ////////////////////////////////////////////////////////////
        // 文字列組み立て
        // ////////////////////////////////////////////////////////////
        // レス番号
        final String entry_id_string = String.valueOf(entry_id_);
        buf.append(entry_id_string);
        buf.append(' ');
        
        final int entry_id_length = entry_id_string.length();
        TextAppearanceSpan entry_id_span = null;
        
        final int back_anchor_list_size = back_anchor_list_.size();
        if (back_anchor_list_size == 0) {
            entry_id_span = style.entry_id_style_span_1_;
        }
        else if (back_anchor_list_size < 3) {
            entry_id_span = style.entry_id_style_span_2_;
        }
        else {
            entry_id_span = style.entry_id_style_span_3_;
        }
        
        // //////////////////////////////
        // 名前とメール
        final int author_name_begin = buf.length();
        int author_name_length = 0;
        TextAppearanceSpan author_name_span = null;
        int author_mail_begin = 0;
        int author_mail_length = 0;
        TextAppearanceSpan author_mail_span = null;
        if (!isNG()) {
            // 名前
            author_name_length = author_name_.length();
            buf.append(author_name_);
            buf.append(' ');
            author_name_span = style.author_name_style_span_;
            
            // メール
            author_mail_length = author_mail_.length();
            if (author_mail_length > 0) {
                author_mail_begin = buf.length();
                author_mail_length += 2;
                buf.append('[');
                buf.append(author_mail_);
                buf.append("] ");
                author_mail_span = style.author_mail_time_style_span_;
            }
        }
        
        // //////////////////////////////
        // 時間
        final int entry_time_begin = buf.length();
        final int entry_time_length = entry_time_.length();
        buf.append(entry_time_);
        buf.append(' ');
        
        // //////////////////////////////
        // 書き込みID
        final int author_id_begin = buf.length();
        final int author_id_real_length = author_id_.length();
        int author_id_length = 0;
        int author_id_count = 0;
        
        buf.append("ID:");
        final int author_id_prefix_length = 3;
        
        if (author_id_real_length > 0) {
            author_id_length = author_id_real_length;
            buf.append(author_id_);
            if (author_id_count_ > 1 && author_id_.indexOf('?') == -1) {
                author_id_count = author_id_count_;
            }
        }
        else {
            buf.append("????");
            author_id_length = 4; // <= "????".length();
        }
        author_id_length += author_id_prefix_length;
        
        // 必死度を数える
        TextAppearanceSpan author_id_span = null;
        if (author_id_count >= 5) {
            author_id_span = style.author_id_style_span_3_;
        }
        else if (author_id_count >= 2) {
            author_id_span = style.author_id_style_span_2_;
        }
        else {
            author_id_span = style.author_id_style_span_1_;
        }
        
        // //////////////////////////////
        // レス数
        int author_id_count_begin = buf.length();
        int author_id_count_length = 0;
        if (author_id_count > 1) {
            buf.append(" (");
            buf.append(author_id_count);
            buf.append(')');
            author_id_count_length = buf.length() - author_id_count_begin;
        }
        
        // //////////////////////////////
        // Be
        final int author_be_begin = buf.length();
        int author_be_length = author_be_.length();
        if (author_be_length > 0) {
            buf.append(' ');
            buf.append(author_be_);
        }
        
        // ////////////////////////////////////////////////////////////
        // span適用
        // ////////////////////////////////////////////////////////////
        SpannableString spannable = new SpannableString(buf);
        
        // //////////////////////////////
        // レス番号
        spannable.setSpan(entry_id_span, 0, entry_id_length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        
        // //////////////////////////////
        // 名前
        if (author_name_span != null) {
            spannable.setSpan(author_name_span, author_name_begin, author_name_begin + author_name_length,
                    Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        
        // //////////////////////////////
        // 時間とメール
        if (author_mail_span != null) {
            spannable.setSpan(author_mail_span, author_mail_begin, entry_time_begin + entry_time_length,
                    Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        else {
            spannable.setSpan(style.author_mail_time_style_span_, entry_time_begin, entry_time_begin
                    + entry_time_length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        
        // //////////////////////////////
        // 書き込みIDの「ID」のとこ
        spannable.setSpan(style.author_id_prefix_style_span_, author_id_begin, author_id_begin
                + author_id_prefix_length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        
        // レス数と必死度を色付け
        if (author_id_count > 1) {
            spannable.setSpan(author_id_span, author_id_begin + author_id_prefix_length, author_id_count_begin
                    + author_id_count_length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        else {
            spannable.setSpan(author_id_span, author_id_begin + author_id_prefix_length, author_id_begin
                    + author_id_length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        
        // ////////////////////////////////////////////////////////////
        // Be
        if (author_be_length > 0) {
            spannable.setSpan(style.author_be_style_span_, author_be_begin, author_be_begin + author_be_length,
                    Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        entry_header_cache_ = new SpannableCache(spannable, style, view_config);
        
        return entry_header_cache_;
    }
    
    private synchronized CharSequence getSpannedEntryBody(final TuboroidApplication.ViewConfig view_config,
            final ViewStyle style) {
        SpannableCache cache = null;
        if (entry_body_cache_ref_ != null) {
            cache = entry_body_cache_ref_.get();
        }
        if (cache == null || !cache.isValid(style, view_config)) {
            cache = createSpannedEntryBody(view_config, style);
        }
        return cache.get();
    }
    
    private synchronized SpannableCache createSpannedEntryBody(final TuboroidApplication.ViewConfig view_config,
            final ViewStyle style) {
        CharSequence spannable = style.spanify_.apply(entry_body_, this);
        SpannableCache cache = new SpannableCache(spannable, style, view_config);
        entry_body_cache_ref_ = new SoftReference<SpannableCache>(cache);
        return cache;
    }
    
    private synchronized CharSequence getSpannedRevLink(TuboroidApplication.ViewConfig view_config, ViewStyle style) {
        SpannableCache cache = null;
        if (entry_rev_cache_ref_ != null) {
            cache = entry_rev_cache_ref_.get();
        }
        if (cache == null || !cache.isValid(style, view_config)) {
            cache = createSpannedRevLink(view_config, style);
        }
        return cache.get();
    }
    
    private synchronized SpannableCache createSpannedRevLink(TuboroidApplication.ViewConfig view_config, ViewStyle style) {
        class JumpAnchorSpan extends ClickableSpan {
            private long num_;
            ViewStyle style_;
            
            public JumpAnchorSpan(ViewStyle style, long num) {
                num_ = num;
                style_ = style;
            }
            
            @Override
            public void onClick(View widget) {
                if (num_ > 0) style_.callback_.onNumberAnchorClicked((int) entry_id_, (int) num_);
            }
        }
        
        SpannableStringBuilder spannable = new SpannableStringBuilder();
        int back_links = 0;
        synchronized (ThreadEntryData.class) {
            for (Long num : back_anchor_list_) {
                spannable.append(" [");
                String num_str = "<<" + String.valueOf(num);
                int start_index = spannable.length();
                spannable.append(num_str);
                spannable.setSpan(new JumpAnchorSpan(style, num), start_index, start_index + num_str.length(),
                        Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                spannable.append("]");
                back_links++;
                if (back_links >= MAX_BACK_LINKS) break;
            }
        }
        SpannableCache cache = new SpannableCache(spannable, style, view_config);
        entry_rev_cache_ref_ = new SoftReference<SpannableCache>(cache);
        
        return cache;
    }
    
    static public class ViewStyle {
        public int style_header_color_default_;
        public int style_header_color_emphasis_;
        
        public TextAppearanceSpan entry_id_style_span_1_;
        public TextAppearanceSpan entry_id_style_span_2_;
        public TextAppearanceSpan entry_id_style_span_3_;
        public TextAppearanceSpan author_name_style_span_;
        public TextAppearanceSpan author_mail_time_style_span_;
        public TextAppearanceSpan author_id_prefix_style_span_;
        public TextAppearanceSpan author_id_style_span_1_;
        public TextAppearanceSpan author_id_style_span_2_;
        public TextAppearanceSpan author_id_style_span_3_;
        public TextAppearanceSpan author_be_style_span_;
        public TextAppearanceSpan ignored_span_;
        
        public int link_color_;
        public int on_clicked_bgcolor_;
        public BackgroundColorSpan on_clicked_bgcolor_span_;
        
        public SpanifyAdapter spanify_;
        public OnAnchorClickedCallback callback_;
        public ImageViewerLauncher image_viewer_launcher_;
        
        public ViewStyle(Activity activity, ImageViewerLauncher image_viewer_launcher, OnAnchorClickedCallback callback) {
            style_header_color_default_ = activity.obtainStyledAttributes(R.styleable.Theme).getColor(
                    R.styleable.Theme_headerColorDefault, 0);
            style_header_color_emphasis_ = activity.obtainStyledAttributes(R.styleable.Theme).getColor(
                    R.styleable.Theme_headerColorEmphasis, 0);
            
            link_color_ = activity.obtainStyledAttributes(R.styleable.Theme).getColor(R.styleable.Theme_entryLinkColor,
                    0);
            
            on_clicked_bgcolor_ = activity.obtainStyledAttributes(R.styleable.Theme).getColor(
                    R.styleable.Theme_entryLinkClickedBgColor, 0);
            on_clicked_bgcolor_span_ = new BackgroundColorSpan(on_clicked_bgcolor_);
            
            entry_id_style_span_1_ = new TextAppearanceSpan(activity, R.style.EntryListEntryID1);
            entry_id_style_span_2_ = new TextAppearanceSpan(activity, R.style.EntryListEntryID2);
            entry_id_style_span_3_ = new TextAppearanceSpan(activity, R.style.EntryListEntryID3);
            author_name_style_span_ = new TextAppearanceSpan(activity, R.style.EntryListAuthorName);
            author_mail_time_style_span_ = new TextAppearanceSpan(activity, R.style.EntryListAuthorMailAndTime);
            author_id_prefix_style_span_ = new TextAppearanceSpan(activity, R.style.EntryListAuthorIDPrefix);
            author_id_style_span_1_ = new TextAppearanceSpan(activity, R.style.EntryListAuthorID1);
            author_id_style_span_2_ = new TextAppearanceSpan(activity, R.style.EntryListAuthorID2);
            author_id_style_span_3_ = new TextAppearanceSpan(activity, R.style.EntryListAuthorID3);
            author_be_style_span_ = new TextAppearanceSpan(activity, R.style.EntryListAuthorBE);
            ignored_span_ = new TextAppearanceSpan(activity, R.style.EntryListIgnored);
            
            image_viewer_launcher_ = image_viewer_launcher;
            callback_ = callback;
            
            spanify_ = new SpanifyAdapter();
            spanify_.addFilter(new ThreadWebURLFilter(new String[] { "http", "https", "ttp" }));
            spanify_.addFilter(new LocalEntryAnchorFilter());
        }
        
        private class ThreadWebURLFilter extends WebURLFilter {
            private class ThreadURLSpan extends URLSpan {
                public ThreadURLSpan(String uri) {
                    super(uri);
                }
                
                @Override
                public void onClick(View widget) {
                    Uri uri = Uri.parse(getURL());
                    callback_.onThreadLinkClicked(uri);
                }
            }
            
            private class BoardURLSpan extends URLSpan {
                public BoardURLSpan(String uri) {
                    super(uri);
                }
                
                @Override
                public void onClick(View widget) {
                    Uri uri = Uri.parse(getURL());
                    callback_.onBoardLinkClicked(uri);
                }
            }
            
            public ThreadWebURLFilter(String[] strings) {
                super(strings);
            }
            
            @Override
            public Object getSpan(String text, SpanSpec spec, Object arg) {
                if (text.startsWith("ttp")) {
                    text = "h" + text;
                }
                if (ThreadData.isThreadUri(text)) {
                    return new ThreadURLSpan(text);
                }
                else if (BoardData.isBoardUri(text)) {
                    return new BoardURLSpan(text);
                }
                return new URLSpan(text);
            }
        }
        
        private class LocalEntryAnchorFilter extends EntryAnchorFilter {
            
            private class EntryAnchorSpan extends ClickableSpan {
                int current_entry_id_;
                int entry_id_;
                
                public EntryAnchorSpan(long current_entry_id, long target_entry_id) {
                    current_entry_id_ = (int) current_entry_id;
                    entry_id_ = (int) target_entry_id;
                }
                
                @Override
                public void onClick(View widget) {
                    if (entry_id_ > 0) {
                        callback_.onNumberAnchorClicked(current_entry_id_, entry_id_);
                    }
                }
            }
            
            @Override
            public Object getSpan(String text, SpanSpec spec, Object arg) {
                long current_entry_id = 0;
                long target_entry_id = 0;
                if (arg != null && arg instanceof ThreadEntryData) {
                    current_entry_id = ((ThreadEntryData) arg).entry_id_;
                }
                if (spec instanceof EntryAnchorSpanSpec) {
                    target_entry_id = ((EntryAnchorSpanSpec) spec).target_id_;
                }
                EntryAnchorSpan span = new EntryAnchorSpan(current_entry_id, target_entry_id);
                return span;
            }
        }
        
    }
    
    public boolean canAddNGID() {
        return author_id_.length() > 0 && author_id_.indexOf('?') == -1;
    }
    
    public static native boolean is2chAsciiArt(String entry);
    
    public static native void initNative();
    
    static {
        System.loadLibrary("info_narazaki_android_tuboroid");
        initNative();
    }
    
}
