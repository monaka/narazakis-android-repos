package info.narazaki.android.tuboroid.agent;

import info.narazaki.android.lib.text.HtmlUtils;
import info.narazaki.android.tuboroid.R;
import info.narazaki.android.tuboroid.agent.thread.DataFileAgent;
import info.narazaki.android.tuboroid.agent.thread.SQLiteAgent;
import info.narazaki.android.tuboroid.data.BoardData;
import info.narazaki.android.tuboroid.data.BoardData2ch;
import info.narazaki.android.tuboroid.data.BoardIdentifier;
import info.narazaki.android.tuboroid.data.FavoriteItemBoardData;
import info.narazaki.android.tuboroid.data.FavoriteItemData;
import info.narazaki.android.tuboroid.data.FavoriteItemSearchKey;
import info.narazaki.android.tuboroid.data.FavoriteItemThreadData;
import info.narazaki.android.tuboroid.data.Find2chKeyData;
import info.narazaki.android.tuboroid.data.ThreadData;
import info.narazaki.android.tuboroid.data.ThreadData2ch;
import info.narazaki.android.tuboroid.data.ThreadEntryData;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.util.Xml;

public class FavoriteImportExportAgent {
    private static final String TAG = "FavoriteImportExportAgent";
    
    private TuboroidAgentManager agent_manager_;
    
    public FavoriteImportExportAgent(TuboroidAgentManager agent_manager) {
        super();
        agent_manager_ = agent_manager;
    }
    
    public static class Terminator {
        private boolean is_terminated_;
        
        public Terminator() {
            is_terminated_ = false;
        }
        
        public synchronized boolean isTerminated() {
            return is_terminated_;
        }
        
        public synchronized void terminate() {
            is_terminated_ = true;
        }
    }
    
    public interface ExportForEn2chCallback {
        public void onFailed();
        
        public void onStart(final int threads_num);
        
        public void onProgress(final String thread_name);
        
        public void onFinished();
    }
    
    public Terminator exportForEn2ch(final String target_path, final ExportForEn2chCallback callback) {
        final Terminator terminator = new Terminator();
        agent_manager_.getFavoriteCacheListAgent().fetchFavoriteList(
                new FavoriteCacheListAgent.FavoriteListFetchedCallback() {
                    @Override
                    public void onFavoriteListFetched(final ArrayList<FavoriteItemData> data_list) {
                        writeXMLToExportForEn2ch(terminator, target_path, data_list, callback);
                    }
                });
        return terminator;
    }
    
    private void writeXMLToExportForEn2ch(final Terminator terminator, final String target_path,
            final ArrayList<FavoriteItemData> data_list, final ExportForEn2chCallback callback) {
        final File target_file = new File(target_path, "shortcuts.xml");
        agent_manager_.getFileAgent().writeFile(target_file.getAbsolutePath(),
                new DataFileAgent.FileWriteUTF8StreamCallback() {
                    @Override
                    public void write(Writer writer) throws IOException {
                        writer.write("<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>");
                        writer.write("<xbel version=\"1.0\">");
                        writer.write("<title>En2ch Ver.1.2.11</title>");
                        
                        for (FavoriteItemData data : data_list) {
                            if (data.isBoard()) {
                                BoardData board_data = data.getBoardData();
                                if (board_data instanceof BoardData2ch) {
                                    writer.write("<bookmark href=\"" + board_data.getSubjectsURI()
                                            + "\"  fullpath=\"\" kind=\"1\">");
                                    writer.write("<title>" + board_data.board_category_ + "  −  "
                                            + board_data.board_name_ + "</title></bookmark>");
                                }
                            }
                            else if (data.isThread()) {
                                ThreadData thread_data = data.getThreadData();
                                if (thread_data instanceof ThreadData2ch) {
                                    File dat_local_file = getEn2chLocalDatFile(target_path, thread_data);
                                    writer.write("<bookmark href=\"" + thread_data.getDatFileURI() + "\"  fullpath=\""
                                            + dat_local_file.getAbsolutePath() + "\" kind=\"0\">");
                                    writer.write("<title>" + thread_data.thread_name_ + "</title></bookmark>");
                                }
                            }
                        }
                        
                        writer.write("</xbel>");
                    }
                }, false, false, true, new DataFileAgent.FileWroteCallback() {
                    
                    @Override
                    public void onFileWrote(boolean succeeded) {
                        if (!succeeded) {
                            callback.onFailed();
                            return;
                        }
                        exportDatListToExportForEn2ch(terminator, target_path, callback);
                    }
                });
    }
    
    private void exportDatListToExportForEn2ch(final Terminator terminator, final String target_path,
            final ExportForEn2chCallback callback) {
        agent_manager_.getThreadListAgent().fetchRecentList(0, new ThreadListAgent.RecentListFetchedCallback() {
            @Override
            public void onRecentListFetched(final ArrayList<ThreadData> data_list) {
                final ArrayList<ThreadData> data_list_2ch = new ArrayList<ThreadData>();
                for (ThreadData data : data_list) {
                    if (data instanceof ThreadData2ch) data_list_2ch.add(data);
                }
                callback.onStart(data_list_2ch.size());
                exportOneDatToExportForEn2ch(terminator, target_path, data_list_2ch, callback);
            }
        });
    }
    
    private void exportOneDatToExportForEn2ch(final Terminator terminator, final String target_path,
            final ArrayList<ThreadData> thread_data_list, final ExportForEn2chCallback callback) {
        if (thread_data_list.size() == 0 || terminator.is_terminated_) {
            callback.onFinished();
            return;
        }
        
        final ThreadData thread_data = thread_data_list.get(0);
        thread_data_list.remove(0);
        
        final ArrayList<ThreadEntryData> entry_list = new ArrayList<ThreadEntryData>();
        
        agent_manager_.getEntryListAgent().reloadThreadEntryList(thread_data, false, true,
                new ThreadEntryListAgent.ThreadEntryListAgentCallback() {
                    
                    private void onThreadEntryListFetchedCompleted() {
                        writeOneDatToExportForEn2ch(terminator, target_path, thread_data, thread_data_list, entry_list,
                                callback);
                    }
                    
                    @Override
                    public void onThreadEntryListFetchedCompleted(ThreadData thread_data, boolean is_analyzed) {
                        onThreadEntryListFetchedCompleted();
                    }
                    
                    @Override
                    public void onThreadEntryListFetchedByCache(List<ThreadEntryData> data_list) {
                        entry_list.addAll(data_list);
                    }
                    
                    @Override
                    public void onThreadEntryListFetched(List<ThreadEntryData> data_list) {
                        entry_list.addAll(data_list);
                    }
                    
                    @Override
                    public void onThreadEntryListFetchStarted(ThreadData thread_data) {}
                    
                    @Override
                    public void onThreadEntryListClear() {
                        entry_list.clear();
                    }
                    
                    @Override
                    public void onInterrupted() {
                        onThreadEntryListFetchedCompleted();
                    }
                    
                    @Override
                    public void onDatDropped(boolean is_permanently) {
                        onThreadEntryListFetchedCompleted();
                    }
                    
                    @Override
                    public void onConnectionOffline(ThreadData thread_data) {
                        onThreadEntryListFetchedCompleted();
                    }
                    
                    @Override
                    public void onConnectionFailed(boolean connectionFailed) {
                        onThreadEntryListFetchedCompleted();
                    }
                });
    }
    
    private File getEn2chLocalDatFile(final String target_path, final ThreadData thread_data) {
        return new File(target_path, thread_data.server_def_.board_server_ + "/" + thread_data.server_def_.board_tag_
                + "/dat/" + thread_data.thread_id_ + ".dat");
    }
    
    private void writeOneDatToExportForEn2ch(final Terminator terminator, final String target_path,
            final ThreadData thread_data, final ArrayList<ThreadData> thread_data_list,
            final ArrayList<ThreadEntryData> entry_list, final ExportForEn2chCallback callback) {
        final File dat_local_file = getEn2chLocalDatFile(target_path, thread_data);
        
        agent_manager_.getFileAgent().writeFile(dat_local_file.getAbsolutePath(),
                new DataFileAgent.FileWriteStreamCallback() {
                    @Override
                    public void write(OutputStream stream) {
                        OutputStreamWriter osw = null;
                        try {
                            osw = new OutputStreamWriter(stream, "MS932");
                            for (ThreadEntryData data : entry_list) {
                                if (data.entry_id_ != 1) {
                                    osw.append("\n");
                                }
                                
                                osw.append(data.author_name_).append("<>");
                                osw.append(data.author_mail_).append("<>");
                                osw.append(data.entry_time_);
                                if (data.author_id_.length() > 0) osw.append(" ID:").append(data.author_id_);
                                if (data.author_be_.length() > 0) osw.append(" BE:").append(data.author_be_);
                                osw.append("<>");
                                
                                String entry_body = HtmlUtils.escapeHtml(data.entry_body_).replace("\n", "<br>");
                                osw.append(entry_body).append("<>");
                                
                                if (data.entry_id_ == 1) {
                                    osw.append(thread_data.thread_name_);
                                }
                            }
                        }
                        catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                        finally {
                            if (osw != null) {
                                try {
                                    osw.close();
                                }
                                catch (IOException e1) {
                                }
                            }
                        }
                    }
                }, false, true, true, new DataFileAgent.FileWroteCallback() {
                    
                    @Override
                    public void onFileWrote(boolean succeeded) {
                        tweakOneDatToExportForEn2ch(terminator, target_path, dat_local_file, thread_data,
                                thread_data_list, callback);
                    }
                });
    }
    
    private void tweakOneDatToExportForEn2ch(final Terminator terminator, final String target_path,
            File dat_local_file, final ThreadData thread_data, final ArrayList<ThreadData> thread_data_list,
            final ExportForEn2chCallback callback) {
        final int file_size = (int) dat_local_file.length();
        final int cache_size = thread_data.cache_size_;
        final int diff = cache_size - file_size;
        
        agent_manager_.getFileAgent().writeFile(dat_local_file.getAbsolutePath(),
                new DataFileAgent.FileWriteUTF8StreamCallback() {
                    
                    @Override
                    public void write(Writer writer) throws IOException {
                        StringBuilder str = new StringBuilder();
                        for (int i = 0; i < diff - 1; i++) {
                            str.append(' ');
                        }
                        writer.write(str.toString());
                        writer.write('\n');
                    }
                }, true, false, false, new DataFileAgent.FileWroteCallback() {
                    
                    @Override
                    public void onFileWrote(boolean succeeded) {
                        callback.onProgress(thread_data.thread_name_);
                        exportOneDatToExportForEn2ch(terminator, target_path, thread_data_list, callback);
                    }
                });
    }
    
    public interface ExportForTuboroidCallback {
        public void onFailed();
        
        public void onStart(final int threads_num);
        
        public void onProgress();
        
        public void onFinished();
    }
    
    public Terminator exportForTuboroid(final File target_file, final ExportForTuboroidCallback callback) {
        final Terminator terminator = new Terminator();
        agent_manager_.getFavoriteCacheListAgent().fetchFavoriteList(
                new FavoriteCacheListAgent.FavoriteListFetchedCallback() {
                    @Override
                    public void onFavoriteListFetched(final ArrayList<FavoriteItemData> favorites_data_list) {
                        agent_manager_.getThreadListAgent().fetchRecentList(0,
                                new ThreadListAgent.RecentListFetchedCallback() {
                                    @Override
                                    public void onRecentListFetched(final ArrayList<ThreadData> recents_data_list) {
                                        writeXMLToExportForTuboroid(terminator, target_file, favorites_data_list,
                                                recents_data_list, callback);
                                    }
                                });
                    }
                });
        return terminator;
    }
    
    static final String TUBOROID_TAG_TYPE_FAVORITES = "favorites";
    static final String TUBOROID_TAG_TYPE_RECENTS = "recents";
    static final String TUBOROID_META_TYPE_BOARD = "board";
    static final String TUBOROID_META_TYPE_THREAD = "thread";
    static final String TUBOROID_META_TYPE_SEARCH = "search";
    
    private void writeXMLToExportForTuboroid(final Terminator terminator, final File target_file,
            final ArrayList<FavoriteItemData> favorites_data_list, final ArrayList<ThreadData> recents_data_list,
            final ExportForTuboroidCallback callback) {
        
        callback.onStart(favorites_data_list.size() + recents_data_list.size());
        
        agent_manager_.getFileAgent().writeFile(target_file.getAbsolutePath(),
                new DataFileAgent.FileWriteUTF8StreamCallback() {
                    @Override
                    public void write(Writer writer) throws IOException {
                        writer.write("<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>\n");
                        writer.write("<xbel version=\"1.0\">\n");
                        Context context = agent_manager_.getContext();
                        String version_string;
                        try {
                            version_string = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                        }
                        catch (NameNotFoundException e) {
                            version_string = "****";
                        }
                        writer.write("<title>Tuboroid " + version_string + "</title>\n");
                        writer.write("<folder><title>" + TUBOROID_TAG_TYPE_FAVORITES + "</title>\n");
                        
                        for (FavoriteItemData data : favorites_data_list) {
                            if (data.isBoard()) {
                                BoardData board_data = data.getBoardData();
                                writer.write("<bookmark href=\"" + board_data.getSubjectsURI() + "\">\n");
                                writer.write("<title>" + board_data.board_name_ + "</title>\n");
                                writer.write("<info>\n");
                                writer.write("<metadata owner=\"\">\n");
                                writer.write("<type>" + TUBOROID_META_TYPE_BOARD + "</type>\n");
                                writer.write("</metadata>\n");
                                writer.write("</info>\n");
                                writer.write("</bookmark>\n");
                            }
                            else if (data.isThread()) {
                                ThreadData thread_data = data.getThreadData();
                                writer.write("<bookmark href=\"" + thread_data.getThreadURI() + "\">\n");
                                writer.write("<title>" + thread_data.thread_name_ + "</title>\n");
                                writer.write("<info>\n");
                                writer.write("<metadata owner=\"\">\n");
                                writer.write("<type>" + TUBOROID_META_TYPE_THREAD + "</type>\n");
                                writer.write("<board_name>" + thread_data.board_name_ + "</board_name>\n");
                                writer.write("<cache_count>" + thread_data.cache_count_ + "</cache_count>\n");
                                writer.write("<cache_size>" + thread_data.cache_size_ + "</cache_size>\n");
                                writer.write("<read_count>" + thread_data.read_count_ + "</read_count>\n");
                                writer.write("</metadata>\n");
                                writer.write("</info>\n");
                                writer.write("</bookmark>\n");
                            }
                            else if (data.isSearchKey()) {
                                Find2chKeyData search_key = data.getSearchKey();
                                writer.write("<bookmark href=\"\">\n");
                                writer.write("<title>" + search_key.keyword_ + "</title>\n");
                                writer.write("<info>\n");
                                writer.write("<metadata owner=\"\">\n");
                                writer.write("<type>" + TUBOROID_META_TYPE_SEARCH + "</type>\n");
                                writer.write("</metadata>\n");
                                writer.write("</info>\n");
                                writer.write("</bookmark>\n");
                            }
                            callback.onProgress();
                        }
                        writer.write("</folder>\n");
                        
                        writer.write("<folder><title>" + TUBOROID_TAG_TYPE_RECENTS + "</title>\n");
                        for (ThreadData thread_data : recents_data_list) {
                            writer.write("<bookmark href=\"" + thread_data.getThreadURI() + "\">\n");
                            writer.write("<title>" + thread_data.thread_name_ + "</title>\n");
                            writer.write("<info>\n");
                            writer.write("<metadata owner=\"\">\n");
                            writer.write("<type>" + TUBOROID_META_TYPE_THREAD + "</type>\n");
                            writer.write("<board_name>" + thread_data.board_name_ + "</board_name>\n");
                            writer.write("<cache_count>" + thread_data.cache_count_ + "</cache_count>\n");
                            writer.write("<cache_size>" + thread_data.cache_size_ + "</cache_size>\n");
                            writer.write("<read_count>" + thread_data.read_count_ + "</read_count>\n");
                            writer.write("</metadata>\n");
                            writer.write("</info>\n");
                            writer.write("</bookmark>\n");
                            callback.onProgress();
                        }
                        writer.write("</folder>\n");
                        
                        writer.write("</xbel>\n");
                    }
                }, false, true, true, new DataFileAgent.FileWroteCallback() {
                    
                    @Override
                    public void onFileWrote(boolean succeeded) {
                        if (!succeeded) {
                            callback.onFailed();
                            return;
                        }
                        callback.onFinished();
                    }
                });
    }
    
    public interface ImportFromTuboroidCallback {
        public void onFailed(final String message);
        
        public void onStart(final int threads_num);
        
        public void onProgress(final String name);
        
        public void onFinished();
    }
    
    private static class ImportingBookmark {
        public String url_ = "";
        public String title_ = "";
        public String type_ = "";
        public String board_name_ = "";
        public int cache_count_ = 0;
        public int cache_size_ = 0;
        public int read_count_ = 0;
    }
    
    private static class InvalidBookmarkException extends Exception {
        private static final long serialVersionUID = 1L;
    }
    
    private static class InvalidBookmarkTypeException extends Exception {
        private static final long serialVersionUID = 1L;
    }
    
    public Terminator importFromTuboroid(final File target_file, final ImportFromTuboroidCallback callback) {
        final Terminator terminator = new Terminator();
        agent_manager_.getFileAgent().readFile(target_file.getAbsolutePath(), new DataFileAgent.FileReadUTF8Callback() {
            @Override
            public void read(BufferedReader reader) {
                final ArrayList<FavoriteItemData> favorites_data_list = new ArrayList<FavoriteItemData>();
                final ArrayList<ThreadData> recents_data_list = new ArrayList<ThreadData>();
                final XmlPullParser parser = Xml.newPullParser();
                String current_tag = null;
                ImportingBookmark current_data = null;
                boolean in_folder = false;
                boolean in_favorite_folder = false;
                try {
                    parser.setInput(reader);
                    int event = 0;
                    while ((event = parser.next()) != XmlPullParser.END_DOCUMENT) {
                        switch (event) {
                        case XmlPullParser.START_TAG:
                            current_tag = parser.getName();
                            if (current_tag.equals("folder")) {
                                in_folder = true;
                            }
                            else if (current_tag.equals("bookmark")) {
                                current_data = new ImportingBookmark();
                                current_data.url_ = parser.getAttributeValue("", "href");
                            }
                            break;
                        
                        case XmlPullParser.END_TAG:
                            String end_tag = parser.getName();
                            if (end_tag.equals("folder")) {
                                in_folder = false;
                                in_favorite_folder = false;
                            }
                            else if (end_tag.equals("bookmark")) {
                                if (current_data == null) throw new InvalidBookmarkException();
                                if (current_data.url_ == null) throw new InvalidBookmarkException();
                                Uri uri = Uri.parse(current_data.url_);
                                BoardIdentifier board_server = BoardData.factoryBoardServer(uri);
                                if (current_data.type_.equals("board")) {
                                    if (in_favorite_folder) {
                                        BoardData board_data = BoardData.factory(0, current_data.title_, "",
                                                board_server);
                                        FavoriteItemData favorite_item = new FavoriteItemBoardData(board_data);
                                        favorites_data_list.add(favorite_item);
                                    }
                                }
                                else if (current_data.type_.equals("search")) {
                                    if (in_favorite_folder) {
                                        Find2chKeyData key_data = new Find2chKeyData(0, current_data.title_, 0, true, 0);
                                        FavoriteItemData favorite_item = new FavoriteItemSearchKey(key_data);
                                        favorites_data_list.add(favorite_item);
                                    }
                                }
                                else {
                                    ThreadData thread_data = ThreadData.factory(uri);
                                    thread_data.board_name_ = current_data.board_name_;
                                    thread_data.thread_name_ = current_data.title_;
                                    thread_data.cache_count_ = current_data.cache_count_;
                                    thread_data.cache_size_ = current_data.cache_size_;
                                    thread_data.read_count_ = current_data.read_count_;
                                    
                                    if (in_favorite_folder) {
                                        FavoriteItemData favorite_item = new FavoriteItemThreadData(thread_data);
                                        favorites_data_list.add(favorite_item);
                                    }
                                    else {
                                        recents_data_list.add(thread_data);
                                    }
                                }
                                current_data = null;
                            }
                            current_tag = null;
                            break;
                        
                        case XmlPullParser.TEXT:
                            String text = parser.getText();
                            if (current_tag == null) {
                            }
                            else if (current_tag.equals("title")) {
                                if (current_data != null) {
                                    current_data.title_ = text;
                                }
                                else {
                                    if (in_folder) {
                                        if (text.equals(TUBOROID_TAG_TYPE_FAVORITES)) {
                                            in_favorite_folder = true;
                                        }
                                        else {
                                            in_favorite_folder = false;
                                        }
                                    }
                                    else {
                                        if (!text.contains("Tuboroid")) throw new InvalidBookmarkTypeException();
                                    }
                                }
                            }
                            else if (current_tag.equals("type")) {
                                if (current_data == null) throw new InvalidBookmarkException();
                                current_data.type_ = text;
                            }
                            else if (current_tag.equals("board_name")) {
                                if (current_data == null) throw new InvalidBookmarkException();
                                current_data.board_name_ = text;
                            }
                            else if (current_tag.equals("cache_count")) {
                                if (current_data == null) throw new InvalidBookmarkException();
                                current_data.cache_count_ = Integer.valueOf(text);
                            }
                            else if (current_tag.equals("cache_size")) {
                                if (current_data == null) throw new InvalidBookmarkException();
                                current_data.cache_size_ = Integer.valueOf(text);
                            }
                            else if (current_tag.equals("read_count")) {
                                if (current_data == null) throw new InvalidBookmarkException();
                                current_data.read_count_ = Integer.valueOf(text);
                            }
                            break;
                        }
                        
                    }
                }
                catch (InvalidBookmarkTypeException e) {
                    e.printStackTrace();
                    callback.onFailed(agent_manager_.getContext().getString(R.string.text_invalid_bookmark_file_type));
                    return;
                }
                catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(agent_manager_.getContext().getString(R.string.text_invalid_bookmark_file));
                    return;
                }
                
                callback.onStart(favorites_data_list.size() + recents_data_list.size());
                importFavoritesFromTuboroid(terminator, favorites_data_list, recents_data_list, callback);
            }
        });
        return terminator;
    }
    
    public void importFavoritesFromTuboroid(final Terminator terminator,
            final ArrayList<FavoriteItemData> favorites_data_list, final ArrayList<ThreadData> recents_data_list,
            final ImportFromTuboroidCallback callback) {
        if (terminator.is_terminated_) {
            callback.onFinished();
            return;
        }
        if (favorites_data_list.size() == 0) {
            importRecentsFromTuboroid(terminator, recents_data_list, callback);
            return;
        }
        final FavoriteItemData favorite_item = favorites_data_list.get(0);
        favorites_data_list.remove(0);
        Uri uri = Uri.parse(favorite_item.getURI());
        if (favorite_item.isSearchKey()) {
            agent_manager_.getFavoriteCacheListAgent().addFavorite(favorite_item.getSearchKey(), new Runnable() {
                @Override
                public void run() {
                    callback.onProgress(favorite_item.getSearchKey().keyword_);
                    importFavoritesFromTuboroid(terminator, favorites_data_list, recents_data_list, callback);
                }
            });
        }
        else {
            agent_manager_.getBoardListAgent().getBoardData(uri, false, new BoardListAgent.BoardFetchedCallback() {
                @Override
                public void onBoardFetched(final BoardData board_data) {
                    if (favorite_item.isBoard()) {
                        agent_manager_.getFavoriteCacheListAgent().addFavorite(board_data, new Runnable() {
                            @Override
                            public void run() {
                                callback.onProgress(board_data.board_name_);
                                importFavoritesFromTuboroid(terminator, favorites_data_list, recents_data_list,
                                        callback);
                            }
                        });
                    }
                    else {
                        final ThreadData thread_data = favorite_item.getThreadData();
                        agent_manager_.getDBAgent().insertThreadData(thread_data, new SQLiteAgent.DbTransaction() {
                            @Override
                            public void run() {
                                agent_manager_.getFavoriteCacheListAgent().addFavorite(thread_data, new Runnable() {
                                    
                                    @Override
                                    public void run() {
                                        callback.onProgress(thread_data.thread_name_);
                                        importFavoritesFromTuboroid(terminator, favorites_data_list, recents_data_list,
                                                callback);
                                    }
                                });
                            }
                            
                            @Override
                            public void onError() {
                                run();
                            }
                        });
                        
                    }
                }
            });
        }
    }
    
    public void importRecentsFromTuboroid(final Terminator terminator, final ArrayList<ThreadData> recents_data_list,
            final ImportFromTuboroidCallback callback) {
        if (terminator.is_terminated_) {
            callback.onFinished();
            return;
        }
        if (recents_data_list.size() == 0) {
            callback.onFinished();
            return;
        }
        final ThreadData thread_data = recents_data_list.get(0);
        recents_data_list.remove(0);
        Uri uri = Uri.parse(thread_data.getThreadURI());
        agent_manager_.getBoardListAgent().getBoardData(uri, false, new BoardListAgent.BoardFetchedCallback() {
            @Override
            public void onBoardFetched(final BoardData board_data) {
                agent_manager_.getDBAgent().insertThreadData(thread_data, new SQLiteAgent.DbTransaction() {
                    @Override
                    public void run() {
                        callback.onProgress(thread_data.thread_name_);
                        importRecentsFromTuboroid(terminator, recents_data_list, callback);
                    }
                    
                    @Override
                    public void onError() {
                        run();
                    }
                });
            }
        });
        
    }
}
