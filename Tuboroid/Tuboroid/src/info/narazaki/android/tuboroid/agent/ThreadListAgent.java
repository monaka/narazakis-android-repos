package info.narazaki.android.tuboroid.agent;

import info.narazaki.android.lib.text.LevenshteinDistance;
import info.narazaki.android.tuboroid.agent.task.HttpGetThreadListTask;
import info.narazaki.android.tuboroid.agent.thread.SQLiteAgent;
import info.narazaki.android.tuboroid.data.BoardData;
import info.narazaki.android.tuboroid.data.ThreadData;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.database.Cursor;

public class ThreadListAgent {
    private static final String TAG = "ThreadListAgent";
    private static final int MAX_THREAD_LIST_HARD_CACHE = 2;
    
    private TuboroidAgentManager agent_manager_;
    
    private final HashMap<BoardData, SoftReference<List<ThreadData>>> soft_cache_map_;
    
    private final HashMap<BoardData, List<ThreadData>> hard_cache_map_ = new LinkedHashMap<BoardData, List<ThreadData>>(
            MAX_THREAD_LIST_HARD_CACHE / 2, 0.75f, true) {
        private static final long serialVersionUID = 1L;
        
        @Override
        protected boolean removeEldestEntry(LinkedHashMap.Entry<BoardData, List<ThreadData>> eldest) {
            if (size() <= MAX_THREAD_LIST_HARD_CACHE) return false;
            return true;
        }
    };
    
    public ThreadListAgent(TuboroidAgentManager agent_manager) {
        super();
        agent_manager_ = agent_manager;
        soft_cache_map_ = new HashMap<BoardData, SoftReference<List<ThreadData>>>();
    }
    
    public static interface ThreadListFetchedCallback {
        public void onThreadListFetchedCache(final List<ThreadData> data_list);
        
        public void onThreadListFetched(final List<ThreadData> data_list);
        
        public void onThreadListFetchCompleted();
        
        public void onInterrupted();
        
        public void onThreadListFetchFailed(final boolean maybe_moved);
        
        public void onConnectionOffline();
        
        public void onStartOnlineLoading();
    }
    
    public static interface RecentListFetchedCallback {
        public void onRecentListFetched(final ArrayList<ThreadData> data_list);
    }
    
    public void fetchThreadList(final BoardData board_data, final boolean force_reload,
            final ThreadListFetchedCallback callback) {
        reloadThreadList(board_data, force_reload, callback);
    }
    
    private void reloadThreadList(final BoardData board_data, final boolean force_reload,
            final ThreadListFetchedCallback callback) {
        agent_manager_.getDBAgent().getThreadList(board_data, new SQLiteAgent.DbResultReceiver() {
            @Override
            public void onQuery(Cursor cursor) {
                final ArrayList<ThreadData> data_list_db = new ArrayList<ThreadData>();
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    while (true) {
                        data_list_db.add(ThreadData.factory(cursor));
                        if (cursor.moveToNext() == false) break;
                    }
                }
                cursor.close();
                if (!force_reload && reloadCachedThreadList(board_data, data_list_db, callback)) return;
                
                callback.onThreadListFetchedCache(data_list_db);
                reloadOnlineThreadList(board_data, data_list_db, callback);
            }
            
            @Override
            public void onError() {
                reloadOnlineThreadList(board_data, new ArrayList<ThreadData>(), callback);
            }
        });
    }
    
    synchronized private void removeMemoryCacheData(final BoardData board_data) {
        soft_cache_map_.remove(board_data);
        hard_cache_map_.remove(board_data);
    }
    
    synchronized private List<ThreadData> fetchMemoryCacheData(final BoardData board_data) {
        final SoftReference<List<ThreadData>> cache = soft_cache_map_.get(board_data);
        if (cache == null) return null;
        final List<ThreadData> data_list = cache.get();
        if (data_list == null) return null;
        
        return data_list;
    }
    
    private boolean reloadCachedThreadList(final BoardData board_data, final List<ThreadData> data_list_db,
            final ThreadListFetchedCallback callback) {
        
        List<ThreadData> data_list = fetchMemoryCacheData(board_data);
        if (data_list != null) {
            onCachedThreadListFetched(board_data, data_list_db, data_list, callback);
            return true;
        }
        return false;
    }
    
    private void onCachedThreadListFetched(final BoardData board_data, final List<ThreadData> data_list_db,
            final List<ThreadData> data_list, final ThreadListFetchedCallback callback) {
        
        ArrayList<ThreadData> data_list_result = new ArrayList<ThreadData>();
        data_list_result.addAll(data_list_db);
        data_list_result.addAll(mergeThreadList(data_list, data_list_db));
        
        callback.onThreadListFetchedCache(data_list_result);
        callback.onThreadListFetched(new ArrayList<ThreadData>());
        callback.onThreadListFetchCompleted();
    }
    
    synchronized private void storeMemoryCacheData(final BoardData board_data, final List<ThreadData> data_list) {
        soft_cache_map_.put(board_data, new SoftReference<List<ThreadData>>(data_list));
        hard_cache_map_.put(board_data, data_list);
    }
    
    private void reloadOnlineThreadList(final BoardData board_data, final List<ThreadData> data_list_db,
            final ThreadListFetchedCallback callback) {
        // Offlineチェック
        if (!agent_manager_.isOnline()) {
            callback.onConnectionOffline();
            return;
        }
        
        removeMemoryCacheData(board_data);
        callback.onStartOnlineLoading();
        
        final List<ThreadData> data_list_result = new ArrayList<ThreadData>();
        final List<ThreadData> data_list_cache_temp = new ArrayList<ThreadData>();
        data_list_result.addAll(data_list_db);
        
        HttpGetThreadListTask task = board_data.factoryGetThreadListTask(new HttpGetThreadListTask.Callback() {
            @Override
            public void onConnectionFailed() {
                callback.onThreadListFetchFailed(false);
            }
            
            @Override
            public void onCompleted() {
                updateDatDroppedThreads(data_list_result);
                boolean maybe_moved = data_list_cache_temp.size() == 0;
                storeMemoryCacheData(board_data, data_list_cache_temp);
                if (maybe_moved) {
                    callback.onThreadListFetchFailed(true);
                }
                else {
                    callback.onThreadListFetchCompleted();
                }
            }
            
            @Override
            public void onReceived(final List<ThreadData> data_list) {
                data_list_cache_temp.addAll(data_list);
                List<ThreadData> new_data_list = mergeThreadList(data_list, data_list_db);
                data_list_result.addAll(new_data_list);
                callback.onThreadListFetched(new_data_list);
            }
            
            @Override
            public void onInterrupted() {
                callback.onInterrupted();
            }
            
        });
        task.sendTo(agent_manager_.getMultiHttpAgent());
    }
    
    private List<ThreadData> mergeThreadList(final List<ThreadData> data_list, final List<ThreadData> data_list_db) {
        final List<ThreadData> data_list_result = new ArrayList<ThreadData>();
        final HashMap<Long, ThreadData> data_map_db = new HashMap<Long, ThreadData>();
        final List<ThreadData> new_data_list_db = new ArrayList<ThreadData>();
        for (ThreadData data : data_list_db) {
            data_map_db.put(data.thread_id_, data);
        }
        for (ThreadData data : data_list) {
            ThreadData db_data = data_map_db.get(data.thread_id_);
            if (db_data != null) {
                db_data.up2date(data);
                new_data_list_db.add(db_data);
            }
            else {
                data_list_result.add(data);
            }
        }
        agent_manager_.getDBAgent().updateThreadDataList(new_data_list_db, null);
        return data_list_result;
    }
    
    private void updateDatDroppedThreads(final List<ThreadData> data_list) {
        final List<ThreadData> dropped_list = new ArrayList<ThreadData>();
        for (ThreadData data : data_list) {
            if (data.sort_order_ == 0 && !data.is_dropped_) {
                data.is_dropped_ = true;
                dropped_list.add(data);
            }
        }
        
        if (dropped_list.size() > 0) {
            agent_manager_.getDBAgent().updateThreadDataList(dropped_list, null);
        }
    }
    
    public void fetchRecentList(final int recent_order, final RecentListFetchedCallback callback) {
        agent_manager_.getDBAgent().getRecentList(recent_order, new SQLiteAgent.DbResultReceiver() {
            @Override
            public void onQuery(Cursor cursor) {
                final ArrayList<ThreadData> data_list_db = new ArrayList<ThreadData>();
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    while (true) {
                        data_list_db.add(ThreadData.factory(cursor));
                        if (cursor.moveToNext() == false) break;
                    }
                }
                cursor.close();
                if (callback != null) callback.onRecentListFetched(data_list_db);
            }
            
            @Override
            public void onError() {
                if (callback != null) callback.onRecentListFetched(new ArrayList<ThreadData>());
            }
            
        });
    }
    
    public void fetchSimilarThreadList(final BoardData board_data, final long thread_id, final String search_key,
            final boolean force_reload, final boolean find_next_thread, final ThreadListFetchedCallback callback) {
        final List<ThreadData> result_list = new ArrayList<ThreadData>();
        reloadThreadList(board_data, force_reload, new ThreadListFetchedCallback() {
            
            @Override
            public void onThreadListFetchCompleted() {
                filterSimilarThreadList(result_list, thread_id, search_key, find_next_thread, callback);
            }
            
            @Override
            public void onThreadListFetchedCache(List<ThreadData> dataList) {
                result_list.addAll(dataList);
            }
            
            @Override
            public void onThreadListFetched(List<ThreadData> dataList) {
                result_list.addAll(dataList);
            }
            
            @Override
            public void onInterrupted() {
                callback.onInterrupted();
            }
            
            @Override
            public void onThreadListFetchFailed(final boolean maybe_moved) {
                callback.onThreadListFetchFailed(maybe_moved);
            }
            
            @Override
            public void onConnectionOffline() {
                callback.onConnectionOffline();
            }
            
            @Override
            public void onStartOnlineLoading() {
                callback.onStartOnlineLoading();
            }
        });
    }
    
    private static class SimilarThreadItem {
        ThreadData thread_data_;
        int rate_;
        
        public SimilarThreadItem(ThreadData threadData, int rate) {
            super();
            thread_data_ = threadData;
            rate_ = rate;
        }
    }
    
    private void filterSimilarThreadList(List<ThreadData> data_list, final long thread_id, final String search_key,
            final boolean find_next_thread, final ThreadListFetchedCallback callback) {
        int max_rate = 0;
        
        TreeSet<SimilarThreadItem> ordered_list = new TreeSet<SimilarThreadItem>(new Comparator<SimilarThreadItem>() {
            @Override
            public int compare(SimilarThreadItem data1, SimilarThreadItem data2) {
                int diff = data2.rate_ - data1.rate_;
                if (diff != 0) return diff;
                return (int) (data2.thread_data_.thread_id_ - data1.thread_data_.thread_id_);
            }
        });
        
        for (ThreadData data : data_list) {
            if (data.is_dropped_ || thread_id == data.thread_id_) continue;
            // 次スレ検索なら古いスレは不要
            if (find_next_thread && thread_id > data.thread_id_) continue;
            
            int rate = LevenshteinDistance.similarity(search_key, data.thread_name_);
            if (rate > max_rate) max_rate = rate;
            // 一致度1/5以下は足切り
            if (rate > LevenshteinDistance.MAX_SIMILARITY_RATE / 5) {
                ordered_list.add(new SimilarThreadItem(data, rate));
            }
        }
        
        int border_rate = 0;
        if (find_next_thread) {
            // 次スレ検索ならボーダーは厳しくする
            border_rate = ordered_list.first().rate_ * 4 / 5;
        }
        else {
            border_rate = java.lang.Math.min(ordered_list.first().rate_ / 2,
                    LevenshteinDistance.MAX_SIMILARITY_RATE / 3);
        }
        List<ThreadData> result_list = new ArrayList<ThreadData>();
        for (SimilarThreadItem data : ordered_list) {
            if (data.rate_ >= border_rate) result_list.add(data.thread_data_);
        }
        
        callback.onThreadListFetchedCache(result_list);
        callback.onThreadListFetchCompleted();
    }
    
}
