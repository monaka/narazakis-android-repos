package info.narazaki.android.lib.aplication;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.Thread.UncaughtExceptionHandler;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.util.Log;

public class NApplication extends Application {
    private NActivityStackManager activity_stack_;
    private File crash_report_file_;
    
    public NApplication(int max_activities) {
        activity_stack_ = new NActivityStackManager(max_activities);
        
        Thread.setDefaultUncaughtExceptionHandler(new NUncaughtExceptionHandler(Thread
                .getDefaultUncaughtExceptionHandler()));
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        crash_report_file_ = createCrashReportFile();
    }
    
    public void onActivityResume(Activity activity) {
        activity_stack_.onResume(activity);
    }
    
    public void onActivityDestroy(Activity activity) {
        activity_stack_.onDestroy(activity);
    }
    
    public int getScrollingAmount() {
        return 100;
    }
    
    public File createCrashReportFile() {
        File dir = getDir("crash_report", Context.MODE_PRIVATE);
        if (dir.isDirectory() || dir.mkdirs()) {
            return new File(dir, "dump.txt");
        }
        return null;
    }
    
    private class NUncaughtExceptionHandler implements UncaughtExceptionHandler {
        UncaughtExceptionHandler parent_handler_;
        
        public NUncaughtExceptionHandler(UncaughtExceptionHandler parent_handler) {
            super();
            parent_handler_ = parent_handler;
        }
        
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            if (crash_report_file_ == null) return;
            
            try {
                BufferedWriter osw = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(crash_report_file_), "UTF-8"));
                PackageInfo package_info = getPackageManager().getPackageInfo(getPackageName(), 0);
                osw.append(getPackageName());
                osw.append(" : ").append(package_info.versionName);
                osw.append('(').append(Integer.toString(package_info.versionCode)).append(")\n");
                
                osw.append("---- Environment ----\n");
                osw.append("Build.ID = ").append(Build.ID).append('\n');
                osw.append("Build.DISPLAY = ").append(Build.DISPLAY).append('\n');
                osw.append("Build.PRODUCT = ").append(Build.PRODUCT).append('\n');
                osw.append("Build.DEVICE = ").append(Build.DEVICE).append('\n');
                osw.append("Build.BOARD = ").append(Build.BOARD).append('\n');
                osw.append("Build.BRAND = ").append(Build.BRAND).append('\n');
                osw.append("Build.MODEL = ").append(Build.MODEL).append('\n');
                osw.append("Build.TYPE = ").append(Build.TYPE).append('\n');
                osw.append("Build.TAGS = ").append(Build.TAGS).append('\n');
                osw.append("Build.FINGERPRINT = ").append(Build.FINGERPRINT).append('\n');
                osw.append("Build.VERSION.RELEASE = ").append(Build.VERSION.RELEASE).append('\n');
                osw.append("Build.VERSION.SDK = ").append(Build.VERSION.SDK).append('\n');
                
                osw.append("---- Exception ----\n");
                osw.append(ex.toString()).append('\n');
                osw.append(ex.getMessage()).append('\n');
                osw.append(ex.getLocalizedMessage()).append('\n');
                Throwable cause = ex.getCause();
                if (cause != null) {
                    osw.append("---- Exception (Cause) ----\n");
                    osw.append(cause.toString()).append('\n');
                    osw.append(cause.getMessage()).append('\n');
                    osw.append(cause.getLocalizedMessage()).append('\n');
                }
                
                Throwable current_throwable = ex;
                while (current_throwable != null) {
                    osw.append("---- Stacktrace ----\n");
                    StackTraceElement[] traces = current_throwable.getStackTrace();
                    for (StackTraceElement trace : traces) {
                        osw.append(trace.getClassName()).append('#');
                        osw.append(trace.getMethodName()).append(':');
                        osw.append(Integer.toString(trace.getLineNumber())).append('\n');
                    }
                    current_throwable = current_throwable.getCause();
                }
                osw.append("---- Activities ----\n");
                for (Activity activity : activity_stack_.getActivities()) {
                    osw.append(activity.getClass().getName()).append('\n');
                }
                osw.close();
            }
            catch (IOException e) {
            }
            catch (NameNotFoundException e) {
            }
            
            if (parent_handler_ != null) parent_handler_.uncaughtException(thread, ex);
        }
    }
    
}
