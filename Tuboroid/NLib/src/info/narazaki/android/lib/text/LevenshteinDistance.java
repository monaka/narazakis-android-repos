package info.narazaki.android.lib.text;

/**
 * 2つの文字列についての編集距離計算機
 * 
 * @author H.Narazaki
 */
public abstract class LevenshteinDistance {
    public static final int MAX_SIMILARITY_RATE = 65535;
    
    /**
     * 2つの文字列について編集距離を計算する
     * 
     * 挿入・削除は1、置換について1か2かは実装による 最大はstr1.length() + str2.length()である
     * 
     * @param str1
     * @param str2
     * @return 編集距離
     */
    // public int diff(String str1, String str2) {
    // return calcONP(str1, str2);
    // }
    public static native int diff(String str1, String str2);
    
    /**
     * 2つの文字列について編集距離の「近さ」を計算する
     * 
     * 一致度が高いほど数値は大きくなり、完全一致で最大値、完全不一致で0、となる
     * 
     * @param str1
     * @param str2
     * @return 編集距離の近さ
     */
    public static int similar(String str1, String str2) {
        return str1.length() + str2.length() - diff(str1, str2);
    }
    
    /**
     * 2つの文字列について編集距離の「近さ」の割合を計算する
     * 
     * 最大距離からの割合で計算されるので MAX_SIMILARITY_RATE(完全一致)から0(完全不一致)までで表される
     * 
     * @param str1
     * @param str2
     * @return
     */
    public static int similarity(String str1, String str2) {
        int max = str1.length() + str2.length();
        return (max - diff(str1, str2)) * MAX_SIMILARITY_RATE / max;
    }
    
    public static native void initNative();
    
    static {
        System.loadLibrary("info_narazaki_android_nlib");
        initNative();
    }
    
}
