package info.narazaki.android.lib.text.span;

import java.util.ArrayList;
import android.text.Spannable;
import android.text.Spanned;

/**
 * カスタマイズ可能なSpanify
 * 
 * @author H.Narazaki
 */
public class SpanifyAdapter {
    public static final String TAG = "SpanifyAdapter";
    
    private ArrayList<SpanifyFilter> filter_list_;
    private Spannable.Factory spannable_factory_;
    
    public SpanifyAdapter() {
        filter_list_ = new ArrayList<SpanifyFilter>();
        spannable_factory_ = Spannable.Factory.getInstance();
    }
    
    final public void addFilter(SpanifyFilter filter) {
        filter_list_.add(filter);
    }
    
    final public CharSequence apply(CharSequence text) {
        return apply(text, null);
    }
    
    final public CharSequence apply(CharSequence text, Object arg) {
        Spannable result = null;
        ;
        if (text instanceof Spannable) result = (Spannable) text;
        for (SpanifyFilter filter : filter_list_) {
            SpanSpec[] spec_list = filter.gather(text, arg);
            if (spec_list != null) {
                if (result == null) result = spannable_factory_.newSpannable(text);
                for (SpanSpec spec : spec_list) {
                    result.setSpan(filter.getSpan(spec.text_, spec, arg), spec.start_, spec.end_,
                            Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                }
            }
        }
        
        return (result != null) ? result : text;
    }
    
}
