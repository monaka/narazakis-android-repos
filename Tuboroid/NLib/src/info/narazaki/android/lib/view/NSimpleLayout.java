package info.narazaki.android.lib.view;

import info.narazaki.android.lib.memory.SimpleFloatArrayPool;
import info.narazaki.android.lib.memory.SimpleIntArrayPool;
import info.narazaki.android.lib.memory.SimpleObjectArrayPool;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Paint.FontMetrics;
import android.text.Layout;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.CharacterStyle;
import android.text.style.LineHeightSpan;
import android.text.style.MetricAffectingSpan;
import android.text.style.ParagraphStyle;
import android.text.style.ReplacementSpan;

public class NSimpleLayout {
    public static final String TAG = "NSimpleLayout";
    
    private static final SimpleFloatArrayPool g_width_float_pool_;
    private static final SimpleIntArrayPool g_lines_int_pool_;
    private static final SimpleFloatArrayPool g_metric_float_pool_;
    private static final SimpleIntArrayPool g_metric_int_pool_;
    private static final TextPaintPool g_textpaint_pool_;
    
    static {
        g_width_float_pool_ = new SimpleFloatArrayPool(4);
        g_lines_int_pool_ = new SimpleIntArrayPool(4);
        g_metric_float_pool_ = new SimpleFloatArrayPool(4);
        g_metric_int_pool_ = new SimpleIntArrayPool(4);
        g_textpaint_pool_ = new TextPaintPool(2);
    }
    
    private TextPaint paint_;
    private int width_;
    private CharSequence text_;
    
    private float[] width_array_ = null;
    
    private static final int LINES_I_START = 0;
    private static final int LINES_I_FM_TOP = 1;
    private static final int LINES_I_FM_DESCENT = 2;
    private static final int LINES_I_WIDTH = 3;
    private static final int LINES_I_MAX = 4;
    
    private static final int FM_I_TOP = 0;
    private static final int FM_I_BOTTOM = 1;
    private static final int FM_I_ASCENT = 2;
    private static final int FM_I_DESCENT = 3;
    private static final int FM_I_LEADING = 4;
    private static final int FM_I_MAX = 5;
    
    private int line_count_ = 0;
    private int lines_[] = null;
    
    public NSimpleLayout(CharSequence text, TextPaint paint, int width, Layout.Alignment align, float spacingMult,
            float spacingAdd, boolean includepad) {
        generate(text, paint, width, align);
    }
    
    @Override
    protected void finalize() throws Throwable {
        recycle();
        super.finalize();
    }
    
    public void recycle() {
        if (width_array_ != null) width_array_ = g_width_float_pool_.recycle(width_array_);
        if (lines_ != null) lines_ = g_lines_int_pool_.recycle(lines_);
    }
    
    public void regenerate(CharSequence text, TextPaint paint, int width, Layout.Alignment align) {
        generate(text, paint, width, align);
    }
    
    private void generate(CharSequence text, TextPaint paint, int width, Layout.Alignment align) {
        paint_ = paint;
        width_ = width;
        text_ = text;
        if (text instanceof Spanned) {
            generateSpanned((Spanned) text, paint, width, align);
        }
        else {
            generateNotSpanned(text, paint, width, align);
        }
    }
    
    private void generateNotSpanned(CharSequence text, TextPaint paint, int width, Layout.Alignment align) {
        int text_len = text.length();
        
        recycle();
        width_array_ = g_width_float_pool_.obtain(text_len + 1);
        
        // デフォルトのMetricで計算
        paint.getTextWidths(text.toString(), width_array_);
        FontMetrics default_fm = paint.getFontMetrics();
        
        int[] metric_change_point = g_metric_int_pool_.obtain(2);
        float[] metric_list = g_metric_float_pool_.obtain(FM_I_MAX);
        metric_change_point[0] = 0;
        metric_change_point[1] = 0;
        metric_list[FM_I_TOP] = default_fm.top;
        metric_list[FM_I_BOTTOM] = default_fm.bottom;
        metric_list[FM_I_ASCENT] = default_fm.ascent;
        metric_list[FM_I_DESCENT] = default_fm.descent;
        metric_list[FM_I_LEADING] = default_fm.leading;
        
        // 禁則処理付き折り返し
        int[] lines_tmp = g_lines_int_pool_.obtain((text.length() / 20 + 2) * LINES_I_MAX);
        lines_ = generateNative(text.toString(), width, lines_tmp, width_array_, metric_change_point, metric_list, 1);
        if (lines_ != lines_tmp) {
            lines_tmp = g_lines_int_pool_.recycle(lines_tmp);
        }
        line_count_ = lines_[lines_.length - 1];
        
        g_metric_int_pool_.recycle(metric_change_point);
        g_metric_float_pool_.recycle(metric_list);
    }
    
    private void generateSpanned(Spanned text, TextPaint paint, int width, Layout.Alignment align) {
        int text_len = text.length();
        
        if (width_array_ != null) g_width_float_pool_.recycle(width_array_);
        width_array_ = g_width_float_pool_.obtain(text_len + 1);
        float[] work_width_array = g_width_float_pool_.obtain(text_len + 1);
        
        // デフォルトのMetricで計算
        paint.getTextWidths(text.toString(), width_array_);
        FontMetrics fm = paint.getFontMetrics();
        
        // MetricAffectingSpanの部分を再計算
        int max_metric_changes = countMetricAffectingSpanChanges(text);
        int[] metric_change_point = g_metric_int_pool_.obtain(max_metric_changes + 1);
        float[] metric_list = g_metric_float_pool_.obtain((max_metric_changes + 1) * FM_I_MAX);
        int metric_changes = 0;
        
        if (max_metric_changes == 0) {
            // デフォルトのMetricで計算
            metric_change_point[0] = 0;
            metric_list[FM_I_TOP] = fm.top;
            metric_list[FM_I_BOTTOM] = fm.bottom;
            metric_list[FM_I_ASCENT] = fm.ascent;
            metric_list[FM_I_DESCENT] = fm.descent;
            metric_list[FM_I_LEADING] = fm.leading;
            paint.getTextWidths(text.toString(), width_array_);
        }
        else {
            TextPaint work_paint = g_textpaint_pool_.obtain();
            work_paint.set(paint);
            int end = text.length();
            for (int i = 0; i < end; i++) {
                int next = text.nextSpanTransition(i, end, MetricAffectingSpan.class);
                
                MetricAffectingSpan[] spans = text.getSpans(i, next, MetricAffectingSpan.class);
                work_paint.set(paint);
                for (MetricAffectingSpan span : spans) {
                    span.updateMeasureState(work_paint);
                }
                work_paint.getFontMetrics(fm);
                int len = work_paint.getTextWidths(text, i, next, work_width_array);
                System.arraycopy(work_width_array, 0, width_array_, i, len);
                
                metric_change_point[metric_changes] = i;
                metric_list[metric_changes * FM_I_MAX + FM_I_TOP] = fm.top;
                metric_list[metric_changes * FM_I_MAX + FM_I_BOTTOM] = fm.bottom;
                metric_list[metric_changes * FM_I_MAX + FM_I_ASCENT] = fm.ascent;
                metric_list[metric_changes * FM_I_MAX + FM_I_DESCENT] = fm.descent;
                metric_list[metric_changes * FM_I_MAX + FM_I_LEADING] = fm.leading;
                
                metric_changes++;
                i = next;
            }
            g_textpaint_pool_.recycle(work_paint);
        }
        work_width_array = g_width_float_pool_.recycle(work_width_array);
        
        // 禁則処理付き折り返し
        int[] lines_tmp = g_lines_int_pool_.obtain((text.length() / 20 + 2) * LINES_I_MAX);
        lines_ = generateNative(text.toString(), width, lines_tmp, width_array_, metric_change_point, metric_list,
                metric_changes);
        if (lines_ != lines_tmp) {
            lines_tmp = g_lines_int_pool_.recycle(lines_tmp);
        }
        line_count_ = lines_[lines_.length - 1];
        
        g_metric_int_pool_.recycle(metric_change_point);
        g_metric_float_pool_.recycle(metric_list);
    }
    
    private int countMetricAffectingSpanChanges(final Spanned text) {
        int changes = 0;
        int end = text.length();
        for (int i = 0; i < end; i++) {
            int next = text.nextSpanTransition(i, end, MetricAffectingSpan.class);
            if (next == end) break;
            changes++;
            i = next;
        }
        return changes;
    }
    
    private native int[] generateNative(String text, int max_width, int[] lines_tmp, float[] width_array,
            int[] metric_change_point, float[] metric_list, int metric_changes);
    
    private void reallocLines(int size) {
        int want = size * LINES_I_MAX;
        if (lines_ != null && lines_.length > want) {
            return;
        }
        int[] new_lines = g_lines_int_pool_.obtain(want * 2 + 1);
        if (lines_ != null) {
            System.arraycopy(lines_, 0, new_lines, 0, lines_.length);
            g_lines_int_pool_.recycle(lines_);
        }
        lines_ = new_lines;
    }
    
    // check unsupported span
    public static boolean checkSupported(CharSequence text, TextPaint paint, int width, Layout.Alignment align,
            float spacingMult, float spacingAdd, boolean includepad) {
        if (spacingMult != 1 || spacingAdd != 0) return false;
        if (align != Layout.Alignment.ALIGN_NORMAL) return false;
        if (!includepad) return false;
        if (text instanceof Spanned) {
            Spanned spanned = (Spanned) text;
            int len = text.length();
            int found = spanned.nextSpanTransition(0, len, ParagraphStyle.class);
            if (found != len) return false;
            
            found = spanned.nextSpanTransition(0, len, LineHeightSpan.class);
            if (found != len) return false;
            
            found = spanned.nextSpanTransition(0, len, ReplacementSpan.class);
            if (found != len) return false;
        }
        return checkSupportedNative(text.toString());
    }
    
    private static native boolean checkSupportedNative(String text);
    
    private static Rect g_temp_rect_ = new Rect();
    
    /**
     * Draw this Layout on the specified canvas, with the highlight path drawn
     * between the background and the text.
     * 
     * @param c
     *            the canvas
     */
    public void draw(Canvas c) {
        int dtop, dbottom;
        
        synchronized (g_temp_rect_) {
            if (!c.getClipBounds(g_temp_rect_)) return;
            dtop = g_temp_rect_.top;
            dbottom = g_temp_rect_.bottom;
        }
        
        int top = 0;
        int bottom = getLineTop(getLineCount());
        
        if (dtop > top) {
            top = dtop;
        }
        if (dbottom < bottom) {
            bottom = dbottom;
        }
        boolean is_spanned = text_ instanceof Spanned;
        
        int first_line = getLineForVertical(top);
        int last_line = getLineForVertical(bottom);
        
        if (is_spanned) {
            drawSpannedText(c, first_line, last_line);
        }
        else {
            drawPlainText(c, first_line, last_line);
        }
    }
    
    private void drawPlainText(Canvas c, int first_line, int last_line) {
        TextPaint paint = paint_;
        CharSequence buf = text_;
        int prev_line_end = getLineStart(first_line);
        
        for (int i = first_line; i <= last_line; i++) {
            int start = prev_line_end;
            prev_line_end = getLineStart(i + 1);
            int end = getLineVisibleEnd(i);
            
            int line_bottom = getLineTop(i + 1);
            int line_baseline = line_bottom - getLineDescent(i);
            
            c.drawText(buf, start, end, 0, line_baseline, paint);
        }
    }
    
    private void drawSpannedText(Canvas c, int first_line, int last_line) {
        TextPaint work_paint = g_textpaint_pool_.obtain();
        Spanned spanned = (Spanned) text_;
        String spannded_body = text_.toString();
        int prev_line_end = getLineStart(first_line);
        
        for (int i = first_line; i <= last_line; i++) {
            int start = prev_line_end;
            prev_line_end = getLineStart(i + 1);
            int end = getLineVisibleEnd(i);
            
            int line_bottom = getLineTop(i + 1);
            int line_baseline = line_bottom - getLineDescent(i);
            
            int next;
            float x = 0;
            for (int j = start; j < end; j = next) {
                next = spanned.nextSpanTransition(j, end, CharacterStyle.class);
                work_paint.set(paint_);
                CharacterStyle[] spans = spanned.getSpans(j, next, CharacterStyle.class);
                for (CharacterStyle span : spans) {
                    span.updateDrawState(work_paint);
                }
                c.drawText(spannded_body, j, next, x, line_baseline, work_paint);
                for (int k = j; k < next; k++) {
                    x += width_array_[k];
                }
            }
        }
        
        g_textpaint_pool_.recycle(work_paint);
    }
    
    public int getWidth() {
        return width_;
    }
    
    public final void increaseWidthTo(int wid) {
        width_ = wid;
        regenerate(text_, paint_, width_, Layout.Alignment.ALIGN_NORMAL);
    }
    
    public final CharSequence getText() {
        return text_;
    }
    
    public int getLineForVertical(int vertical) {
        int high = getLineCount(), low = -1, guess;
        
        while (high - low > 1) {
            guess = (high + low) / 2;
            
            if (getLineTop(guess) > vertical) high = guess;
            else low = guess;
        }
        
        if (low < 0) return 0;
        else return low;
    }
    
    /**
     * Gets the horizontal extent of the specified line, including trailing
     * whitespace.
     */
    public float getLineWidth(int line) {
        if (line > line_count_) return 0;
        return lines_[line * LINES_I_MAX + LINES_I_WIDTH];
    }
    
    /**
     * Return the number of lines of text in this layout.
     */
    public int getLineCount() {
        return line_count_;
    }
    
    /**
     * Return the vertical position of the top of the specified line
     * (0&hellip;getLineCount()). If the specified line is equal to the line
     * count, returns the bottom of the last line.
     */
    public int getLineTop(int line) {
        if (line > line_count_) return line = line_count_;
        return lines_[line * LINES_I_MAX + LINES_I_FM_TOP];
    }
    
    public final int getLineEnd(int line) {
        return getLineStart(line + 1);
    }
    
    public int getLineVisibleEnd(int line) {
        CharSequence text = text_;
        int start = getLineStart(line);
        int end = getLineStart(line + 1);
        
        if (line == getLineCount() - 1) {
            return end;
        }
        
        char chr;
        for (; end > start; end--) {
            chr = text.charAt(end - 1);
            if (chr != '\n' && chr != ' ' && chr != '\t') break;
        }
        
        return end;
    }
    
    /**
     * Return the descent of the specified line(0&hellip;getLineCount() - 1).
     */
    public int getLineDescent(int line) {
        if (line > line_count_) return line = line_count_;
        return lines_[line * LINES_I_MAX + LINES_I_FM_DESCENT];
    }
    
    /**
     * Return the text offset of the beginning of the specified line (
     * 0&hellip;getLineCount()). If the specified line is equal to the line
     * count, returns the length of the text.
     */
    public int getLineStart(int line) {
        if (line > line_count_) return line = line_count_;
        return lines_[line * LINES_I_MAX + LINES_I_START];
    }
    
    /**
     * Returns whether the specified line contains one or more characters that
     * need to be handled specially, like tabs or emoji.
     */
    public boolean getLineContainsTab(int line) {
        return false;
    }
    
    /**
     * Returns the (negative) number of extra pixels of ascent padding in the
     * top line of the Layout.
     */
    public int getTopPadding() {
        return 0;
    }
    
    /**
     * Returns the number of extra pixels of descent padding in the bottom line
     * of the Layout.
     */
    public int getBottomPadding() {
        return 0;
    }
    
    /**
     * Return the offset of the first character to be ellipsized away, relative
     * to the start of the line. (So 0 if the beginning of the line is
     * ellipsized, not getLineStart().)
     */
    public int getEllipsisStart(int line) {
        return getLineStart(line);
    }
    
    /**
     * Returns the number of characters to be ellipsized away, or 0 if no
     * ellipsis is to take place.
     */
    public int getEllipsisCount(int line) {
        return 0;
    }
    
    public int getOffsetForHorizontal(int line, float horiz) {
        int start = getLineStart(line);
        int end = getLineEnd(line);
        if (end - 1 <= start) return start;
        float width = 0;
        for (int i = start + 1; i < end; i++) {
            width += width_array_[i];
            if (width > horiz) return i - 1;
        }
        return end - 1;
    }
    
    static private class TextPaintPool extends SimpleObjectArrayPool<TextPaint> {
        public TextPaintPool(int max_pool) {
            super(max_pool);
        }
        
        @Override
        protected TextPaint construct() {
            return new TextPaint();
        }
    }
    
    static {
        System.loadLibrary("info_narazaki_android_nlib");
        // initNative();
    }
    
}
