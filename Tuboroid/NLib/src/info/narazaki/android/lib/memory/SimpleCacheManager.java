package info.narazaki.android.lib.memory;

import java.lang.ref.SoftReference;
import java.util.LinkedHashMap;

import android.util.Log;

public class SimpleCacheManager<CACHE_TAG, CACHE_DATA> {
    private static final String TAG = "SimpleCacheDataList";
    
    private final int max_cache_size_;
    private final int min_cache_size_;
    private final boolean is_debug_mode_;
    
    private final LinkedHashMap<CACHE_TAG, CACHE_DATA> hard_cache_;
    private final LinkedHashMap<CACHE_TAG, SoftReference<CACHE_DATA>> soft_cache_;
    
    public SimpleCacheManager(int min_cache_size, int max_cache_size, boolean is_debug_mode) {
        min_cache_size_ = min_cache_size;
        max_cache_size_ = max_cache_size;
        is_debug_mode_ = is_debug_mode;
        
        if (min_cache_size_ > 0) {
            hard_cache_ = new LinkedHashMap<CACHE_TAG, CACHE_DATA>(min_cache_size_, 0.75f, true) {
                private static final long serialVersionUID = 1L;
                
                @Override
                protected boolean removeEldestEntry(LinkedHashMap.Entry<CACHE_TAG, CACHE_DATA> eldest) {
                    if (size() <= min_cache_size_) {
                        if (is_debug_mode_) {
                            final CACHE_TAG tag = eldest.getKey();
                            putDebugMessage(tag, "hard cache : expired");
                        }
                        return false;
                    }
                    return true;
                }
            };
        }
        else {
            hard_cache_ = null;
        }
        soft_cache_ = new LinkedHashMap<CACHE_TAG, SoftReference<CACHE_DATA>>(max_cache_size_, 0.75f, true) {
            private static final long serialVersionUID = 1L;
            
            @Override
            protected boolean removeEldestEntry(LinkedHashMap.Entry<CACHE_TAG, SoftReference<CACHE_DATA>> eldest) {
                if (size() <= max_cache_size_) {
                    if (is_debug_mode_) {
                        final CACHE_TAG tag = eldest.getKey();
                        putDebugMessage(tag, "soft cache : expired");
                    }
                    return false;
                }
                return true;
            }
        };
    }
    
    private void putDebugMessage(final CACHE_TAG tag, final String message) {
        Log.v(TAG, tag.getClass().getName() + " : " + tag.toString() + "(" + tag.hashCode() + ") : " + message);
    }
    
    public synchronized CACHE_DATA getData(CACHE_TAG tag, boolean update_order) {
        SoftReference<CACHE_DATA> ref = soft_cache_.get(tag);
        if (ref == null) {
            if (is_debug_mode_) putDebugMessage(tag, "cache missing");
            return null;
        }
        CACHE_DATA data = ref.get();
        if (data == null) {
            if (is_debug_mode_) putDebugMessage(tag, "soft ref missing");
            return null;
        }
        
        if (hard_cache_ != null) hard_cache_.put(tag, data);
        if (is_debug_mode_) putDebugMessage(tag, "cache hit!");
        
        return data;
    }
    
    public synchronized void setData(CACHE_TAG tag, CACHE_DATA data) {
        if (hard_cache_ != null) hard_cache_.put(tag, data);
        soft_cache_.put(tag, new SoftReference<CACHE_DATA>(data));
        if (is_debug_mode_) putDebugMessage(tag, "cache set");
    }
    
    public synchronized void removeData(CACHE_TAG tag) {
        if (hard_cache_ != null) hard_cache_.remove(tag);
        soft_cache_.remove(tag);
        if (is_debug_mode_) putDebugMessage(tag, "cache removed");
    }
    
}
