package info.narazaki.android.lib.system;

public interface NCallback<T> {
    void run(T arg);
}
