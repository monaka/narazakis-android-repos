package info.narazaki.android.lib.activity.base;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ExpandableListView;
import info.narazaki.android.lib.activity.base.NSimpleListActivity.ReloadTerminator;
import info.narazaki.android.lib.adapter.SimpleExpandableListAdapterBase;
import info.narazaki.android.lib.aplication.NSimpleApplication;
import info.narazaki.android.lib.system.NCallback;

abstract public class NSimpleExpandableListActivity extends NExpandableListActivity {
    private static final String TAG = "NSimpleExpandableListActivity";
    
    private static final String INTENT_KEY_RESTORE_ITEM_PACKED_POS = "__NSimpleExpandableListActivity_restore_item_packed_pos";
    private static final String INTENT_KEY_RESTORE_ITEM_TOP = "__NSimpleExpandableListActivity_restore_item_top";
    private static final String INTENT_KEY_RESTORE_ITEM_EXPAND_STAT_LIST = "__NSimpleExpandableListActivity_restore_item_expand_stat_list";
    
    public static class StatData {
        public PositionData pos_data_;
        public ArrayList<Boolean> expand_stat_list_;
        
        public StatData() {
            super();
            pos_data_ = new PositionData(0, 0);
            expand_stat_list_ = new ArrayList<Boolean>();
        }
        
        public StatData(StatData stat) {
            super();
            pos_data_ = new PositionData(stat.pos_data_.packed_pos_, stat.pos_data_.y_);
            expand_stat_list_ = new ArrayList<Boolean>(stat.expand_stat_list_);
        }
        
        public StatData(PositionData pos_data, ArrayList<Boolean> expand_stat_list) {
            super();
            pos_data_ = pos_data;
            expand_stat_list_ = expand_stat_list;
        }
    }
    
    protected boolean is_active_;
    protected SimpleExpandableListAdapterBase<?> list_adapter_ = null;
    
    private boolean reload_in_progress_ = false;
    
    private StatData restore_position_ = null;
    
    private boolean has_initial_data_ = false;
    
    private ReloadTerminator current_reload_terminator_;
    
    abstract protected SimpleExpandableListAdapterBase<?> createListAdapter();
    
    abstract protected void onFirstDataRequired();
    
    abstract protected void onResumeDataRequired();
    
    protected final NSimpleApplication getNSimpleApplication() {
        return (NSimpleApplication) getApplication();
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        is_active_ = true;
        
        has_initial_data_ = false;
        restore_position_ = null;
        reload_in_progress_ = false;
        
        current_reload_terminator_ = new ReloadTerminator();
        
        String class_name = this.getClass().getName();
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(class_name + INTENT_KEY_RESTORE_ITEM_PACKED_POS)) {
                restore_position_ = new StatData();
                restore_position_.pos_data_.packed_pos_ = savedInstanceState.getLong(class_name
                        + INTENT_KEY_RESTORE_ITEM_PACKED_POS);
                restore_position_.pos_data_.y_ = savedInstanceState.getInt(class_name + INTENT_KEY_RESTORE_ITEM_TOP);
                boolean[] list = savedInstanceState.getBooleanArray(class_name
                        + INTENT_KEY_RESTORE_ITEM_EXPAND_STAT_LIST);
                for (boolean data : list) {
                    restore_position_.expand_stat_list_.add(data);
                }
            }
        }
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        String class_name = this.getClass().getName();
        if (has_initial_data_) {
            saveRestorePosition();
            if (restore_position_ != null) {
                onSaveRestorePositonState(restore_position_);
                outState.putLong(class_name + INTENT_KEY_RESTORE_ITEM_PACKED_POS,
                        restore_position_.pos_data_.packed_pos_);
                outState.putInt(class_name + INTENT_KEY_RESTORE_ITEM_TOP, restore_position_.pos_data_.y_);
                boolean[] list = new boolean[restore_position_.expand_stat_list_.size()];
                for (int i = 0; i < restore_position_.expand_stat_list_.size(); i++) {
                    list[i] = restore_position_.expand_stat_list_.get(i);
                }
                outState.putBooleanArray(class_name + INTENT_KEY_RESTORE_ITEM_EXPAND_STAT_LIST, list);
            }
        }
        super.onSaveInstanceState(outState);
    }
    
    @Override
    protected void onDestroy() {
        list_adapter_.clear();
        super.onDestroy();
    }
    
    @Override
    protected void onStart() {
        is_active_ = true;
        super.onStart();
        if (list_adapter_ == null) {
            list_adapter_ = createListAdapter();
            setListAdapter(list_adapter_);
        }
    }
    
    @Override
    protected void onResume() {
        is_active_ = true;
        setListViewVisibility(false);
        super.onResume();
    }
    
    @Override
    protected void onFirstResume() {
        super.onFirstResume();
        onFirstDataRequired();
    }
    
    protected boolean hasInitialData() {
        return has_initial_data_;
    }
    
    protected void hasInitialData(boolean has_initial_data) {
        has_initial_data_ = has_initial_data;
    }
    
    protected boolean hasValidData() {
        return hasInitialData() && !isReloadInProgress();
    }
    
    protected void setListViewVisibility(boolean visibility) {
        ExpandableListView list_view = getExpandableListView();
        if (list_view != null) {
            list_view.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
        }
    }
    
    @Override
    protected void onSecondResume() {
        super.onSecondResume();
        onResumeDataRequired();
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        is_active_ = false;
        saveRestorePosition();
        if (restore_position_ != null) onPauseRestorePosition(restore_position_);
        discardDataOnPause();
        has_initial_data_ = false;
        setReloadInProgress(false);
        current_reload_terminator_.is_terminated_ = true;
    }
    
    protected void onPauseRestorePosition(StatData pos_data) {}
    
    protected void discardDataOnPause() {
        list_adapter_.clear();
    }
    
    protected boolean hasRestorePosition() {
        return restore_position_ != null;
    }
    
    protected ReloadTerminator getNewReloadTerminator() {
        current_reload_terminator_.is_terminated_ = true;
        current_reload_terminator_ = new ReloadTerminator();
        return current_reload_terminator_;
    }
    
    private void saveRestorePosition() {
        if (list_adapter_ == null) return;
        if (restore_position_ == null && list_adapter_.getGroupCount() > 0) {
            ExpandableListView view = getExpandableListView();
            
            restore_position_ = new StatData();
            PositionData pos_data = getCurrentPosition();
            restore_position_.pos_data_ = pos_data;
            restore_position_.expand_stat_list_.clear();
            int group_count = list_adapter_.getGroupCount();
            for (int i = 0; i < group_count; i++) {
                restore_position_.expand_stat_list_.add(view.isGroupExpanded(i));
            }
        }
    }
    
    public void setRestorePosition(StatData stat) {
        restore_position_ = new StatData(stat);
    }
    
    protected void clearRestorePosition() {
        restore_position_ = null;
    }
    
    protected void onSaveRestorePositonState(StatData stat) {}
    
    protected final boolean onBeginReload() {
        if (isReloadInProgress()) return false;
        setReloadInProgress(true);
        return true;
    }
    
    protected final boolean isReloadInProgress() {
        return reload_in_progress_;
    }
    
    protected final synchronized void setReloadInProgress(boolean reload_in_progress) {
        reload_in_progress_ = reload_in_progress;
    }
    
    protected void onEndReload() {
        if (restore_position_ != null) {
            restorePosition(restore_position_.expand_stat_list_, restore_position_.pos_data_.packed_pos_,
                    restore_position_.pos_data_.y_, new NCallback<Boolean>() {
                        @Override
                        public void run(Boolean arg) {
                            onRestorePosition();
                        }
                    });
            return;
        }
        onRestorePosition();
    }
    
    protected void onEndReloadJumped() {
        has_initial_data_ = true;
    }
    
    protected void restorePosition(ArrayList<Boolean> expand_stat_list, long position, int y,
            NCallback<Boolean> callback) {
        ExpandableListView view = getExpandableListView();
        if (list_adapter_.getGroupCount() > 0) {
            int group_count = list_adapter_.getGroupCount();
            if (group_count > expand_stat_list.size()) {
                group_count = expand_stat_list.size();
            }
            for (int i = 0; i < group_count; i++) {
                if (expand_stat_list.get(i)) {
                    view.expandGroup(i);
                }
                else {
                    view.collapseGroup(i);
                }
            }
            expand_stat_list.clear();
        }
        setListPositionFromTop(position, y, callback);
    }
    
    protected void onRestorePosition() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setReloadInProgress(false);
                hasInitialData(true);
                setListViewVisibility(true);
                clearRestorePosition();
                onEndReloadJumped();
            }
        });
    }
    
    protected boolean useVolumeButtonScrolling() {
        return getNSimpleApplication().useVolumeButtonScrolling();
    }
    
    protected boolean useCameraButtonScrolling() {
        return getNSimpleApplication().useCameraButtonScrolling();
    }
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (useVolumeButtonScrolling()) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (hasInitialData()) setListPageUp();
                }
                return true;
            }
            if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (hasInitialData()) setListPageDown();
                }
                return true;
            }
        }
        if (useCameraButtonScrolling()) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_CAMERA) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (hasInitialData()) setListPageDown();
                }
                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
