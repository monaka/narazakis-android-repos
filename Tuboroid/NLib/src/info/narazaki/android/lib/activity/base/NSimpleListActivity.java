package info.narazaki.android.lib.activity.base;

import info.narazaki.android.lib.adapter.SimpleListAdapterBase;
import info.narazaki.android.lib.aplication.NSimpleApplication;
import info.narazaki.android.lib.system.NCallback;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListView;

abstract public class NSimpleListActivity extends NListActivity {
    private static final String TAG = "NSimpleListActivity";
    
    private static final String INTENT_KEY_RESTORE_ITEM_POS = "__NSimpleListActivity_restore_item_pos";
    private static final String INTENT_KEY_RESTORE_ITEM_TOP = "__NSimpleListActivity_restore_item_top";
    
    protected boolean is_active_;
    protected SimpleListAdapterBase<?> list_adapter_ = null;
    
    private boolean reload_in_progress_ = false;
    
    private PositionData restore_position_ = null;
    
    private boolean has_initial_data_ = false;
    
    abstract protected SimpleListAdapterBase<?> createListAdapter();
    
    abstract protected void onFirstDataRequired();
    
    abstract protected void onResumeDataRequired();
    
    static protected class ReloadTerminator {
        public boolean is_terminated_ = false;
    }
    
    private ReloadTerminator current_reload_terminator_;
    
    protected final NSimpleApplication getNSimpleApplication() {
        return (NSimpleApplication) getApplication();
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        is_active_ = true;
        has_initial_data_ = false;
        setReloadInProgress(false);
        
        restore_position_ = null;
        current_reload_terminator_ = new ReloadTerminator();
        
        String class_name = this.getClass().getName();
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(class_name + INTENT_KEY_RESTORE_ITEM_POS)) {
                restore_position_ = new PositionData(0, 0);
                restore_position_.position_ = savedInstanceState.getInt(class_name + INTENT_KEY_RESTORE_ITEM_POS);
                restore_position_.y_ = savedInstanceState.getInt(class_name + INTENT_KEY_RESTORE_ITEM_TOP);
            }
        }
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        saveRestorePosition();
        if (restore_position_ != null && has_initial_data_) {
            String class_name = this.getClass().getName();
            onSaveRestorePositonState(restore_position_);
            outState.putInt(class_name + INTENT_KEY_RESTORE_ITEM_POS, restore_position_.position_);
            outState.putInt(class_name + INTENT_KEY_RESTORE_ITEM_TOP, restore_position_.y_);
        }
        super.onSaveInstanceState(outState);
    }
    
    @Override
    protected void onDestroy() {
        list_adapter_.clear();
        super.onDestroy();
    }
    
    @Override
    protected void onStart() {
        is_active_ = true;
        super.onStart();
        if (list_adapter_ == null) {
            list_adapter_ = createListAdapter();
            setListAdapter(list_adapter_);
        }
    }
    
    @Override
    protected void onResume() {
        is_active_ = true;
        setListViewVisibility(false);
        super.onResume();
    }
    
    @Override
    protected void onFirstResume() {
        super.onFirstResume();
        onFirstDataRequired();
    }
    
    protected boolean hasInitialData() {
        return has_initial_data_;
    }
    
    protected void hasInitialData(boolean has_initial_data) {
        has_initial_data_ = has_initial_data;
    }
    
    protected boolean hasValidData() {
        return hasInitialData() && !isReloadInProgress();
    }
    
    protected void setListViewVisibility(boolean visibility) {
        ListView list_view = getListView();
        if (list_view != null) {
            list_view.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
        }
    }
    
    @Override
    protected void onSecondResume() {
        super.onSecondResume();
        onResumeDataRequired();
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        saveRestorePosition();
        if (restore_position_ != null) onPauseRestorePosition(restore_position_);
        is_active_ = false;
        discardDataOnPause();
        has_initial_data_ = false;
        setReloadInProgress(false);
        current_reload_terminator_.is_terminated_ = true;
    }
    
    protected void onPauseRestorePosition(PositionData pos_data) {}
    
    protected void discardDataOnPause() {
        list_adapter_.clear();
    }
    
    protected boolean hasRestorePosition() {
        return restore_position_ != null;
    }
    
    protected ReloadTerminator getNewReloadTerminator() {
        current_reload_terminator_.is_terminated_ = true;
        current_reload_terminator_ = new ReloadTerminator();
        return current_reload_terminator_;
    }
    
    protected void saveRestorePosition() {
        if (list_adapter_ == null) return;
        if (list_adapter_.getCount() > 0 && has_initial_data_) {
            PositionData pos_data = getCurrentPosition();
            if (pos_data != null) restore_position_ = pos_data;
        }
    }
    
    protected void setRestorePosition(int restore_item_pos, int restore_item_top_y) {
        restore_position_ = new PositionData(restore_item_pos, restore_item_top_y);
    }
    
    protected PositionData getRestorePosition() {
        return restore_position_;
    }
    
    protected void clearRestorePosition() {
        restore_position_ = null;
    }
    
    protected void onSaveRestorePositonState(PositionData stat) {}
    
    protected final boolean onBeginReload() {
        if (isReloadInProgress()) return false;
        setReloadInProgress(true);
        return true;
    }
    
    protected final synchronized boolean isReloadInProgress() {
        return reload_in_progress_;
    }
    
    protected final synchronized void setReloadInProgress(boolean reload_in_progress) {
        reload_in_progress_ = reload_in_progress;
    }
    
    protected void onEndReload() {
        if (restore_position_ != null && list_adapter_ != null) {
            restorePosition(new NCallback<Boolean>() {
                @Override
                public void run(Boolean arg) {
                    onRestorePosition();
                }
            });
            return;
        }
        else {
            onRestorePosition();
        }
    }
    
    protected void onEndReloadJumped() {}
    
    protected void restorePosition(NCallback<Boolean> callback) {
        if (restore_position_ != null && list_adapter_ != null) {
            restorePosition(restore_position_.position_, restore_position_.y_, callback);
        }
        else {
            callback.run(false);
        }
    }
    
    protected void restorePosition(int position, int y, NCallback<Boolean> callback) {
        setListPositionFromTop(position, y, callback);
    }
    
    protected void onRestorePosition() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setReloadInProgress(false);
                hasInitialData(true);
                setListViewVisibility(true);
                clearRestorePosition();
                onEndReloadJumped();
            }
        });
    }
    
    protected boolean useVolumeButtonScrolling() {
        return getNSimpleApplication().useVolumeButtonScrolling();
    }
    
    protected boolean useCameraButtonScrolling() {
        return getNSimpleApplication().useCameraButtonScrolling();
    }
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (useVolumeButtonScrolling()) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (hasInitialData()) setListPageUp();
                }
                return true;
            }
            if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (hasInitialData()) setListPageDown();
                }
                return true;
            }
        }
        if (useCameraButtonScrolling()) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_CAMERA) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (hasInitialData()) setListPageDown();
                }
                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }
    
    public void postListViewAndUiThread(final Runnable runnable) {
        final SimpleListAdapterBase<?> adapter = list_adapter_;
        if (adapter != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.postAdapterThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(runnable);
                        }
                    });
                }
            });
        }
        else {
            runOnUiThread(runnable);
        }
    }
    
}
