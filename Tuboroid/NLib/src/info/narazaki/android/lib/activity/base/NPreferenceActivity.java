package info.narazaki.android.lib.activity.base;

import java.util.IllegalFormatException;

import info.narazaki.android.lib.aplication.NApplication;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public class NPreferenceActivity extends PreferenceActivity {
    private boolean on_first_shown_ = true;
    private static final String PASSWD_DUMMY_TEXT = "********";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        on_first_shown_ = true;
        super.onCreate(savedInstanceState);
    }
    
    protected NApplication getNApplication() {
        return (NApplication) getApplication();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        getNApplication().onActivityResume(this);
        if (on_first_shown_) {
            onFirstResume();
            on_first_shown_ = false;
        }
        else {
            onSecondResume();
        }
    }
    
    protected void onFirstResume() {}
    
    protected void onSecondResume() {}
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        getNApplication().onActivityDestroy(this);
    }
    
    protected void setListPreferenceSummary(final String pref_name, final int pref_summary_id) {
        setListPreferenceSummary(pref_name, pref_summary_id, null);
    }
    
    protected void setListPreferenceSummary(final String pref_name, final int pref_summary_id,
            final Preference.OnPreferenceChangeListener callback) {
        final ListPreference pref = (ListPreference) findPreference(pref_name);
        pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object newValue) {
                CharSequence[] entries = pref.getEntries();
                int index = pref.findIndexOfValue((String) newValue);
                if (index >= 0) {
                    setListPreferenceSummaryCompat(preference, getString(pref_summary_id), entries[index]);
                }
                if (callback != null) return callback.onPreferenceChange(preference, newValue);
                return true;
            }
        });
        CharSequence entry_value = pref.getEntry();
        if (entry_value != null) setListPreferenceSummaryCompat(pref, getString(pref_summary_id), entry_value);
    }
    
    // Fix ListPreferences compatibility issue (Issue 14355)
    // http://code.google.com/p/android/issues/detail?id=14355
    private void setListPreferenceSummaryCompat(final Preference preference, final String format,
            final CharSequence entry) {
        String summary = String.format(format, entry);
        preference.setSummary(summary);
        try {
            preference.getSummary();
        }
        catch (IllegalFormatException e) {
            preference.setSummary(summary.replace("%", "%%"));
        }
    }
    
    protected void setEditTextPreferenceSummary(final String pref_name) {
        setEditTextPreferenceSummary(pref_name, null);
    }
    
    protected void setEditTextPreferenceSummary(final String pref_name,
            final Preference.OnPreferenceChangeListener callback) {
        final EditTextPreference pref = (EditTextPreference) findPreference(pref_name);
        pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object newValue) {
                preference.setSummary((CharSequence) newValue);
                if (callback != null) return callback.onPreferenceChange(preference, newValue);
                return true;
            }
        });
        CharSequence entry_value = pref.getText();
        if (entry_value != null) pref.setSummary(entry_value);
    }
    
    protected void setEditPasswordPreferenceSummary(final String pref_name) {
        setEditPasswordPreferenceSummary(pref_name, null);
    }
    
    protected void setEditPasswordPreferenceSummary(final String pref_name,
            final Preference.OnPreferenceChangeListener callback) {
        final EditTextPreference pref = (EditTextPreference) findPreference(pref_name);
        pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object newValue) {
                preference.setSummary(((CharSequence) newValue).length() == 0 ? "" : PASSWD_DUMMY_TEXT);
                if (callback != null) return callback.onPreferenceChange(preference, newValue);
                return true;
            }
        });
        CharSequence entry_value = pref.getText();
        if (entry_value != null) pref.setSummary(entry_value.length() == 0 ? "" : PASSWD_DUMMY_TEXT);
    }
    
}
